ARM Software hypervisor
=======================

This project is the proof of concept that has been developped during
François Serman's PhD. Thesis. If you want to know the motivation and
scientific purpose of this hypervisor, you are welcome to read the
thesis which is available [on the french PhD thesis portal](http://theses.fr/2016LIL10189).

This hypervisor is developped in the 2XS team of the
[CRIStAL](http://cristal.univ-lille.fr) laboratory of [Université
Lille 1](http://www.univ-lille1.fr)

The first author of this project is François Serman although he does
not work directly on it anymore.

Corresponding authors and current maintainers are:

* Michael Hauspie <michael.hauspie@univ-lille1.fr>
* Gilles Grimaud <gilles.grimaud@univ-lille1.fr>

Additional contributors may be found in the [AUTHORS.md](AUTHORS.md) file

This file tries to summarize how the hypervisor works and gives
pointers to someone that will try to understand its internals.

Repository content
==================

The main part of this project are located in the following folders:

* `hypervisor` : the platform indepented code of the
  hypervisor. Contains the main analysis algorithm,
* `utils` : some utility functions. A subset of libc is provided, as well as debug tools,
* `tools` : external tools and libraries. For example, qemu submodule is cloned and compiled there.
* `platforms/P` : platform specific code for platform `P`. Only
  `raspi` platform for raspberry pi 1 and 2 is implemented yet


Building and running the hypervisor
===================================

The hypervisor can be run either using QEMU or on a real raspberry pi
board. It uses the `arm-none-eabi` gcc toolchain. Thus you need to
have `arm-non-eabi-*` commands in your `PATH` before proceeding. The
compiler used can be modified in the `platform/raspi/Makefile.vars`
file. However, other compilers have not been tested.

Building QEMU
-------------

As of october 2017, the mainstream QEMU includes support for raspberry
pi 2 board only. However, it does not support the bcm2836 ARM timer
that is used in several guest samples. Thus, using the mainstream will
not allow you to run all the samples and to pass the tests.

For that, you will need to build another version of QEMU that supports
raspberry pi 1 with the bcm2835 timer.

To build qemu:

    $ git submodule update --init tools/qemu
    $ cd tools/qemu
    $ git submodule update --init dtc
    $ ./configure --target-list=arm-softmmu
    $ make

If your default python install is python3, you will need to install python2 and to call:

    $ ./configure --target-list=arm-softmmu --python=/usr/bin/python2

The qemu executable path is then `qemu/tools/qemu/arm-softmmu`. Put it
first in your `PATH` environment variable.

For all build rules that involves qemu, you need to have
`qemu-system-arm` in your `PATH`. Moreover, it must support `raspi`
machine if you build for raspberry pi 1 and `raspi2` machine if you
build for raspberry pi 2. Use `qemu-system-arm -M help` to check if
your QEMU supports the needed platform.

Building the hypervisor
-----------------------

All given commands must be executed in the `platforms/raspi` folder.

To build the hypervisor, you first need to build a guest sample. If
you forget this step, the hypervisor build chain will build the
`simple_scheduler` guest by default. To build a sample use this command line

    $ make -C sample_guests/sample_to_build

For example, to build an hello world sample:

    $ make -C sample_guests/hello_world

You can run a sample in standalone inside QEMU (that is, without the
hypervisor running) by running:

    $ make -C sample_guests/hello_world emu

Be aware that the makefile rule that makes the hypervisor use the build
guest is either the default one or the `deploy` one. Thus, when
calling the `emu` rule, the guest is *not* deployed in the hypervisor.

To build the hypervisor using the last deployed guest, use this command line:

    $ make

To run the hypervisor inside QEMU, use this one:

    $ make emu

The build can be tweaked using environment variables or make
variables. To get help on this mechanism, use:

    $ make help
    The build mode can be tweaked by setting those variables (either as environment variable or in make command line):
    * BUILD : build mode. Possible values: release, nooptim, only_guest (default release)
    * RPI2 : build for raspberry pi 2 (default 0, build for raspberry pi 1)
    * BENCH_INTERNALS : set to 1 to activate generation of statistics (cache, analysis count...) (default: 0)
    * ANALYSER_CACHE : set to 1 to activate analyser cache, 0 to disable it (default: 1)
    * ANALYSER_CACHE_OPTIMIZATION : set to 1 to activate analyser cache optimisation. Also force analyser cache activation (default: 1)
    * INSTRUCTION_CACHE : set to 1 to use instruction disassembly cache (default: 1)
    * LTO : set to 1 to activate link time optimization (default: 1)
    * LOG_LEVEL : set the log level value. This must be or bitwise ORed value of the following options (default: 0):
	  Cache optimizer messages:	           1
	  Analyser messages:                   2
	  Cache hits:				           4
	  Cache misses:		                   8
	  Cache updates:				      16
	  Cache general messages:			  32
	  Global hypervisor messages:		  64
	  Instruction disassembler messages: 128
	  Behavior callbacks messages:		 256
	  Instruction patches:			     512
	  Cache template engine:			1024

Thus, to build the hypervisor without any cache optimization, nor compiler optimization (useful for debug) use:

    $ make BUILD=nooptim ANALYSER_CACHE_OPTIMIZATION=0

and to run it: 

    $ make BUILD=nooptim ANALYSER_CACHE_OPTIMIZATION=0 emu

For ease of use, you can also use environment variables to set the
configuration for your shell session:

    $ export BUILD=nooptim
    $ export ANALYSER_CACHE_OPTIMIZATION=0
    $ make
    $ make emu

To run the tests, use

    $ make test
    
If all goes well, you shoud have a message that tells that all tests passed.

Using the hypervisor on real hardware
-------------------------------------

### Preparing the SD card ###

To build for a real raspberry pi hardware is the almost the same
process as building for QEMU. Only one more step is needed to build
the flash card with the right files. 

To create a file hierarchy that can be copied on an SD card and then used in the real PI, use this command

    $ make flash-tree
    [...]
    Copy the content of the build__raspi/bootloader to the root of a SD card and insert it the pi

If you are using linux and have `sudo` installed, you can automate the
process by using the label `2XS_HYPERV` for your SD card partition. Then, use:

    $ make flash

This should mount `/dev/disk/by-label/2XS_HYPERV` to
`/mnt/2xs_hyperv`, copy the needed files and unmount the flash
card. You can now insert it in the Pi and boot it.

### Using the Raspberry pi UART ###

The UART pins are (see http://pi.gadgetoid.com/pinout)

* TXD -> 8
* RXD -> 10
* GND -> 6

Do not connect the 3V3 so that the PI does not tries to boot with too
few power when connect to USB. By doing this, you keep good control on
the PI on/off status using the standard power plug.

You must use a FTDI if you want to connect using USB.

UART configuration

* 115200 bauds
* 8 bit data
* Odd parity
* 1 stop bit

When connecting using `screen` or `minicom` you should see the output
of the hypervisor through the serial port.

How it works
============

Initialization
--------------

Hypervisor C code starts in `bootstrap_main`. It begins by zeroing the
`bss` section. Then, the structure is created, fields are set to
specific platform function pointers and the platform is initialized by
a call to `hypervisor_init_platform`.

This function does two things:

1. It copies the structure pointed by the platform parameter to a global variable.
2. It calls the `init()` function of the platform, performing hardware
   specific initialization. 

The platform structure used as parameter to this function is then
never used anymore by the hypervisor. It should not be used again and
a call to `hypervisor_current_platform` must be done if one needs to
access the platform.

Starting the guest code
-----------------------

The guest code is started using the `hypervise()` function with the
address of the guest image boot code and the number of parameters to
use to call the guest code function. If this number more than zero,
the hypervisor considers that the guest image code address points to a
function that uses EABI calling convention and ensure that this
function will be called using the right parameters.

For example, if the guest code address is a function such as:

    void guest_code(int a, int b, int c);

then, this function can be hypervised using the following call:

    hypervise(guest_code, 3, 42, 43, 44);

the `guest_code()` function will then be called with 42, 43 and 44 for
parameters `a`, `b` and `c`, respectively.

The `hypervise()` function is defined in `hypervise.S` and saves the
current context (all general purpose registers, cpsr and spsr) and
call `hypervise_intern()`, which actually does the necessary
initialisation of the guest state before calling
`hypervisor_start()`. The context of the caller of the `hypervise()`
is saved in the `_hypervise_hypervisor_context` static in
`arm_variables.c` for the raspi platform. This context will be
restored when the guest code executes an `UDF` instruction. This is
how we simulate the guest code termination.

The `hypervisor_start()` function inits some internal flags and calls
`hypervisor_execute()` by passing the state pointer. Now, the main
hypervisor code kicks in.

Hypervisor main loop algorithm
------------------------------

The hypervisor algorithm is the following:

1. fetch the PC address from the guest state,
2. search the next instruction that needs arbitration by the
   hypervisor. This search is performed by the
   `findNextArbitrationAddress` function,
3. if an instruction that needs arbitration is found, it is saved and
   replaced by a branch to the hypervisor code. Actually, on ARM
   platform (and this would probably be needed for other platform as
   well), not only this instruction is modified, but also the next
   that is replaced by the actual hypervisor code address. This is
   needed because a long jump to the hypervisor can not be done by
   using a simple 32 bits intruction. The jump instruction is thus a
   `ldr pc, [pc, #-4]`. The return address is the one of the
   `hypervisor_handle_return` function,
4. call `platform.execute` to start the guest code. It will run
   normally until it reaches the instruction that needs arbitration
   and, because we patched it, jumps back to the hypervisor code in
   `hypervisor_handle_return`,
5. When returning from guest, we restore the modified instructions and
   check that the guest intructions will actually execute (because of
   conditional flags). Depending on the behavior of the instruction
   (that we obtain from the `platform.instructionInfo` function, we
   decide to let it run or not, or to execute something else. The
   function `do_runtime_jobs` does the trick and returns whether this
   instruction should be executed or the hypervisor has performed
   something to replace it. Then `hypervisor_execute` is called and
   we're back in `1.`

Exit from guest
---------------

The hypervisor can handle a special instruction in the guest image to
consider that it should exit. In the case of the ARM platform, this
special instruction is `UDF`.

When the analyser detects an `UDF`, instead of patching it with
`hypervisor_handle_return()`, it patches it with
`hypervisor_exit_from_guest()`. This function unpatch to restore the
UDF and restore the hypervisor context that was saved by `hypervise()`
using the `restore_hypervisor_context()` function. The control flow
then continues after the last call to `hypervise()`.


Guest API
---------

For bench purposes, the hypervisor provides two api calls to stop and
restart hypervision. These calls are located at fixed addresses
* `hypervise_stop()` at `0x9100`
* `hypervise_restart()` at `0x9000`

Of course, this API is for benchmarking and test purpose only and
should not be available for real applications.

### Stopping hypervision ###

When the analyzer detects the call to `hypervise_stop()` it simply
call `platform.execute()` with the correct state without patching.
Thus, it looses the control on the guest.

### Restarting hypervision ###

When the guest actually calls `hypervise_restart()`, it branches in
`arm_trampoline_hypervise_restart`. This code:
1. Saves the guest context in the current CPU Mode state
2. Set the PC of this state to the instruction following the
   `hypervise_restart()` call
3. sets a new stack for the hypervisor
4. disables the interrupts
5. branches in `hypervisor_execute` with the correct state.


Exceptions management 
---------------------

To manage exceptions, the first thing done by the hypervisor is to set
its own exception vector. On the ARM platforms, this is done by
modifying the VBAR register to point to the hypervisor's exception vector.

This vector is located neither at `0x00000000` nor at `0xffff0000`
where the guests will copy its own vector. Thus, when an exception
occurs, it is always the exception vector of the hypervisor that is
run.

When the hypervisor runs (analysis and runtime check), exceptions are
disabled. Thus, the only place when they can occur is when the guest
is executed **or** in the very few instructions where the hypervisor
saves or restores the guest state before disabling instruction or
after reenabling. Thus, when an exception occurs:

1. The corresponding vector of the hypervisor is called. It stores the
   guests corresponding exception vector address in r0 (after saving
   it) and LR in r1 (also after saving it). It then calls a generic
   handler
2. This handler checks if the exception occurs it the hypervisor code
   or in the guest code. For now, if the exception occurs in the
   hypervisor code, it just returns and ignores it. This **must** be
   changed later, because if the exception was caused by a peripheral,
   then doing nothing in the vector will cause the peripheral to
   trigger the exception again immediatly after exception
   return. Thus, the hypervisor will be stuck forever.
3. If the exception occurs in the guest code, the hypervisor runs the
   analysis on the guest exception vector. Thus this vector is run
   under hypervision. When the hypervisor detects a return from
   exception, it simply executes it normally. This is perfectly safe because:
    * Either the guest was in user mode. Then it is safe to let it run
      non hypervised.
    * Or it was preempted when under hypervision. In this case, the
      guest as **necessarily** be patched by the hypervisor and
      executing a *safe* part. Letting it run normally will trigger
      the branch to hypervisor and it will restart the analysis
      normally. Anyway, the hypervisor always knows if the code
      running in a particular mode has been patched and it releases
      only in that case.



Specifics of the Raspberry pi platform
======================================

Memory Map
----------

These addresses are defined by link script and are physical addresses
(MMU disabled at boot). The following memory map is used for raspberry
pi 1 platform.

    0x00008000: +-------------------------+
                |     Reset code          |
                |setups stacks and branch |
                | to bootstrap_main       |
                +-------------------------+
                |          ...            |
    0x00009000: +-------------------------+
                |  Guest callable API     |
                +-------------------------+
                |     Hypervisor code     |
                +-------------------------+
                |     Hypervisor data     |
                +-------------------------+
                |     Hypervisor bss      |
                +-------------------------+
                |          ...            |
    0x00508000  +-------------------------+
                |    Guest code           |
                +-------------------------+
                |          ...            |
    0x20200000: +-------------------------+
                |     GPIO Configuration  |
                |         Registers       |
    0x20201000: +-------------------------+
                |     UART Configuration  |
                |         Registers       |
                +-------------------------+

For raspberry pi 2, the peripheral configuration registers are mapped
at `0x3B000000` thus, the GPIO configuration registers are at `0x3B200000`.

Boot process
------------

1. At power on, the ARM processor is not powered, but the GPU is,
2. The GPU executes a first bootloader located in ROM that loads the
   `bootcode.bin` file from the SD Card in the cache.
3. This bootloader powers on the SDRAM, loads the `start.elf` into
   RAM from the SD at address `0x0` and the GPU runs it.
4. The `start.elf` reads `config.txt` and `cmdline.txt`. It then loads
   `kernel.img` at address `0x8000` and sets the ARM CPU to execute
   it.
5. In the hypervisor, the code from file `boot/reset.S` is loaded at
   address `0x8000`. This code inits supervisor stack as well as IRQ
   stack then jumps in C code at `bootstrap_main` function of file
   `boot/bootstrap.c`.


ARM  dependent initialization
-----------------------------

This initialization is performed by the call to `platform.init()`
within the `hypervisor_init_platform` function. The purpose of this
function is to finish platform dependent initialization. At the time
these operations are:

1. zero the `ARMMode_Supervisor` state
2. copy the exception vector defined in `arm_exception_vector.S` to
   address `0x0`. This address holds the real exception vector. So
   this code installs our exception vector.

After Hypervisor has been initialized, it can start the guest by
calling `hypervisor_start` with the address of the state of the guest
code to run. The state should then be initialized before calling the
function. In particular, the PC address of the state have to be set to
the address of the guest code. When the hypervisor starts, it will
start analyzing guest code at this address then restore the CPU state
to execute the guest. It is then important that the state is
consistent with the guest system expectations (value of `r0`, `r1` and
`r2` for starting linux for example)



GOTCHAs
=======

This section lists a bunch of things of which we are aware that a
developer must pay attention to:

* The `ARM_SavedState` structure is used in assembly files where the offset are hard coded.
  When modifying the function, be sure to update the offsets in `arm_trampoline.S`

LICENSE
=======

This software is released under CeCILL license. It also uses some code
written by others. Care have been taken to respect and respectfully
cite code owners of the part we did not write. However, some
references may have not been properly cited. If you find piece of your
own code which is not correctly referenced, please contact us so that
we can fix it and properly cite your work.

