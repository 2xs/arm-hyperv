/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __DEBUG_HEADER__
#define __DEBUG_HEADER__


#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int debug_func(const char *function, const char* fmt, ...) ;
int strong_debug_func(const char *function, const char* fmt, ...) ;


#define debug(...) debug_func(__func__, __VA_ARGS__)
#define strong_debug(...) strong_debug_func(__func__, __VA_ARGS__)

#include "hypervisor.h"

#ifndef DISABLE_ASSERT

#ifdef LIGHT_DEBUG
/* Use debug macro for assertion in debug mode */
#define assert(cond) do {                                               \
      if (!(cond)) {                                                    \
         debug("Assertion %s failed in %s:%d\n", #cond, __FILE__, __LINE__); \
         hypervisor_current_platform()->stop();                         \
      }                                                                 \
   }while(0)
#else
/* Use printf for assertion in release/nooptim mode */
#include "printf.h"
#define assert(cond) do {                                               \
      if (!(cond)) {                                                    \
         printf("Assertion %s failed in %s:%s:%d\n", #cond, __FUNCTION__, __FILE__, __LINE__); \
         hypervisor_current_platform()->stop();                         \
      }                                                                 \
   }while(0)
#endif /* LIGHT_DEBUG */

#else

#define assert(ignore) ((void)0)

#endif /* DISABLE_ASSERT */

/* Some defines to use as failing conditions in assertions. This
 * allows a clear message instead of 'Assertion 0 failed' */
#define NOT_IMPLEMENTED 0
#define NO_DANGEROUS_INSTRUCTION_FOUND 0

#endif /*__DEBUG_HEADER__*/
