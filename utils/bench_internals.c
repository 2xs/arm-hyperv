/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "bench_internals.h"
#include "cache.h"

#ifdef BENCH_INTERNALS
uint32_t bench_variables[BENCH_VARIABLES_COUNT];

static const char * const bench_var_names[] =
{
   "CACHE_LOOKUP",
   "CACHE_HIT",
   "CACHE_MISS",
   "CACHE_CLEAR",
   "CACHE_UPDATE",
   "CACHE_COLLISION",
   "CACHE_FIND_ITER",
   "CACHE_FIND_COUNT",
   "ANALYSE_ITER_SUM",
   "ANALYSE_BLOCK_SIZE_SUM",
   "ANALYSE_COUNT",
   "HANDLE_RETURN",
   "INSTRUCTION_INFO_LOOP_SUM",
   "INSTRUCTION_INFO_LOOP_COUNT",
   "INSTRUCTION_INFO_COUNT",
   "OPTIMIZATION_FAILED",
   "OPTIMIZATION_BCC_FAILED",
   "OPTIMIZATION_CACHE_LOOKUP",
   "OPTIMIZATION_CACHE_UPDATE",
   "OPTIMIZATION_LOOP_ITSELF",
   "OPTIMIZATION_LOOP_ITSELF_OPT",
};
static int bench_var_names_count = sizeof(bench_var_names)/sizeof(bench_var_names[0]);

static const struct {
   int sum_idx, count_idx;
} bench_averages[] = {
   {ANALYSE_ITER_SUM, ANALYSE_COUNT},
   {ANALYSE_BLOCK_SIZE_SUM, ANALYSE_COUNT},
   {INSTRUCTION_INFO_LOOP_SUM, INSTRUCTION_INFO_LOOP_COUNT},
   {CACHE_FIND_ITER, CACHE_FIND_COUNT},
};

#define VAR_NAME(var) (((var) < bench_var_names_count) ? bench_var_names[(var)] : "UNDEFINED VARIABLE NAME")

static int bench_averages_count = sizeof(bench_averages)/sizeof(bench_averages[0]);
#include "printf.h"
void bench_print_variables(void)
{
   int i;

   printf("STATS: Raw values\n");
   for (i = 0 ; i < BENCH_VARIABLES_COUNT ; ++i)
   {
      printf("STATS: %s: %d\n", VAR_NAME(i), bench_variables[i]);
   }
   printf("STATS: Averages\n");
   for (i = 0 ; i < bench_averages_count ; ++i)
   {
      printf("STATS: AVG: %s: %d\n", VAR_NAME(bench_averages[i].sum_idx), BENCH_AVG(bench_averages[i].sum_idx, bench_averages[i].count_idx));
   }
   cachePrintStats();
}

#endif
