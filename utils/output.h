/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __OUTPUT_H__
#define __OUTPUT_H__

#ifdef LOG_LEVEL
#include "printf.h"

extern int global_log_level;

extern void log_func(const char *level, const char *function, const char *format, ...);

#define log(level,...) do {if ((level) & global_log_level) log_func(#level, __func__, __VA_ARGS__);}while(0)
#else
#define log(...) do {} while(0)
#endif

#define LOG_OPTIMIZER          (1)
#define LOG_ANALYSER           (1 << 1)
#define LOG_CACHE_HIT          (1 << 2) /**< cache hit logs */
#define LOG_CACHE_MISS         (1 << 3) /**< cache miss logs */
#define LOG_CACHE_UPDATE       (1 << 4) /**< cache update logs */
#define LOG_CACHE_INFO         (1 << 5) /**< general cache informations */
#define LOG_HYPERVISOR         (1 << 6)
#define LOG_INSTRUCTION_INFO   (1 << 7)
#define LOG_BEHAVIOR           (1 << 8)
#define LOG_PATCH              (1 << 9)
#define LOG_CACHE_TEMPLATE     (1 << 10)


#endif
