/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __BENCH_INTERNALS_H__
#define __BENCH_INTERNALS_H__

#include <stdint.h>

typedef enum
{
   CACHE_LOOKUP, /* How many calls to cache */
   CACHE_HIT,    /* How many hits */
   CACHE_MISS,   /* How many misses */
   CACHE_CLEAR,  /* How many calls to cacheClear */
   CACHE_UPDATE, /* How many calls to cacheUpdate */
   CACHE_COLLISION, /* How many collisions when updating */
   CACHE_FIND_ITER, /* How many iteration made when querying the cache */
   CACHE_FIND_COUNT, /* How many calls to cacheFindEntry */
   ANALYSE_ITER_SUM, /* The sum of the number of analyse iteration before an abitration point is reached */
   ANALYSE_BLOCK_SIZE_SUM, /* The sum of the analyzed block size */
   ANALYSE_COUNT,    /* How many calls to findNextArbitration */
   HANDLE_RETURN, /* How many time we do hypervisor call */
   INSTRUCTION_INFO_LOOP_SUM, /* Sum of the number of instruction info loop iteration (to find the right table entry) */
   INSTRUCTION_INFO_LOOP_COUNT, /* How many calls to instruction_info enters the find loop */
   INSTRUCTION_INFO_COUNT, /* How many calls to instruction_info */
   OPTIMIZATION_FAILED, /* How many calls to optimize failed */
   OPTIMIZATION_BCC_FAILED, /* How many calls to optimize in case of bcc failed */
   OPTIMIZATION_CACHE_LOOKUP, /* How many calls to cache_lookup is made by optimize function */
   OPTIMIZATION_CACHE_UPDATE, /* How many calls to cahce_update is made by optimize function */
   OPTIMIZATION_LOOP_ITSELF, /* How many time a loop condition loop itself */
   OPTIMIZATION_LOOP_ITSELF_OPT, /* How many we optimize self looping condition */
   BENCH_VARIABLES_COUNT,
} bench_value_t;

#ifdef BENCH_INTERNALS

extern uint32_t bench_variables[BENCH_VARIABLES_COUNT];
extern void bench_print_variables(void);

#define BENCH_CLR(var)     do { bench_variables[(var)] = 0; } while (0)
#define BENCH_INC(var)     do { bench_variables[(var)]++; } while (0)
#define BENCH_ADD(var,val) do { bench_variables[(var)] += (val); } while (0)
#define BENCH_SUB(var,val) do { bench_variables[(var)] -= (val); } while (0)
#define BENCH_AVG(sum,count) (((count) > 0) ? (bench_variables[(sum)] / bench_variables[(count)]) : 0)
#define BENCH_DEC(var)     do { bench_variables[(var)]--; } while (0)
#define BENCH_SET(var,val) do { bench_variables[(var)] = (val); } while (0)
#define BENCH_VAL(var)     bench_variables[(var)]

#else

#define BENCH_CLR(var)    
#define BENCH_INC(var)    
#define BENCH_ADD(var,val)    
#define BENCH_SUB(var,val)    
#define BENCH_AVG(sum, count)    
#define BENCH_DEC(var)    
#define BENCH_SET(var,val)

#endif

#endif /* __BENCH_INTERNALS_H__ */
