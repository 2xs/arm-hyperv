/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "types.h"
#include "cache-template.h"
#include "output.h"


static inline uint16_t hash(uint32_t key)
{
   key >>= 2;
   return (key ^ (key >> HASH_SIZE_IN_BITS) ^ (key >> (HASH_SIZE_IN_BITS << 1))) & (CACHE_SIZE -1);
}

#define KEY_TO_IDX(a) hash((a))

/*******************/
/* Cache functions */
/*******************/


/* Find a suitable entry for key in the cache.
   If insert is true, then returns a suitable entry for updating
   If insert is false, it returns -1 if key is not in cache
*/
static int cache_find_entry(const cache_entry_t *cache, uint32_t key, int insert)
{
   const int cache_depth_search = 5;
   uint32_t hash_source = key;
   int lookup;
   for (int i = 0 ; i < cache_depth_search ; ++i, hash_source += 0x37824AFC)
   {
      lookup = KEY_TO_IDX(hash_source);
      if (!cache[lookup].used && insert)
         return lookup;
      if (cache[lookup].used && cache[lookup].key == key)
         return lookup;
      /* the key is not directly in the cache, try to find a new place */
   }
   if (!insert) /* cache miss */
      return -1;
   /* If one key must be discarded, let it be the one that will be found faster 
      
      This decision is arguable and may be changed later

    */
   return KEY_TO_IDX(key); 
}

/* Lookup function */
const cache_value_t *cache_lookup(const cache_entry_t *cache, uint32_t key)
{
    
   int lookup = cache_find_entry(cache, key, 0);
   
   if (lookup != -1)
   {
      log(LOG_CACHE_TEMPLATE, "lookup %p, hit\n");      
      return &cache[lookup].value;
   }
   log(LOG_CACHE_TEMPLATE, "lookup %p, miss\n");
   return NULL;
}

void cache_update(cache_entry_t *cache, uint32_t key, const cache_value_t *value)
{
   uint32_t lookup = cache_find_entry(cache, key, 1);
   cache[lookup].used = 1;
   cache[lookup].key = key;
   cache[lookup].value = *value;
   log(LOG_CACHE_TEMPLATE, "update %p\n", key);
}

void cache_clear(cache_entry_t *cache, uint32_t key)
{
   int lookup = cache_find_entry(cache, key, 0);
   if (lookup != -1)
      cache[lookup].used = 0;
}


void cache_print_stats(const cache_entry_t *cache)
{
   int i;
   int used = 0;
   log(LOG_CACHE_TEMPLATE,"Cache content:\n");
   log(LOG_CACHE_TEMPLATE,"+--------------+------------+-------------+\n");
   log(LOG_CACHE_TEMPLATE,"|key           |value (int) |value (addr) |\n");
   log(LOG_CACHE_TEMPLATE,"+--------------+------------+-------------+\n");
   for (i = 0 ; i < CACHE_SIZE ; ++i)
   {
      if (cache[i].used)
      {
         log(LOG_CACHE_TEMPLATE,"|%13x |%11d |%12x |", cache[i].key, cache[i].value.integer, cache[i].value.address);
         log(LOG_CACHE_TEMPLATE, "\n");
         used++;
      }
   }
   log(LOG_CACHE_TEMPLATE, "%d/%d entries used\n", used, CACHE_SIZE);
}


