/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "uart.h"
#include "printf.h"
#include "strlen.h"

#define NULL ((void *) 0)

unsigned long long __udivmoddi4(unsigned long long num, unsigned long long den, unsigned long long * rem_p)
{
	unsigned long long quot = 0, qbit = 1;

	if (den == 0)
	{
		return 0;
	}

	/*
	 * left-justify denominator and count shift
	 */
	while ((signed long long) den >= 0)
	{
		den <<= 1;
		qbit <<= 1;
	}

	while (qbit)
	{
		if (den <= num)
		{
			num -= den;
			quot += qbit;
		}
		den >>= 1;
		qbit >>= 1;
	}

	if (rem_p)
		*rem_p = num;

	return quot;
}

int itoa(long value, char *out, int base, int is_signed, int zero_pad, int force_length) {
    const char *l_base = "0123456789abcdefghijklmnopqrstuvwxyz";

    if (is_signed && value < 0) {
        *out++ = '-';
        value = -value;
    }

    int cnt = 0;
    int cnt_limit = 8;
    if (base == 10) cnt_limit = 10;
    if (base == 2) cnt_limit = 32;

    long c_value = value;
    while (c_value && cnt < cnt_limit) {
        c_value = __udivmoddi4((unsigned long long) c_value, (unsigned long long) base, NULL);
        cnt++;
    }
    if (cnt == 0) cnt++;

    if (force_length && cnt < force_length) {
        int i;
        char pad = (zero_pad ? '0' : ' ');
        for (i=cnt; i<force_length; i++) {
            *out++ = pad;
        }
    }

    out += cnt;
    *out = 0;

    unsigned long long r=0;
    while (cnt--) {
        value = __udivmoddi4((unsigned long long) value, (unsigned long long) base, &r);
        out--;
        *out = l_base[r];
    }

    return cnt;
}

int vprintf(const char *fmt, va_list args) {
    static char nb[80];

    int len = 0;

    while (*fmt) {
        switch (*fmt++) {
            case '%':
            {
                int first_digit = 1;
                int zero_pad = 0;
                int force_length = 0;
                int has_fmt = 0;

                while (!has_fmt && *fmt) {
                    switch (*fmt++)
                    {
                        case '0':
                            if (first_digit) {
                                first_digit = 0;
                                zero_pad = 1;
                                break;
                            }

                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            force_length = force_length * 10 + (fmt[-1] - '0');
                            break;

                        case '%':
                            uart_putc('%');
                            len++;
                            has_fmt = 1;
                            break;
                        case 'b':
                            itoa(va_arg(args, long), nb, 2, 0, zero_pad, force_length);
                            uart_puts(nb);
                            len += strlen(nb);
                            has_fmt = 1;
                            break;
                        case 'c':
                            uart_putc((char)va_arg(args, int));
                            len++;
                            has_fmt = 1;
                            break;
                        case 'p':
                            uart_puts("0x");
                            len += 2;
                        case 'x':
                        case 'X':
                            itoa(va_arg(args, long), nb, 16, 0, zero_pad, force_length);
                            uart_puts(nb);
                            len += strlen(nb);
                            has_fmt = 1;
                            break;
                        case 'u':
                            itoa(va_arg(args, long), nb, 10, 0, zero_pad, force_length);
                            uart_puts(nb);
                            len += strlen(nb);
                            has_fmt = 1;
                            break;
                        case 'd':
                            itoa(va_arg(args, long), nb, 10, 1, zero_pad, force_length);
                            uart_puts(nb);
                            len += strlen(nb);
                            has_fmt = 1;
                            break;
                        case 's':
                        {
                            const char *str = va_arg(args, const char *);
                            uart_puts(str);
                            len += strlen(str);
                            has_fmt = 1;
                            break;
                        }

                        default:
                            has_fmt = 1;
                            break;
                    }
                }
                break;
            }

            default:
                uart_putc(fmt[-1]);
                len++;
        }
    }

    return len;
}

int printf(const char* fmt, ...) {
    va_list args;
    va_start (args, fmt);
    int len = vprintf (fmt, args);
    va_end (args);
    return len;
}
