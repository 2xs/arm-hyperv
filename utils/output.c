/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifdef LOG_LEVEL
#include <stdarg.h>
#include "printf.h"
int global_log_level = LOG_LEVEL;

#define LINE_HEADER(HEADER, level, function) {                   \
      static int print_line_header = 1;                         \
      static const char *last_func = 0;                         \
      if (print_line_header)                                    \
      {                                                         \
         if (last_func != function)                             \
            /* ptr comparison shoud be ok for our use */        \
         {                                                      \
            printf(HEADER" \e[31mIn %s\n", level, function);    \
            last_func = function;                               \
         }                                                      \
         printf(HEADER, level);                                 \
         printf("\e[39m");                                      \
         print_line_header = 0;                                 \
      }                                                         \
      int i;                                                    \
      for (i = 0 ; fmt[i] ; ++i)                                \
         if (fmt[i] == '\n')                                    \
            print_line_header = 1;                              \
                                                                \
   } while(0)                                   


int log_func(const char *level, const char *function, const char* fmt, ...)
{
   LINE_HEADER("\e[32m %s:", level, function);
   va_list args;
   va_start (args, fmt);
   int len = vprintf (fmt, args);
   va_end (args);
   return len;
}

#endif
