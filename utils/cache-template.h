/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __CACHE_TEMPLATE_H__
#define __CACHE_TEMPLATE_H__

#include <stdint.h>
#include "platform.h"

typedef union
{
   int integer;
   addr_t address;
   void *thing;
} cache_value_t;

typedef struct
{
   int used;
   uint32_t key;
   cache_value_t value;
} cache_entry_t;

#define HASH_SIZE_IN_BITS (10)
#define CACHE_SIZE (1 << HASH_SIZE_IN_BITS)


/** Looks up for a entry in the cache.

    Returns a pointer to the cache entry value if found, NULL otherwise

    @param key the key to lookup
    @param cache pointer to the cache
*/
const cache_value_t *cache_lookup(const cache_entry_t *cache, uint32_t key);

/** Updates a cache entry.
    If a collision occurs, the previous entry is discarded
*/
void cache_update(cache_entry_t *cache, uint32_t key, const cache_value_t *val);


/** clears a cache entry (erase it). */
void cache_clear(cache_entry_t *cache, uint32_t key);

/** print cache usage statistics */
void cache_print_stats(const cache_entry_t *cache);


#endif
