This prototype has been initiated by François Serman for its PhD Thesis.

# Main contributors

* François Serman
* Michaël Hauspie <michael.hauspie@univ-lille1.fr> (corresponding author)

# Additional contributors
* Vincent Benony
* Pierre Graux
* Clément Boin
* Gilles Grimaud
* Vlad Rusu (for coq proofs)
