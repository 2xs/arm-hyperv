/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "hypervisor.h"
#include "memset.h"
#include "debug.h"
#include "memcpy.h"
#include "abort.h"
#include "debug.h"
#include "hypervisor_debug.h"
#include "output.h"

Hypervisor_Platform platform;


void hypervisor_init_platform(const Hypervisor_Platform * p) {
   /* Copy the platform to a global holding the current platform */
   memcpy(&platform, p,  sizeof(Hypervisor_Platform) );
   /* Call platform specific initialization */
   platform.init();
}

Hypervisor_Platform * hypervisor_current_platform() {
    return &platform;
}

static void hypervisor_display_configuration(void)
{
   printf("HYPERV: Hypervisor configuration\n");
#ifndef LOG_LEVEL
   printf("HYPERV: No log output\n");
#else
   printf("HYPERV: Log level: %d\n", LOG_LEVEL);
#endif
   printf("HYPERV: Analyser cache: ");
#ifdef USE_CACHE
   printf("enabled\n");
#else
   printf("disabled\n");
#endif
   printf("HYPERV: Analyser cache optimization: ");

#ifdef NOCACHE_OPTIMIZATION
   printf("disabled\n");
#else
   printf("enabled\n");
#endif
   printf("HYPERV: Instruction identification cache: ");
#ifdef NO_INSTRUCTION_CACHE
   printf("disabled\n");
#else
   printf("enabled\n");
#endif
}

void NORETURN hypervisor_start(void * state)
{
   int i;

   hypervisor_display_configuration();

   for(i=0;i<NB_BEHAVIOR;i++) {
      platform.behavior_runtime_functions[i] = 0x0;
   }

   log(LOG_HYPERVISOR, "start at %p\n", platform.getPCValue(state));


   hypervisor_execute(state, 0);
}
