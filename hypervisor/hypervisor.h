/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __HYPERVISOR_H__
#define __HYPERVISOR_H__

#include "platform.h"
#include "analyse.h"
#include "cache.h"
#include "runtime.h"

/** Returns the current platform.
 */
Hypervisor_Platform * hypervisor_current_platform();

/** Inits the hypervisor and call hardware specific initializations.
    
    @note the platform structure is copied into a global. Thus, 
    the platform sent as parameter to this function must not be used
    later on. If you need to access the platform later, use the
    ::hypervisor_current_platform() function.
 */
void hypervisor_init_platform(const Hypervisor_Platform * platform);


/** Runs the hypervisor */
void NORETURN hypervisor_start(void * state);

#endif
