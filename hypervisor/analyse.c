/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "hypervisor.h"
#include "debug.h"
#include "output.h"
#include "hypervisor_debug.h"

#include "bench_internals.h"
#ifdef BENCH_INTERNALS
    #define DEBUG_LOG_OPTIMIZATION_FAILED BENCH_INC(OPTIMIZATION_FAILED)
    #define DEBUG_LOG_OPTIMIZATION_BCC_FAILED BENCH_INC(OPTIMIZATION_BCC_FAILED)
    #define DEBUG_LOG_OPTIMIZATION_CACHE_LOOKUP BENCH_INC(OPTIMIZATION_CACHE_LOOKUP)
    #define DEBUG_LOG_OPTIMIZATION_CACHE_UPDATE BENCH_INC(OPTIMIZATION_CACHE_UPDATE)
    #define DEBUG_LOG_HANDLE_RETURN BENCH_INC(HANDLE_RETURN)
    #define DEBUG_LOG_OPTIMIZATION_LOOP_ITSELF BENCH_INC(OPTIMIZATION_LOOP_ITSELF)
    #define DEBUG_LOG_OPTIMIZATION_LOOP_ITSELF_OPT BENCH_INC(OPTIMIZATION_LOOP_ITSELF_OPT)
#else
    #define DEBUG_LOG_OPTIMIZATION_FAILED do{}while(0)
    #define DEBUG_LOG_OPTIMIZATION_BCC_FAILED do{}while(0)
    #define DEBUG_LOG_OPTIMIZATION_CACHE_LOOKUP do{}while(0)
    #define DEBUG_LOG_OPTIMIZATION_CACHE_UPDATE do{}while(0)
    #define DEBUG_LOG_HANDLE_RETURN do{}while(0)
    #define DEBUG_LOG_OPTIMIZATION_LOOP_ITSELF do{}while(0)
    #define DEBUG_LOG_OPTIMIZATION_LOOP_ITSELF_OPT do{}while(0)
#endif

extern Hypervisor_Platform platform;

int is_sensitive_instruction(const Hypervisor_InstrInfo *info){
   if( (info->behavior & Behavior_Branches)        ||
       (info->behavior & Behavior_Link)            ||
       (info->behavior & Behavior_OddBranch)       ||
       (info->behavior & Behavior_ReadsCopro)      ||
       (info->behavior & Behavior_WritesCopro)     ||
       (info->behavior & Behavior_RaisesException) ||
       (info->behavior & Behavior_WritesPsr)       ||
       (info->behavior & Behavior_Unsuported))
      return 1;
   return 0;
}


#if !defined(NOCACHE_OPTIMIZATION)
/** This function performs optimization of the values in cache 
    by tracking loops that do not contain sensitive instructions

    @param state the current state of the CPU at the time of the analysis
    @param dst  the address at which to try to optimize the cache

    @returns
*/

enum cache_optimizer_action
{
   CACHE_OPTIMIZER_LET_GUEST_RUN,   /* Cache has detected that the guest can be run without control */
   CACHE_OPTIMIZER_CACHE_OPTIMIZED, /* Cache has been optimized, must re-run the last query */
   CACHE_OPTIMIZER_NOTHING,         /* Cache optimizer did nothing */
};

enum cache_optimizer_action performUnconditionalBranchOptimization(Hypervisor_InstrInfo *info, addr_t start, addr_t dst)
{
   /* Query the cache to know where the next sensitive information is from the destination of the branch */
   unsigned int idx = 0;
   addr_t newDst = cacheLookup(info->pc_destination, &idx);
   DEBUG_LOG_OPTIMIZATION_CACHE_LOOKUP;
   
   if (newDst)
   {
      if (newDst == dst)
      {
         /* Here we detect an infinite loop such as:
            start:
            instr1
            instr2
            ...
            instrN
            dst:  b start ; (with newDst == dst)
            
            We can let the hypervisor run the guest
            unmodified because none of the instr1... instrN
            is dangerous
            One solution to this is to set the next patch to one instruction after dst
         */
         log(LOG_OPTIMIZER, "Found an infinite loop without dangerous instruction from %p to %p.\n", start, dst);
         return CACHE_OPTIMIZER_LET_GUEST_RUN; 
      }
      
      log(LOG_OPTIMIZER, "Found optimization in %s about  %p -> %p => %p -> %p (%d)\n", __FUNCTION__, start, dst, start, newDst, idx);
      cacheUpdate(start, newDst, idx);
      DEBUG_LOG_OPTIMIZATION_CACHE_UPDATE;
      
      /* Start analysis at the new analysis point */
      return CACHE_OPTIMIZER_CACHE_OPTIMIZED;
   }
   
   return CACHE_OPTIMIZER_NOTHING;
}

enum cache_optimizer_action performConditionalBranchOptimization(void *state, Hypervisor_InstrInfo *info, addr_t start, addr_t dst)
{
   unsigned int idx = 0;
   addr_t cnd1Dst = cacheLookup(info->pc_destination, &idx);
   addr_t cnd2Dst;
   DEBUG_LOG_OPTIMIZATION_CACHE_LOOKUP;
   
   /* If the cache has already analyzed this address */
   if (cnd1Dst)
   {
      /* Looking for instruction in case of failed branch */
      /* cnd2Dst not known yet, analyse to detect it */
      log(LOG_OPTIMIZER, "from %p: instruction at %p branches at %p(next %p), launching analyzis for failed branch at %p ", start, dst, info->pc_destination, cnd1Dst, dst + 4);
      
      if (findNextArbitrationAddress(state, (dst + 4), &cnd2Dst, MAX_ITER))
      {
         log(LOG_OPTIMIZER, "analysis result: %p\n", cnd2Dst);
         Hypervisor_InstrInfo info_dst;
         idx = platform.instructionInfo(state, cnd2Dst, &info_dst, 0, 0);
      }
      else
         assert(NOT_IN_GUEST);

      /* If cnd1Dst and cnd2Dst are identical, then we make a shorcut between start and cnd2Dst */
      if (cnd1Dst == cnd2Dst)
      {
         log(LOG_OPTIMIZER, "Found optimization in %s about %p => %p (%d)\n", __FUNCTION__, start, cnd2Dst, idx);
         cacheUpdate(start, cnd2Dst, idx);
         DEBUG_LOG_OPTIMIZATION_CACHE_UPDATE;
         return CACHE_OPTIMIZER_CACHE_OPTIMIZED;
      }
      /* Otherwise, the conditional branch loop on itself and if output block has aleary been analyzed. We can make the same shortcut. */
      else if (cnd1Dst == dst)
      {
         DEBUG_LOG_OPTIMIZATION_LOOP_ITSELF;
         if (cnd2Dst)
         {
            DEBUG_LOG_OPTIMIZATION_LOOP_ITSELF_OPT;
            log(LOG_OPTIMIZER, "Found optimization in %s about %p => %p (%d)\n", __FUNCTION__, start, cnd2Dst, idx);
            cacheUpdate(start, cnd2Dst, idx);
            DEBUG_LOG_OPTIMIZATION_CACHE_UPDATE;
            return CACHE_OPTIMIZER_CACHE_OPTIMIZED;
         }
      } 
      /* if the failed case was already alanzed we can make the same shortcut in oposite way */
      else if (cnd2Dst == dst)
      {
         DEBUG_LOG_OPTIMIZATION_LOOP_ITSELF;
         
         if (cnd1Dst)
         {
            DEBUG_LOG_OPTIMIZATION_LOOP_ITSELF_OPT;
            log(LOG_OPTIMIZER, "Found optimization in %s about %p => %p (%d)\n", __FUNCTION__, start, cnd1Dst, idx);
            cacheUpdate(start, cnd1Dst, idx);
            DEBUG_LOG_OPTIMIZATION_CACHE_UPDATE;
            return CACHE_OPTIMIZER_CACHE_OPTIMIZED;
         }
      }
   }

   return CACHE_OPTIMIZER_NOTHING;
}

enum cache_optimizer_action performSubroutineOptimization(void * state, Hypervisor_InstrInfo *info, addr_t start, addr_t dst)
{
   unsigned int index;
   addr_t newDst = cacheLookup(info->pc_destination, &index);

   /* If newDst is a BX LR */
   Hypervisor_InstrInfo newDst_info;
   index = platform.instructionInfo(state, newDst, &newDst_info, index, 0);

   /* Compute the opcode of bx instruction */
   const uint32_t opcode = (Cond_AL << 28) | (0x12FFF1 << 4) | 14;

   /* If we met a bx lr*/
   if (newDst_info.instr == opcode)
   {
      index = platform.instructionInfo(state, (dst + 4), &newDst_info, 0, 0);
      log(LOG_OPTIMIZER, "Found optimization in %s about %p => %p\n", __FUNCTION__, start, dst+4);
      addr_t found;
      if (findNextArbitrationAddress(state, (dst + 4), &found, MAX_ITER)) {
         cacheLookup(found, &index);
         cacheUpdate(start, found, index);
      }
      else
         assert(NOT_IN_GUEST);
   }

   return CACHE_OPTIMIZER_NOTHING;
}

enum cache_optimizer_action performCacheOptimization(void *state, addr_t start, addr_t dst, unsigned int idx)
{
    Hypervisor_InstrInfo dst_info;
    /* Get information about the instruction at dst */
    idx = platform.instructionInfo(state, dst, &dst_info, idx, 0);

    if (dst_info.behavior == Behavior_Branches)
    {
       if (dst_info.condition == Cond_AL)
          return performUnconditionalBranchOptimization(&dst_info, start, dst);
       
       return performConditionalBranchOptimization(state, &dst_info, start, dst);
    }

    /* If we met a branch with link */
    if ((dst_info.behavior & (Behavior_Branches | Behavior_Link)) == (Behavior_Branches | Behavior_Link))
    {
       performSubroutineOptimization(state, &dst_info, start, dst);
    }
    
    /* Do not change anything */
    return CACHE_OPTIMIZER_NOTHING;
}

#else // NOCACHE_OPTIMIZATION
#warning "Compiling without cache optimization"
#endif // CACHE_OPTIMIZATION

/* 
*/
int findNextArbitrationAddress(void * state, addr_t start, /* out */ addr_t * found, const int max_iter) {
    Hypervisor_InstrInfo info;
    unsigned int index = 0;
    addr_t a = 0;

#if defined(USE_CACHE)
    a = cacheLookup(start, &index);
#endif // defined(USE_CACHE)

    if (!a)
       a = start;

    BENCH_INC(ANALYSE_COUNT);
    for (int iter = 0 ; iter < max_iter ; ++iter) {

       if( ( index = platform.instructionInfo(state, a, &info, 0, 0))){

          if(is_sensitive_instruction(&info)){
             *found = a;
#ifdef USE_CACHE
             cacheUpdate(start, a, index);
#endif
             log(LOG_ANALYSER, "Found sensitive instruction at %p when analyzing from %p\n", a, start);
           
             BENCH_ADD(ANALYSE_ITER_SUM, iter);
             return 1;
          }
       }
       a += info.length;
    }
    return 0;
}

void guest_static_analysis(int mode, int pass_count)
{
#ifdef USE_CACHE // Static analysis works only if there is cache
   extern unsigned char guest_image_end;
   extern unsigned char guest_image_start;
   addr_t start = (addr_t) &guest_image_start;
   addr_t end = (addr_t) &guest_image_end;
   void *state = platform.stateForMode(mode);

   /* First populate the cache with initial analysis */
   start = (addr_t) &guest_image_start;
   while (start < end)
   {
      addr_t found = 0;
      findNextArbitrationAddress(state, start, &found, 10);
      start = platform.get_next_instr(start);
   }

#if !defined(NOCACHE_OPTIMIZATION)
   /* Now optimize the cache */
   int cache_optimized = 1;

   for (int i = 0 ; cache_optimized && i < pass_count ; ++i)
   {
      log(LOG_OPTIMIZER, "Static cache optimisation, pass %d/%d", i, pass_count);
      cache_optimized = 0;
      start = (addr_t)&guest_image_start;
      while (start < end)
      {
         unsigned int index;
         addr_t a = cacheLookup(start, &index);
         if (a)
         {
            if (performCacheOptimization(state, start, a, index) !=  CACHE_OPTIMIZER_NOTHING)
               cache_optimized = 1;
         }
         start = platform.get_next_instr(start);
      }
   }
#endif // NOCACHE_OPTIMIZATION
#endif // USE_CACHE
}

int is_in_guest(addr_t addr)
{
/* Needed for analysis assertion */
   extern unsigned char guest_image_end;
   extern unsigned char guest_image_start;
   /* Guest code is either in exception vector ([0 .. 0x1C])
      or inside guest_image */
   if (addr <= 0x1C) /* addr is unsigned, so testing the upper bound is enough */
      return 1;
   if ((addr >= (addr_t) &guest_image_start) && (addr < (addr_t)&guest_image_end))
      return 1;
   printf("Warning, %p is not in guest image ([%p-%p])\n", addr, &guest_image_start, &guest_image_end);
   return 0;
}

int is_in_hypervisor(addr_t addr)
{
   extern uint32_t _text_start;
   extern uint32_t _text_end;
   if ((addr >= (addr_t) &_text_start) && (addr < (addr_t)&_text_end))
      return 1;
   return 0;
}

void NORETURN hypervisor_execute(void * state, int skip_first) {

   /* get the REAL current state depending on the new mode.  This part
      is a bit tricky, let's explain (This is valid for ARM, but the
      trick would be almost the same on another platform):

      When the hypervisor detects a change in CPU mode (msr on cpsr or
      cps), it patches it and just before executing it does a good
      copy from the state of the previous mode to the new mode.  

      The parameter state is, at this point, the state of the mode
      BEFORE the msr or cps. Thus not the state that will be activated
      after the actual execution of the CPS. When copying the states
      (done the the behavior function), the hypervisor sets the new
      mode value in a field of the OLD state. The only purpose of
      this is to get this value when entering this function.

      After retrieving the mode in which the CPU will be when the
      hypervisor will let it execute, we must get the state for this
      mode so that the analyzer uses the good state for analysis (that
      is, the state in which the CPU will actually run the analyzed
      code until the next arbitrationAddress)
    */
   log(LOG_ANALYSER, "Starting analysis with state %p\n", state);
    int mode = platform.modeIndex(state);
    int mode_next_cpu_mode = platform.destinationMode(state);
    log(LOG_ANALYSER, "Code to analyse at %p, skip_first = %d. saved mode is %d\n", platform.getPCValue(state), skip_first, mode);
    assert(mode != 0);
    if (mode_next_cpu_mode != 0 && mode_next_cpu_mode != mode)
    {
       log(LOG_ANALYSER, "The code to analyse will execute in %d mode, change state\n", mode_next_cpu_mode);
       mode = mode_next_cpu_mode;
       state = platform.stateForMode(mode);
       log(LOG_ANALYSER, "New state %p for mode %d\n", state, mode);
    }
 
    addr_t start = platform.getPCValue(state);    

    /* Asserts that the analyser analyses the guest code */
    assert(is_in_guest(start));

    if (skip_first) {
       Hypervisor_InstrInfo info;
       platform.instructionInfo(state, start, &info, 0, 0);
       start += info.length;
    } 

    addr_t next;
    int found_next = findNextArbitrationAddress(state, start, &next, MAX_ITER);
    BENCH_ADD(ANALYSE_BLOCK_SIZE_SUM, next - start);

    if (found_next) {
#ifdef USE_CACHE
#if !defined(NOCACHE_OPTIMIZATION)
       unsigned int index;
       addr_t a = cacheLookup(start, &index);
       assert(a == next);
       if (a)
       {
          switch (performCacheOptimization(state, start, a, index))
          {
             case CACHE_OPTIMIZER_CACHE_OPTIMIZED:
                a = cacheLookup(start, &index);
                /* Optimization may have found a better breakpoint */
                if (a) next = a;
                break;
             case CACHE_OPTIMIZER_LET_GUEST_RUN:
                platform.execute(state);
                break;
             default: /* cache optimizer did nothing new */
                break;
          }
       }
#endif // not defined(CACHE_OPTIMIZATION)
#endif // USE_CACHE
       
       platform.patchAddress(state, next, (addr_t) hypervisor_handle_return);
    } else {
       assert(NO_DANGEROUS_INSTRUCTION_FOUND);
    }
    DEBUG_LOG_HANDLE_RETURN;
    platform.execute(state);

}
