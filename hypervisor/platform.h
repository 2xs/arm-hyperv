/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include <stdint.h>
#include <stdbool.h>

#define HYPERVISOR_MAX_CPU_MODE_COUNT   32

typedef uint32_t addr_t;

typedef enum Hypervisor_InstrCondition {
   Cond_EQ, Cond_NE, Cond_CS, Cond_CC,
   Cond_MI, Cond_PL, Cond_VS, Cond_VC,
   Cond_HI, Cond_LS, Cond_GE, Cond_LT,
   Cond_GT, Cond_LE, Cond_AL, Cond_UNDEF,
} Hypervisor_InstrCondition;

#define NB_BEHAVIOR 16
typedef enum Hypervisor_InstrBehavior {
   Behavior_Nothing = 1,

   Behavior_ReadsMemory = 2,
   Behavior_WritesMemory =4,

   Behavior_Branches = 8,
   Behavior_Link = 16,
   Behavior_CanChangeState = 32,

   Behavior_RaisesException = 64,

   Behavior_ITBlockStart = 128,

   Behavior_ReadsCopro = 256,
   Behavior_WritesCopro = 512,

   Behavior_Unpredictable = 1024,
   Behavior_Unsuported = 2048,
    
   Behavior_ReadsPsr = 1<<12,
   Behavior_WritesPsr = 1<<13,

   Behavior_HardwareAccess = 1<<14,

   Behavior_OddBranch = 1<<15,
} Hypervisor_InstrBehavior;

typedef enum Hypervisor_InstrBehavior_Number {
   Behavior_Number_Nothing = 0,
   Behavior_Number_ReadsMemory = 1,
   Behavior_Number_WritesMemory = 2,
   Behavior_Number_Branches = 3,
   Behavior_Number_Link = 4,
   Behavior_Number_CanChangeState = 5,
   Behavior_Number_RaisesException = 6,
   Behavior_Number_ITBlockStart = 7,
   Behavior_Number_ReadsCopro = 8,
   Behavior_Number_WritesCopro = 9,
   Behavior_Number_Unpredictable = 10,
   Behavior_Number_Unsuported = 11,
   Behavior_Number_ReadsPsr = 12,
   Behavior_Number_WritesPsr = 13,
   Behavior_Number_HardwareAccess = 14,
   Behavior_Number_OddBranch = 15,
} Hypervisor_InstrBehavior_Number;

typedef struct Hypervisor_InstrInfo {
   // Basic information
   addr_t address;
   uint32_t instr;
   int length;

   Hypervisor_InstrBehavior behavior;

   // Extended information
   Hypervisor_InstrCondition condition;
   int condition_met;

   addr_t memory_access;
   int access_byte_width;
   int memory_dest_register;

   addr_t pc_destination;
   uint32_t unstack_reg_list;
   int exception;
   int idx; // table entry index that is used to fill that structure
} Hypervisor_InstrInfo;

typedef int (*Hypervisor_Behavior_Function) (void* state, Hypervisor_InstrInfo* info, int extended);

typedef struct {
   int cpu_mode_count;

   void (*io_putc)(char c);

   int (*modeIndex)(void * state);
   int (*destinationMode)(void *state); /**< returns the mode in which
                                         * the next analyzed code will
                                         * be effectively run */
   addr_t (*getPCValue)(void * state);
   uint32_t (*getReturnValue)(void * state);  
   void (*setReturnValue)(void * saved_state, uint32_t value);
   void (*setPCValue)(void * state, addr_t value, int link);
   void (*setCPSR)(void * state, uint32_t value);
   uint32_t (*getCPSR)(void * state);
   void (*copyState)(void * state, void ** new_state);
   void (*setLastITAddr)(void * state, addr_t value);
   int (*isInITBlock)(void * state, addr_t instr_addr);

   unsigned int (*instructionInfo)(void * saved_state, addr_t at, /* out */ Hypervisor_InstrInfo * info, unsigned int index, int extended);

   void NORETURN (*execute)(void * saved_state);

   int (*bytesNeededToPatchAddress)(void * saved_state, addr_t patch_address, addr_t destination);
   void (*patchAddress)(void * saved_state, addr_t patch_address, addr_t destination);
   void (*unpatchAddress)(void * saved_state);

   void * (*stateForMode)(int mode);
   void (*init)(void);
   int (*isConditionMet)(void* state);
   void (*debugDumpState)(void * saved_state);
   void (*execute_step)(void * saved_state, uint32_t addr, bool thumb);
   void (*stop)(void);
  
   Hypervisor_Behavior_Function* behavior_runtime_functions;

   void (*init_perfcounters)(uint8_t reset_cyclecount);
   unsigned int (*get_cyclecount)(void);
   void (*set_cyclecount)(unsigned int value);
   void (*init_overflow_timer)(void);
   void (*irq_enable)(void);
   void (*irq_disable)(void);
   addr_t (*get_next_instr)(const addr_t start);   
   uint32_t (*get_timer)(void);
} Hypervisor_Platform;

#endif
