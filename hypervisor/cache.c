/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "cache.h"
#include "printf.h"

/**********************/
/* Define cache types */
/**********************/
#ifdef USE_CACHE /* Only allocate if cache is used */

/* This is a cache entry :
   for a given start, it should return the next addr to arbitrate 
*/
typedef struct
{
  addr_t start;
  addr_t next_arbitration;
  unsigned int table_index;
#ifdef BENCH_INTERNALS
   unsigned int collisions;
#endif
} addr_cache_t;

#define HASH_SIZE_IN_BITS (10)
#define ADDR_CACHE_SIZE (1 << HASH_SIZE_IN_BITS)

/* Actual cache entries list */
static addr_cache_t addr_cache[ADDR_CACHE_SIZE];


static inline uint16_t hash(uint32_t addr)
{
   addr >>= 2;
   return (addr ^ (addr >> HASH_SIZE_IN_BITS) ^ (addr >> (HASH_SIZE_IN_BITS << 1))) & (ADDR_CACHE_SIZE -1);
}

/*#define ADDR_TO_IDX(a) (((a) >> 1) & (ADDR_CACHE_SIZE-1))*/
#define ADDR_TO_IDX(a) hash((a))

#include "bench_internals.h"
#ifdef BENCH_INTERNALS
   #define DEBUG_LOG_CACHE_UPDATE BENCH_INC(CACHE_UPDATE)
   #define DEBUG_LOG_CACHE_CLEAR  BENCH_INC(CACHE_CLEAR)
   #define DEBUG_LOG_CACHE_MISS BENCH_INC(CACHE_MISS)
   #define DEBUG_LOG_CACHE_HIT  BENCH_INC(CACHE_HIT)
   #define DEBUG_LOG_CACHE_FIND_ITER  BENCH_INC(CACHE_FIND_ITER)
   #define DEBUG_LOG_CACHE_FIND       BENCH_INC(CACHE_FIND_COUNT)
   #define DEBUG_LOG_CACHE_COLLISION(a,b) do { if ((b).start && ((a) != (b).start)) {BENCH_INC(CACHE_COLLISION);(b).collisions++;} } while(0)
#else
   #define DEBUG_LOG_CACHE_UPDATE do{}while(0)
   #define DEBUG_LOG_CACHE_CLEAR do{}while(0)
   #define DEBUG_LOG_CACHE_MISS do{}while(0)
   #define DEBUG_LOG_CACHE_HIT do{}while(0)
   #define DEBUG_LOG_CACHE_FIND_ITER  do {} while(0)
   #define DEBUG_LOG_CACHE_FIND       do {} while(0)
   #define DEBUG_LOG_CACHE_COLLISION(a,b) do {} while(0)
#endif


#endif /* USE_CACHE */

/*******************/
/* Cache functions */
/*******************/


/* Find a suitable entry for addr in the cache.
   If insert is true, then returns a suitable entry for updating
   If insert is false, it returns -1 if addr is not in cache
*/
#define SMART_CACHE_FIND
static int cacheFindEntry(addr_t addr, int insert)
{
#ifdef USE_CACHE
#ifdef SMART_CACHE_FIND
   const int cache_depth_search = 5;
   addr_t hash_source = addr;
   int lookup;
   DEBUG_LOG_CACHE_FIND;
   for (int i = 0 ; i < cache_depth_search ; ++i, hash_source += 0x37824AFC)
   {
      DEBUG_LOG_CACHE_FIND_ITER;
      lookup = ADDR_TO_IDX(hash_source);
      if (!addr_cache[lookup].start && insert)
         return lookup;
      if (addr_cache[lookup].start == addr)
         return lookup;
      /* the addr is not directly in the cache, try to find a new place */
   }
   if (!insert) /* cache miss */
      return -1;
   /* If one address must be discarded, let it be the one that will be found faster 
      
      This decision is arguable and may be changed later

    */
   return ADDR_TO_IDX(addr); 
#else // SMART_CACHE_FIND
   int lookup = ADDR_TO_IDX(addr);
   if (addr_cache[lookup].start == addr)
      return lookup; /* cache hit */
   if (insert)
      return lookup; /* cache collision */
   return -1; /* cache miss */
#endif // SMART_CACHE_FIND
#endif // USE_CACHE
}

/* Lookup function */
addr_t cacheLookup(addr_t start, unsigned int* table_index)
{
    
#ifdef USE_CACHE
   int lookup = cacheFindEntry(start, 0);

   BENCH_INC(CACHE_LOOKUP);
   if (lookup != -1)
   {
      DEBUG_LOG_CACHE_HIT;
      
      *table_index = addr_cache[lookup].table_index;
      return addr_cache[lookup].next_arbitration;   
   }
   DEBUG_LOG_CACHE_MISS;
#endif /* USE_CACHE */
   *table_index = 0;
   return 0;
}

void cacheUpdate(addr_t start, addr_t next_arbitration, unsigned int table_index)
{
#ifdef USE_CACHE
   addr_t lookup = cacheFindEntry(start, 1);
   DEBUG_LOG_CACHE_UPDATE;
   DEBUG_LOG_CACHE_COLLISION(start, addr_cache[lookup]);
   addr_cache[lookup].start = start;
   addr_cache[lookup].next_arbitration = next_arbitration;
   addr_cache[lookup].table_index = table_index;
#endif /* USE_CACHE */
}

void cacheClear(addr_t start)
{
#ifdef USE_CACHE
   addr_t lookup = ADDR_TO_IDX(start);
   DEBUG_LOG_CACHE_CLEAR;
   addr_cache[lookup].start = 0;
   addr_cache[lookup].next_arbitration = 0;
   addr_cache[lookup].table_index = 0;
#endif /* USE_CACHE */
}


void cachePrintStats(void)
{
#ifdef USE_CACHE
#ifdef BENCH_INTERNALS
   int i;
   int used = 0;
   printf("Cache content:\n");
   printf("+--------------+---------------------+------------+------------+\n");
   printf("|start address |destination address  |table index |collisions  |\n");
   printf("+--------------+---------------------+------------+------------+\n");
   for (i = 0 ; i < ADDR_CACHE_SIZE ; ++i)
   {
      if (addr_cache[i].start)
      {
         printf("|%13x |%19x |%12d |%11d |", addr_cache[i].start, addr_cache[i].next_arbitration, addr_cache[i].table_index, addr_cache[i].collisions);
         if (addr_cache[i].start >= addr_cache[i].next_arbitration)
            printf("*");
         printf("\n");
         used++;
      }
   }
   printf("Cache usage: %d/%d (%d updates, %d clears, %d/%d/%d hit/miss/total, collisions: %d)\n", used, ADDR_CACHE_SIZE, BENCH_VAL(CACHE_UPDATE), BENCH_VAL(CACHE_CLEAR), BENCH_VAL(CACHE_HIT), BENCH_VAL(CACHE_MISS), BENCH_VAL(CACHE_HIT)+BENCH_VAL(CACHE_MISS), BENCH_VAL(CACHE_COLLISION));
#endif /* BENCH_INTERNALS */
#endif /* USE_CACHE */
}


