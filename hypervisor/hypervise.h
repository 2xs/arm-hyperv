/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __HYPERVISE_H__
#define __HYPERVISE_H__

/** This function is used to patch an UDF call so that the guest
   gracefuly exits The effect is that the caller of the hypervise
   function will continue its execution after the hypervise call.
*/
void NAKED hypervisor_exit_from_guest(void * state);

/** Runs a guest system under supervision, optionaly giving it parameters
    using EABI calling convetion. This saves the hypervisor context.
*/
void NAKED hypervise(addr_t code_start, int nparams, ...);

/** Restores the hypervisor_context saved by the hypervise function. */
void NORETURN restore_hypervisor_context(void);

/** This function can be called from a guest to ask for hypervision stop.
    This is provided for bench purpose
    This is located at address 0x17000
*/
void NAKED hypervise_stop(void);

/** This function can be called from a guest to ask for hypervision restart.
    This is provided for bench purpose
    This is located at address 0x17100
*/
void NAKED hypervise_restart(void);

#endif /* __HYPERVISE_H__ */
