/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "hypervisor.h"
#include "debug.h"
#include "hypervisor_debug.h"

extern Hypervisor_Platform platform;

void * saved_state;
void NORETURN hypervisor_return_from_destination_computation(void * state) {
   platform.unpatchAddress(state);

   platform.behavior_runtime_functions[Behavior_Number_OddBranch](state, (Hypervisor_InstrInfo*)0x0 /*info*/, 1 /* extended ?*/);
   platform.setCPSR(state, platform.getCPSR(saved_state));
   platform.setPCValue( state, (addr_t) platform.getReturnValue(state), 0 );
   platform.setReturnValue( state, (addr_t) platform.getReturnValue(saved_state));
   hypervisor_execute(state, 0);
}
void NORETURN hypervisor_is_cond_met(void * state) {
   Hypervisor_InstrInfo info;

   
   platform.unpatchAddress(state);

   platform.instructionInfo(state, platform.getPCValue(state), &info, 0, 1);

   if (info.condition_met) {
      platform.copyState(state,&saved_state);
      
      platform.patchAddress(state,(uint32_t)(info.address + info.length),(addr_t)hypervisor_return_from_destination_computation);
      platform.setPCValue( state, (addr_t)(info.address), 0 );
      platform.execute(state);
   } else {
      platform.behavior_runtime_functions[Behavior_Number_OddBranch](state, (Hypervisor_InstrInfo*)0x0 /*info*/, 0 /* extended ?*/);
      hypervisor_execute(state, 1);
   }
}

void NORETURN hypervisor_handle_return(void * state) {
#ifdef DEBUG
    platform.debugDumpState(state);
#endif

    platform.unpatchAddress(state);

    // What was the cause of the stop?
    Hypervisor_InstrInfo info;
    addr_t pc = platform.getPCValue(state);

    
    if (!platform.instructionInfo(state, pc, &info, 0, 1))
    {
       /* We do not assert(platform.instructionInfo(...)) here
        * because one may think that all the assertion code could be
        * removed. Actually, the call to instruction info is
        * important, that's why it is not directly in the assertion
        */
       assert(0);
    }
    
    assert(!(info.behavior & Behavior_Unsuported));

    int skip_first = 1;
    
      if (info.condition_met)
         skip_first = do_runtime_jobs(state,&info, 0 /*extended ?*/);
      else
         skip_first = 1;
      hypervisor_execute(state, skip_first);
}

#include "hypervise.h" /* for hypervisor_stop */
int do_runtime_jobs(void* state, Hypervisor_InstrInfo* info, int extended){
  int skip_first = 1;
  int i;


/* For each behavior of the instruction, if it is possible, we do runtime job*/
  for(i=0;i<NB_BEHAVIOR;i++) {
     if( (info->behavior & (1<<i)) && platform.behavior_runtime_functions[i] ){
        skip_first &= platform.behavior_runtime_functions[i](state, info, extended);
     }
  }
  /* First idea for handling stop: Compare pc_destination with the
     address of hypervise_stop.  If match to the corresponding trick
     which is execute the guest directly without patching
  */
  if (info->pc_destination == (addr_t) hypervise_stop)
  {
     platform.execute(state);
  }
  
  return skip_first;
}
