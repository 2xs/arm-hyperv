/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "hypervisor_debug.h"
#include "output.h"

void hypervisor_dump_instr_info(Hypervisor_InstrInfo * info __attribute__((unused)), int extended __attribute__((unused))) {
#ifdef LOG_LEVEL
    static const char *cond_txt[] = {
        "EQ", "NE", "CS", "CC",
        "MI", "PL", "VS", "VC",
        "HI", "LS", "GE", "LT",
        "GT", "LE", "AL", "UNDEF"
    };

    log(LOG_INSTRUCTION_INFO,"Instruction at %p\n", (void *) info->address);
    log(LOG_INSTRUCTION_INFO,"    > word: ");
    if (info->length == 1) log(LOG_INSTRUCTION_INFO,"%02X", info->instr);
    else if (info->length == 2) log(LOG_INSTRUCTION_INFO,"%04X", info->instr);
    else if (info->length == 3) log(LOG_INSTRUCTION_INFO,"%06X", info->instr);
    else if (info->length == 4) log(LOG_INSTRUCTION_INFO,"%08X", info->instr);
    else log(LOG_INSTRUCTION_INFO,"%X", info->instr);
    log(LOG_INSTRUCTION_INFO,"\n");
    if (extended) log(LOG_INSTRUCTION_INFO,"    > condition %s is%s met\n", cond_txt[info->condition], info->condition_met ? "" : " not");
    log(LOG_INSTRUCTION_INFO,"    > behavior:");
    if (info->behavior & Behavior_ReadsMemory) {
        log(LOG_INSTRUCTION_INFO," ReadsMemory");
        if (extended) log(LOG_INSTRUCTION_INFO," (at %p, length = %d)", (void *) info->memory_access, info->access_byte_width);
    }
    if (info->behavior & Behavior_WritesMemory) {
        log(LOG_INSTRUCTION_INFO," WritesMemory");
        if (extended) log(LOG_INSTRUCTION_INFO," (at %p, length = %d)", (void *) info->memory_access, info->access_byte_width);
    }
    if (info->behavior & Behavior_Branches) {
        log(LOG_INSTRUCTION_INFO," Branches");
        if (extended) log(LOG_INSTRUCTION_INFO," (dest = %p)", (void *) info->pc_destination);
    }
    if (info->behavior & Behavior_Link) {
        log(LOG_INSTRUCTION_INFO," Link");
    }
    if (info->behavior & Behavior_CanChangeState) {
        log(LOG_INSTRUCTION_INFO," CanChangeState");
    }
    if (info->behavior & Behavior_RaisesException) {
        log(LOG_INSTRUCTION_INFO," RaisesException");
        if (extended) log(LOG_INSTRUCTION_INFO," (vector %d)", info->exception);
    }
    if (info->behavior & Behavior_ITBlockStart) {
        log(LOG_INSTRUCTION_INFO," ITBlock");
    }
    if (info->behavior & Behavior_ReadsCopro) {
       log(LOG_INSTRUCTION_INFO," Reads coprocessor");
    }
    if (info->behavior & Behavior_WritesCopro) {
       log(LOG_INSTRUCTION_INFO," Writes coprocessor");
    }
    log(LOG_INSTRUCTION_INFO,"\n");

#endif    
}
