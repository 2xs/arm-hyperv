# Better to compile from platforms/raspi
BASE=$(shell echo `pwd`  | sed -e s/hypervisor//)

.PHONY: all
all: emu

.PHONY: archive
archive: arm-hyperv.tar.gz

arm-hyperv.tar.gz:
	git archive --prefix arm-hyperv/ -o $@ HEAD .

.PHONY: doc
doc: README.html

%.html: %.md
	pandoc -o $@ $<

%:
	make -C $(BASE)/platforms/raspi/ $@

