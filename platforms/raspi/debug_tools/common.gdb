# -*- mode: gdb-script; -*-

# Set some variable for easy state debugging
set $supervisor_state=(ARM_SavedState*)(&cpu_states+19*64)
set $system_state=(ARM_SavedState*)(&cpu_states+31*64)
set $user_state=(ARM_SavedState*)(&cpu_states+16*64)
set $irq_state=(ARM_SavedState*)(&cpu_states+18*64)


target remote localhost:1234

# Always breakpoint when abort
b abort_from
