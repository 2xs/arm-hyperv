#!/usr/bin/env bash
if [ -z "$STY" ]; then exec screen -dmS debug -t qemu -t gdb /usr/bin/env bash "$0" "$@"; fi
script=gdb.script
build=release
if [ ! -z "$1" ]
then
	script="gdb.$1.script"
	build="$1"
fi

screen -S debug -X screen -t qemu sh -c "while true; do make BUILD=$build debug-emu; echo '\^C now to exit'; sleep 1 ; done"
screen -S debug -X screen -t gdb
screen -S debug -X stuff "pkill qemu-system-arm ; arm-none-eabi-gdb -x $script\n"

