# -*- mode: gdb-script; -*-

set verbose off
set confirm off
set pagination 0


define hr
  printf "------ BP "
  # printf %s does not work
  echo $arg0
  printf " state: 0x%X, CPU Mode %d ----\n", $r0, $cpsr & 0x1F
end


define context_info
  printf "Task 0x%x(sp:0x%x): R11: 0x%x\n", $r9, $sp, $r11
end

b arm_trampolines.S:88 if $r1 == 0x1F

b *0x18004
commands
  silent
  x/i $pc
  display_cpu_state
  continue
end
b *0x1800c
commands
  silent
  x/i $pc
  display_cpu_state
  continue
end
b *0x18014
commands
  silent
  x/i $pc
  display_cpu_state
  continue
end
b *0x18018
commands
  silent
  x/i $pc
  display_cpu_state
  continue
end

# b *0x40000
# commands
#   silent
#   printf "Before executing guest code\n"
#   display_state_from_addr $r0
#   continue
# end
# b *0x40030
# commands
#   silent
#   if $sp != 0x752c8
#     continue
#   end
# end

# b *0x40120
# commands
#   silent
#   printf "Finish run, return to hypervisor\n"
#   display_state_from_addr $r0
#   continue
# end

# b *0x18a44
# commands
#   silent
#   printf "In schedule, before movs\n"
#   i r
#   si
#   commands
#     printf "In task, after movs\n"
#     i r
#     continue
#   end
# end

# b *0x18554
# commands
#   silent
#   printf "Entering user task\n"
#   context_info
#   continue
# end

# b *0x18598
# commands
#   silent
#   printf "Before nop loop\n"
#   context_info
#   continue
# end

# b *0x18a64
# commands
#   silent
#   printf "Just set user stack in schedule\n"
#   x/i$pc
#   context_info
#   # display_state 31
#   # display_state 18
#   continue
# end  

# b *0x18a60
# commands
#   silent
#   printf "SP is set in mode %d value: 0x%x\n", $cpsr & 0x1f, $r2
# end


# b swi_handler
# commands
#   i r
#   continue
# end

# b irq_handler
# commands
#   i r
#   continue
# end
# b arm_exception.c:97
# commands
#   i r
# end


define display_cpu_state
  printf "CPU State at 0x%x\n", $pc
  printf "R0  : 0x%x\t R1  : 0x%x\n", $r0,  $r1
  printf "R2  : 0x%x\t R3  : 0x%x\n", $r2,  $r3
  printf "R4  : 0x%x\t R5  : 0x%x\n", $r4,  $r5
  printf "R6  : 0x%x\t R7  : 0x%x\n", $r6,  $r7
  printf "R8  : 0x%x\t R9  : 0x%x\n", $r8,  $r9
  printf "R10 : 0x%x\t R11 : 0x%x\n", $r10, $r11
  printf "R12 : 0x%x\t SP  : 0x%x\n", $r12, $sp
  printf "LR  : 0x%x\t PC  : 0x%x\n", $lr,  $pc
  printf "CPSR: 0x%x\n", $cpsr
end


define display_state_from_addr
  set $state_addr = (ARM_SavedState*)$arg0
  printf "State at 0x%x\n", $arg0
  printf "R0  : 0x%x\t R1  : 0x%x\n", $state_addr->context.gpr.name.r0, $state_addr->context.gpr.name.r1
  printf "R2  : 0x%x\t R3  : 0x%x\n", $state_addr->context.gpr.name.r2, $state_addr->context.gpr.name.r3
  printf "R4  : 0x%x\t R5  : 0x%x\n", $state_addr->context.gpr.name.r4, $state_addr->context.gpr.name.r5
  printf "R6  : 0x%x\t R7  : 0x%x\n", $state_addr->context.gpr.name.r6, $state_addr->context.gpr.name.r7
  printf "R8  : 0x%x\t R9  : 0x%x\n", $state_addr->context.gpr.name.r8, $state_addr->context.gpr.name.r9
  printf "R10 : 0x%x\t R11 : 0x%x\n", $state_addr->context.gpr.name.r10, $state_addr->context.gpr.name.r11
  printf "R12 : 0x%x\t SP  : 0x%x\n", $state_addr->context.gpr.name.r12, $state_addr->context.gpr.name.sp
  printf "LR  : 0x%x\t PC  : 0x%x\n", $state_addr->context.gpr.name.lr, $state_addr->context.gpr.name.pc
  printf "CPSR: 0x%x\t SPSR: 0x%x\n", $state_addr->context.cpsr.value, $state_addr->context.spsr.value
end
# Arg0 is mode number
define display_state
  set $state_addr = (ARM_SavedState *)(&cpu_states+($arg0*64))
  printf "State for mode %d\n", $arg0
  display_state_from_addr $state_addr
end


define pc_from_state
  set $retval = ((ARM_SavedState*) $arg0)->context.gpr.name.pc
end

define patch_addr_from_state
  set $retval = ((ARM_SavedState*) $arg0)->patch_addr
end
