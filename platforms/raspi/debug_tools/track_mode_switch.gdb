set verbose off
set confirm off
set pagination 0

define hr
  printf "------ BP "
  # printf %s does not work
  echo $arg0
  printf " state: 0x%X, CPU Mode %d ----\n", $r0, $cpsr & 0x1F
end


b arm_behavior_functions.c:500
commands
  silent
  pc_from_state state
  printf "At:\n"
  x /i $retval
  x /x *$retval
  printf "Switching mode from %d to %d\n", old_mode, new_mode
  continue
end

define pc_from_state
  set $retval = ((ARM_SavedState*) $arg0)->context.gpr.name.pc
end

define patch_addr_from_state
  set $retval = ((ARM_SavedState*) $arg0)->patch_addr
end
