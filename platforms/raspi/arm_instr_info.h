/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __ARM_INSTR_INFO_H__
#define __ARM_INSTR_INFO_H__

#include "arm_cpu.h"

typedef union {
    uint32_t instr;
    struct {
        union {
            uint16_t value;
            struct {
                unsigned imm10:10;
                unsigned s:1;
                unsigned b_11110:5;
            } s;
        } first;
        union {
            uint16_t value;
            struct {
                unsigned imm11:11;
                unsigned j2:1;
                unsigned b_1:1;
                unsigned j1:1;
                unsigned b_11:2;
            } s;
        } second;
    } s;
} Thumb_BL_BLX_T1Encoding;

typedef union {
    uint32_t instr;
    struct {
        union {
            uint16_t value;
            struct {
                unsigned imm10H:10;
                unsigned s:1;
                unsigned b_11:5;
            } s;
        } first;
        union {
            uint16_t value;
            struct {
                unsigned h:1;
                unsigned imm10L:10;
                unsigned j2:1;
                unsigned b_12:1;
                unsigned j1:1;
                unsigned b_14:2;
            } s;
        } second;
    } s;
} Thumb_BL_BLX_T2Encoding;

typedef union {
    uint32_t instr;
    struct {
        union {
            uint16_t value;
            struct {
                unsigned imm6:6;
                unsigned cond:4;
                unsigned s:1;
                unsigned b_11:5;
            } s;
        } first;
        union {
            uint16_t value;
            struct {
                unsigned imm11:11;
                unsigned j2:1;
                unsigned b_12:1;
                unsigned j1:1;
                unsigned b_14:2;
            } s;
        } second;
    } s;
} Thumb_B_T3Encoding;

typedef union {
    uint32_t instr;
    struct {
        union {
            uint16_t value;
            struct {
                unsigned imm10:10;
                unsigned s:1;
                unsigned b11:5;
            } s;
        } first;
        union {
            uint16_t value;
            struct {
                unsigned imm11:11;
                unsigned j2:1;
                unsigned b_12:1;
                unsigned j1:1;
                unsigned b_14:2;
            } s;
        } second;
    } s;
} Thumb_B_T4Encoding;


typedef union {
  uint32_t instr;
  struct {
    union {
      uint16_t value;
      struct {
	unsigned crn:4;
	unsigned zero:1;
	unsigned opc1:3;
	unsigned prefix_encoding:8;
      } s;
    } first;
    union {
      uint16_t value;
      struct {
	unsigned crm:4;
	unsigned one:1;
	unsigned opc2:3;
	unsigned coproc:4;
	unsigned rt:4;
      } s;
    } second;
  } s;
} Thumb_MCR_T1Encoding;

typedef union {
  uint32_t instr;
  struct {
    union {
      uint16_t value;
      struct {
	unsigned crn:4;
	unsigned one:1;
	unsigned opc1:3;
	unsigned prefix_encoding:8;
      } s;
    } first;
    union {
      uint16_t value;
      struct {
	unsigned crm:4;
	unsigned one:1;
	unsigned opc2:3;
	unsigned coproc:4;
	unsigned rt:4;
      } s;
    } second;
  } s;
} Thumb_MRC_T1Encoding;


typedef enum { Fault, PageTable, Section, SuperSection, Undef = 4 } ARM_L1_TableType;
int arm_platform_instruction_length(ARM_SavedState * state, uint32_t word);
unsigned int arm_platform_instruction_info(void * saved_state, addr_t at, /* out */ Hypervisor_InstrInfo * info, unsigned int index, int extended);
int arm_platform_condition_is_met(ARM_SavedState * state, Hypervisor_InstrCondition condition);
Hypervisor_InstrCondition arm_platform_condition(/* out */ Hypervisor_InstrInfo * info);

#endif /* end of include guard: __ARM_INSTR_INFO_H__ */
