/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include <stdarg.h>
#include "hypervisor.h"
#include "debug.h"
#include "hypervise.h"
#include "types.h"
#include "arm_cpu.h"
#include "output.h"


void NAKED hypervisor_exit_from_guest(void * state) {
   Hypervisor_Platform * platform = hypervisor_current_platform();
   platform->unpatchAddress(state); /* restore guest code */
   
   log(LOG_HYPERVISOR, "Guest want to execute UDF, end it");
   restore_hypervisor_context();
}


#define STACK_SIZE (1024/sizeof(int))
void hypervise_intern(uint32_t function, int nb_param, ...){
  va_list ap;
  int stack[STACK_SIZE]; /* type int for alignement purpose*/
  int i;

  ARM_SavedState *state;
  Hypervisor_Platform * platform = hypervisor_current_platform();

  state = platform->stateForMode(ARMMode_Supervisor);
  bzero(state, sizeof(ARM_SavedState));
  
  va_start(ap, nb_param);

  /* EABI calling convention. First four parameters are in r0, r1, r2,
   * r3, r4 */
  for(i=0; i<4 && i<nb_param ; i++)
  {
    state->context.gpr.reg[i] = va_arg(ap, int);
  }

  /* Next parameters are in the stack */
  for(;  i<nb_param ; i++)
    stack[(i-4)] = va_arg(ap, int);

  va_end(ap);

  state->context.gpr.name.sp = (uint32_t) stack;
  state->context.gpr.name.pc = (uint32_t) function;
  state->context.cpsr.fields.mode = ARMMode_Supervisor;
  state->context.cpsr.fields.thumb = 0;
  state->context.cpsr.fields.fiq_mask = 1;
  state->context.cpsr.fields.irq_mask = 1;
  state->context.cpsr.fields.async_mask = 1;

  hypervisor_start(state);
}

/** This function is called from hypervise_restart in hypervise.S.
    The call of this function is to save the current state and
    restart the hypervisor
*/
void do_hypervise_restart(addr_t destination_pc)
{
   Hypervisor_Platform *platform = hypervisor_current_platform();
   void *state = platform->stateForMode(ARMMode_Supervisor);
   platform->setPCValue(state, destination_pc, 0);
}
