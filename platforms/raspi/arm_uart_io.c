/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

/*
  (large) Part of this file have been borrowed from http://wiki.osdev.org/Raspberry_Pi_Bare_Bones
 */

#include <stdint.h>

//#define UART0_BASE_ADDR 0x20201000

enum
{
    // The GPIO registers base address.

    GPIO_BASE = (PERIPHERAL_BASE + 0x00200000),
    
    // The offsets for reach register.
    GPFSEL1 = (GPIO_BASE + 0x4),
    GPSET0  = (GPIO_BASE + 0x1C),
    GPCLR0  = (GPIO_BASE + 0x28),
    // Controls actuation of pull up/down to ALL GPIO pins.
    GPPUD = (GPIO_BASE + 0x94),
 
    // Controls actuation of pull up/down for specific GPIO pin.
    GPPUDCLK0 = (GPIO_BASE + 0x98),
 
    // The base address for UART.
    UART0_BASE = (GPIO_BASE + 0x1000),
 
    // The offsets for reach register for the UART.
    UART0_DR     = (UART0_BASE + 0x00),
    UART0_RSRECR = (UART0_BASE + 0x04),
    UART0_FR     = (UART0_BASE + 0x18),
    UART0_ILPR   = (UART0_BASE + 0x20),
    UART0_IBRD   = (UART0_BASE + 0x24),
    UART0_FBRD   = (UART0_BASE + 0x28),
    UART0_LCRH   = (UART0_BASE + 0x2C),
    UART0_CR     = (UART0_BASE + 0x30),
    UART0_IFLS   = (UART0_BASE + 0x34),
    UART0_IMSC   = (UART0_BASE + 0x38),
    UART0_RIS    = (UART0_BASE + 0x3C),
    UART0_MIS    = (UART0_BASE + 0x40),
    UART0_ICR    = (UART0_BASE + 0x44),
    UART0_DMACR  = (UART0_BASE + 0x48),
    UART0_ITCR   = (UART0_BASE + 0x80),
    UART0_ITIP   = (UART0_BASE + 0x84),
    UART0_ITOP   = (UART0_BASE + 0x88),
    UART0_TDR    = (UART0_BASE + 0x8C),
};

/*void arm_uart_putc(char c) {
   *(char *) UART0_BASE_ADDR = c;
}*/
 
static void mmio_write(uint32_t reg, uint32_t data)
{
  *(volatile uint32_t *)reg = data;
}
 
static uint32_t mmio_read(uint32_t reg)
{
  return *(volatile uint32_t *)reg;
}

volatile unsigned int* gpio = (unsigned int*)GPIO_BASE;

void init_ok_led(){	
  /* Write 1 to the GPIO16 init nibble in the Function Select 1 GPIO
     peripheral register to enable GPIO16 as an output */
  //	*(unsigned int*)GPFSEL |= (1 << 18);
  gpio[1] |= (1 << 18);
}

void set_ok_led_off(){
  /* Set the LED GPIO pin high ( Turn OK LED off for original Pi )*/
  //*(unsigned int*)GPSET = (1 << 16);
  gpio[7] = (1 << 16);
}

void set_ok_led_on(){
  /* Set the LED GPIO pin low ( Turn OK LED on for original Pi )*/
  //      *(unsigned int*)GPCLR = (1 << 16);
  gpio[10] = (1 << 16);
}

void delay(void)
{
   unsigned int tmp;
   for(tmp = 0 ; tmp < 150 ; tmp++)
      asm volatile("nop");
}

void arm_uart_init(void)
{
   unsigned int tmp;

   mmio_write(UART0_CR,0);

   tmp=mmio_read(GPFSEL1);
   tmp&=~(7<<12); //gpio14
   tmp|=4<<12;    //alt0
   tmp&=~(7<<15); //gpio15
   tmp|=4<<15;    //alt0
   mmio_write(GPFSEL1,tmp);

   mmio_write(GPPUD,0);
   delay();
   mmio_write(GPPUDCLK0,(1<<14)|(1<<15));
   delay();
   mmio_write(GPPUDCLK0,0);

   mmio_write(UART0_ICR,0x7FF);
   mmio_write(UART0_IBRD,1);
   mmio_write(UART0_FBRD,40);
   mmio_write(UART0_LCRH,0x70);
   mmio_write(UART0_CR,0x301);
}


void arm_uart_putc(char byte)
{
   static int init = 1;
   if (init)
   {
      arm_uart_init();
      init = 0;
   }
  while ( mmio_read(UART0_FR) & (1 << 5) ) { }
  mmio_write(UART0_DR, byte);
  while ( mmio_read(UART0_FR) & (1 << 5) ) { }
}



void arm_uart_puts(const char *s)
{
   while (*s)
      arm_uart_putc(*s++);
}

void arm_uart_puti(const unsigned int i){

  if(i/10 > 0)
     arm_uart_puti(i/10);
  arm_uart_putc('0' + (i%10));         
}
