#!/usr/bin/perl -w

use strict;


sub build
{
    my ($build_mode, $guest, $guest_build_mode) = @_;
    # Clean hypervisor
    system("make BUILD=$build_mode clean >& /dev/null");
    # Build guest
    system("make -BC sample_guests/$guest >& /dev/null") == 0 or return 0;
    # Build Hypervisor
    system("make BUILD=$build_mode >& /dev/null") == 0 or return 0;
    return 1;
}

sub kill_qemu
{
    my ($pid) = @_;
    system("pkill qemu-system-arm");
}

sub run_bench
{
    my ($elf_file) = @_;
    my $qemu_cmd_line = "qemu-system-arm -cpu arm1176 -m 512 -M raspi -nographic -kernel";
    # system("$qemu_cmd_line $elf_file");
    my $qemu_pid = open(QEMU_OUTPUT, "$qemu_cmd_line $elf_file 2>/dev/null|") or return 0;
    local $SIG{ALRM} = sub { kill_qemu($qemu_pid); };
    alarm 30;
    while (<QEMU_OUTPUT>)
    {
        if (/BENCH (.*)/)
        {
            print "$elf_file,$1\n";
        }
        if (/END/)
        {
            last;
        }
    }
    kill_qemu($qemu_pid);
    close(QEMU_OUTPUT);
}

sub build_and_run_bench
{
    my ($guest) = @_;
    build("release", $guest, "release");
    run_bench("bin_release/hypImage.elf");
    run_bench("data/guest.elf");
    
}

print("elf_file,bench,time,run_count\n");
build_and_run_bench("benchmarks");
build_and_run_bench("simple_scheduler");
