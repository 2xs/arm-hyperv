/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __ARM_MMU_H__
#define __ARM_MMU_H__

#include <stdint.h>
#include <platform.h>

#define MMU_FLAG_CACHED 0x1
#define MMU_FLAG_BUFFERED 0x2
#define MMU_FLAG_READWRITE 0x4
#define MMU_FLAG_EXEC 0x8

#define TEX_SHIFT (12)
#define AP_SHIFT  (10)
#define DOM_SHIFT  (5)
#define XN_SHIFT   (4)
#define CB_SHIFT   (2)
/* B3.1326 */
/*

  Section Description: 

    3  2  2  2  2  2  2  2  2  2  2  2  1  1  1  1  1     1    1  0        0  0  0  0  0 0
    1                                0  9  8  7  6  5     2    0  9        5  4  3  2  1 0
    +--------------------------------+--+--+--+--+--+-----+----+--+--------+--+--+--+--+-+
    |                                | N|  | N|  | A|T    |   A| ?|        | X| C| B| 1|P|
    | section base addr PA[31:20]    | S| 0| G| S| P|E    |   P| ?| Domain | N|  |  |  |X|
    |                                |  |  |  |  | 2|X    |[1:0| ?|        |  |  |  |  |N|
    +--------------------------------+--+--+--+--+--+-----+----+--+--------+--+--+--+--+-+

    AP: B3-1356
    2 modes, depending on SCTLR.AFE.
    SCTLR.AFE == 1 : AP[0] => Access flag && AP[2:1] Access permission
    
    AP[2] | AP[1:0] | Privileged level | Unprivileged level | Desc
    ------+---------+------------------+--------------------+----------------
       0  |   00    |    No-access     |      No-access     | All access generate faults
          |   01    |    Read/Write    |      No-access     | Privileged only access
          |   10    |    Read/Write    |      Read-only     | Writes unpriv generates fault
          |   11    |    Read/Write    |      Read/Write    | Full access
    ------+---------+------------------+--------------------+----------------
       1  |   00    |        -         |          -         | Reserved
          |   01    |     Read-only    |      No-access     | Read-only (Privileged)
          |   10    |     Read-only    |      Read-only     | Read-only (Deprecated)
          |   11    |     Read-only    |      Read-only     | Read-only (any Privilege) 

    SCTLR.AFE == 0 : AP[2:0] => Access permission (AP[0] should be 1)

    AP[2] (disables write access) | AP[1] (enables unprivileged access) | Access
    ------------------------------+-------------------------------------+------------------
                    0             |                  0                  |  R/W PL1 only
                    0             |                  1                  |  R/W at any level
                    1             |                  0                  |  RO PL1 only
                    1             |                  1                  |  RO at any level


*/
void arm_mmu_map_section(uint32_t [], addr_t paddr, addr_t vaddr, unsigned int flags);
void arm_flush_tlb(void);


#define TRANSLATION_TABLE_SIZE (4096)
#define MB (1024*1024)

#endif /* __ARM_MMU_H__ */
