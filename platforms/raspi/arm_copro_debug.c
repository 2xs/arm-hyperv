/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "arm_copro_debug.h"
#include "arm_instr_info.h"
#include "printf.h"

void dump_l1_table(uint32_t ttbr)
{
   int i;
   uint32_t *table_addr = (uint32_t*)ttbr;
   for (i=0;i<512; ++i)
   {
      printf("%4d. %8x  %4d. %8x  %4d. %8x  %4d. %8x  %4d. %8x  %4d. %8x  %4d. %8x  %4d. %8x\n", 
             i, table_addr[i],
             512+i, table_addr[512+i],
             1024+i, table_addr[1024+i],
             1536+i, table_addr[1536+i],
             2048+i, table_addr[2048+i],
             2560+i, table_addr[2560+i],
             3072+i, table_addr[3072+i],
             3584+i, table_addr[3584+i]
         );
   }
}

void print_copro_instr(uint32_t instr)
{
   
#define rt(name) name.s.second.s.rt
#define crn(name) name.s.first.s.crn
#define crm(name) name.s.second.s.crm
#define opc1(name) name.s.first.s.opc1
#define opc2(name) name.s.second.s.opc2
#define coproc(name) name.s.second.s.coproc

   Thumb_MRC_T1Encoding inst;
   inst.instr = instr;
   if (inst.s.first.s.one == 1)
   {
      printf("MRC ~");
   }
   else
   {
      printf("MCR ~");
   }
   if (coproc(inst) == 14)
   {
      if (opc1(inst) == 0) printf(" => Printf registers.\n");
      if (opc1(inst) == 1) printf(" => Trace registers.\n");
      if (opc1(inst) == 6) printf(" => ThumbEE registers.\n");
      if (opc1(inst) == 7) printf(" => Jazelle registers.\n");
   }
   else if (coproc(inst) == 15)
   {
      /* B3-17 p1471 */
      switch (crn(inst))
      {
	 case 0:
	    switch (opc1(inst))
	    {
	       case 0:

		  if ((crm(inst) < 0 || crm(inst) > 7) || (opc2(inst) < 0 || opc2(inst) > 7))
		  {
		     printf("No way?! crm = %d, opc2 = %d\n", crm(inst), opc2(inst));
		     for (;;);
		  }
		  else
		  {
		     char* crn0_opc10_to_string[8][8]= {
			/* CRm = 0 */ {"MIDR", "CTR", "TCMTR", "TLBTR", "MIDR", "MPIDR", "REVIDR", "MIDR"},
			/* CRm = 1 */ {"ID_PFR0", "ID_PRFR1", "ID_DRF0", "ID_AFR0", "ID_MMFR0", "ID_MMFR1", "ID_MMFR2", "ID_MMFR3"},
			/* CRm = 2 */ {"ID_ISAR0", "ID_ISAR1", "ID_ISAR2", "ID_ISAR3", "ID_ISAR4", "ID_ISAR5", "ID_ISAR6", "Read-As-Zero" },
			/* Crm = 3 */ {"Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero" },
			/* Crm = 4 */ {"Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero" },
			/* Crm = 5 */ {"Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero" },
			/* Crm = 6 */ {"Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero" },
			/* Crm = 7 */ {"Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero", "Read-As-Zero" }
		     };
		     printf(" -> FSER %s\n", crn0_opc10_to_string[crm(inst)][opc2(inst)]);
		  }
		  break;
	       case 1:
		  if (crm(inst) != 0)
		  {
		     printf("How can crm not be 0?!\n"); for(;;);
		  }
		  else
		  {
		     switch (opc2(inst))
		     {
			case 0:
			   printf(" -> FSER CCSIDR\n");
			   break;
			case 1:
			   printf(" -> FSER CLIDR\n");
			   break;
			case 7:
			   printf(" -> FSER AIDR\n");
			   break;
			default:
			   printf(" -> NO COMPRENDO\n");
			   for(;;);
		     }
		  }
		  break;
	       case 2:
		  if (crm(inst) != 0 || opc2(inst) != 0)
		  {
		     printf(" -> NO COMPRENDO (opc2 and crm not nuls)\n");
		     for (;;);
		  }
		  break;
	       default:
		  printf("Unknown crn = %d opc1 = %d\n", crn(inst), opc1(inst));
		  for(;;);
	    }
	    break;
	 case 1:
	    if (opc1(inst) == 0)
	    {
	       if (crm(inst) == 0)
	       {
		  char *text_label[3] = { "SCTLR", "ACTLR", "CPACR" };
		  if (opc2(inst) > 3)
		  {
		     printf("opc2 > 3\n");
		     for (;;);
		  }
		  else
		  {
		     printf(" -> FSER %s\n", text_label[opc2(inst)]);
		  }
	       }
	       else if (crm(inst) == 1)
	       {
		  char *text_label[3] = { "SCR", "SDER", "NSACR" };
		  if (opc2(inst) > 3)
		  {
		     printf("opc2 > 3\n");
		     for (;;);
		  }
		  else
		  {
		     printf(" -> FSER %s\n", text_label[opc2(inst)]);
		  }
	       }
	       else
	       {
		  printf("Unknown crm = %d\n", crm(inst));
		  for(;;);
	       }
	    }
	    else if (opc1(inst) == 4)
	    {
	       if (crm(inst) == 0)
	       {
		  char *text_label[2] = { "HSCTLR", "HACTLR"};
		  if (opc2(inst) > 2)
		  {
		     printf("opc2 > 2\n");
		     for (;;);
		  }
		  else
		  {
		     printf(" -> FSER %s\n", text_label[opc2(inst)]);
		  }
	       }
	       else if (crm(inst) == 1)
	       {
		  char *text_label[8] = { "HCR", "HDCR", "HCPTR", "HSTR", "non doc", "non doc", "non doc", "HACR" };
		  if (opc2(inst) > 8)
		  {
		     printf("opc2 > 8\n");
		     for (;;);
		  }
		  else
		  {
		     printf(" -> FSER %s\n", text_label[opc2(inst)]);
		  }
	       }
	       else
	       {
		  printf("Unknown crm = %d\n", crm(inst));
		  for(;;);
	       }      
	    }
	    else
	    {
	       printf("Unknown opc1 value %d\n", opc1(inst));
	       for (;;);
	    }
	    break;
	 case 2:
	    if (opc1(inst) == 0)
	    {
	       if (crm(inst) != 0)
	       {
		  printf("crm should be nul (%d)\n", crm(inst));
		  for(;;);
	       }
	       else
	       {
		  if (opc2(inst) > 2)
		  {
		     printf("opc2 > 2\n"); for(;;);
		  }
		  else
		  {
		     char *text_label[3] = { "TTBR0", "TTBR1", "TTBCR"};	 
		     printf(" -> FSER %s\n", text_label[opc2(inst)]);
		  }
	       }
	    }
	    else if (opc1(inst) == 4)
	    {
	       if (opc2(inst) != 2)
	       {
		  printf(" OPC2 should be equals 2\n");
		  for(;;);
	       }
	       else
	       {
		  if (crm(inst) == 0)
		  {
		     printf(" -> FSER HTCR\n");
		  }
		  else if (crm(inst) == 1)
		  {
		     printf(" -> FSER VTCR\n");
		  }
		  else
		  {
		     printf("Unknown crm value (%d)\n", crm(inst));
		     for(;;);
		  }
	       }
	    }
	    else
	    {
	       printf("CRN = 2 : Unknown opc1 value (%d)\n", opc1(inst));
	       for(;;);
	    }

	    break;
	 case 3:
	    if (opc1(inst) != 0 || crm(inst) != 0 || opc2(inst) != 0)
	    {
	       printf("CRN = 3 => opc1 = crm = opc2 = 0\n");
	       for (;;);
	    }
	    else
	    {
	       printf(" -> FSER DACR\n");
	    }
	    break;
	 case 7:
	    if (opc1(inst) == 0)
	    {
	       switch (crm(inst))
	       {
		  case 0:
		     if (opc2(inst) == 4)
		     {
			printf(" -> FSER Unpredictable (was wait for interrupt)\n");
		     }
		     else
		     {
			printf(" -> UNKNOWN value\n");
			for(;;);
		     }
		     break;
		  case 1:
		     if (opc2(inst) == 0)
		     {
			printf(" -> FSER ICIALLUIS\n");
		     }
		     else if (opc2(inst) == 6)
		     {
			printf(" -> FSER BPIALLIS\n");
		     }
		     else
		     {
			printf("UNKNOWN value opc2 (%d)\n", opc2(inst));
			for(;;);
		     }
		     break;
		  case 4:
		     if (opc2(inst) == 0)
		     {
			printf(" -> FSER PAR\n");
		     }
		     else
		     {
			printf(" UNKNOWN value opc2 (%d)\n", opc2(inst));
			for(;;);
		     }
		     break;
		  case 5:
		     switch (opc2(inst))
		     {
			case 0:
			   printf(" -> FSER ICIALLU\n");
			   break;
			case 1:
			   printf(" -> FSER ICIMVAU\n");
			   break;
			case 4:
			   printf(" -> FSER CP15ISB\n");
			   break;
			case 6:
			   printf(" -> FSER BPIALL\n");
			   break;
			case 7:
			   printf(" -> FSER BPIMVA\n");
			   break;
			default:
			   printf("UNKNOWN opc2 value (%d)\n", opc2(inst));
		     }
		     break;
		  case 6:
		     if (opc2(inst) == 1)
		     {
			printf(" -> FSER DCIMVAC\n");
		     }
		     else if (opc2(inst) == 2)
		     {
			printf(" -> FSER DCISW\n");
		     }
		     else
		     {
			printf("UNKNOWN opc2 value (%d)\n", opc2(inst));
			for(;;);
		     }
		     break;
		  case 8: 
		     if (opc2(inst) > 7)
		     {
			printf("opc2 > 7\n");
			for(;;);
		     }
		     else
		     {
			char *text_label[8] = { "HCR", "HDCR", "HCPTR", "HSTR", "non doc", "non doc", "non doc", "HACR" };
			printf(" -> FSER %s\n", text_label[opc2(inst)]);
		     }
		     break;
		  case 10:
		     switch (opc2(inst))
		     {
			case 1:
			   printf(" -> FSER DCCMVAC\n");
			   break;
			case 2:
			   printf(" -> FSER DCCSW\n");
			   break;
			case 4:
			   printf(" -> FSER CP15DSB\n");
			   break;
			case 5:
			   printf(" -> FSER CP15DMB\n");
			   break;
			default:
			   printf("UNKNOWN value opc2 (%d)\n", opc2(inst));
		     }
		     break;
		  case 11:
		     if (opc2(inst) == 1)
		     {
			printf(" -> FSER DCCMVAU\n");
		     }
		     else 
		     {
			printf("UNKNOWN value opc2 (%d)\n", opc2(inst));
			for(;;);
		     }
		     break;
		  case 13:
		     if (opc2(inst) == 1)
		     {
			printf(" -> FSER Unpredictable\n");
		     }
		     else 
		     {
			printf("UNKNOWN value opc2 (%d)\n", opc2(inst));
			for(;;);
		     }
		     break;
		  case 14:
		     if (opc2(inst) == 1)
		     {
			printf(" -> FSER DCCIMVAC\n");
		     }
		     else if (opc2(inst) == 2)
		     {
			printf(" -> FSER DCCISW\n");
		     }
		     else
		     {
			printf("UNKNOWN value opc2 (%d)\n", opc2(inst));
			for(;;);
		     }

		     break;
		  default:
		     printf("CRN = 7, Unknown crm value %d\n", crm(inst));
		     for (;;);
	       }
	    }
	    else if (opc1(inst) == 4)
	    {
	       if (crm(inst) == 8)
	       {
		  if (opc2(inst) == 0)
		  {
		     printf(" -> FSER ATS1HR\n");
		  }
		  else if (opc2(inst) == 1)
		  {
		     printf(" -> FSER ATS1HW\n");
		  }
		  else
		  {
		     printf(" UNKNOWN value for opc2 (%d)\n", opc2(inst));
		     for(;;);
		  }
	       }
	       else
	       {
		  printf("Unknown crm value (%d)\n", crm(inst));
		  for(;;);
	       }
	    }
	    else 
	    {
	       printf(" -> UNKNOWN value for opc1 (%d)\n", opc1(inst));
	       for(;;);
	    }
	    break;
	 case 8:
	    switch (opc1(inst))
	    {
	       case 0:
		  switch (crm(inst))
		  {
		     case 3:
			if (opc2(inst) > 3)
			{
			   printf("opc2 > 3\n");
			   for(;;);
			}
			else
			{
			   char *text_label[4] = {"TLBIALLIS", "TLBIMVAIS", "TLBIASIDIS", "TLBIMVAAIS"};
			   printf(" -> FSER %s\n", text_label[opc2(inst)]);
			}
			break;
		     case 5:
			if (opc2(inst) > 2)
			{
			   printf("opc2 > 2\n");
			   for(;;);
			}
			else
			{
			   char *text_label[3] = {"ITLBIALL", "ITLBIMVA", "ITLBIASID"};
			   printf(" -> FSER %s\n", text_label[opc2(inst)]);
			}
			break;
		     case 6:
			if (opc2(inst) > 2)
			{
			   printf("opc2 > 2\n");
			   for(;;);
			}
			else
			{
			   char *text_label[3] = {"DTLBIALL", "DTLBIMVA", "DTLBIASID"};
			   printf(" -> FSER %s\n", text_label[opc2(inst)]);
			}
			break;
		     case 7:
			if (opc2(inst) > 3)
			{
			   printf("opc2 > 3\n");
			   for(;;);
			}
			else
			{
			   char *text_label[4] = {"TLBIALL", "TLBIMVA", "TLBIASID", "TLBIMVAA"};
			   printf(" -> FSER %s\n", text_label[opc2(inst)]);
			}
			break;
		     default:
			printf("UNKNOWN stuff\n");
			for(;;);
		  }
		  break;
	       case 4:
		  printf(" -> FSER GRMPF TODO\n");
		  break;
	       default:
		  printf("Unknown value opc1 (%d)\n", opc1(inst));
		  for(;;);
	    }
	    break;
	 case 10:
	    switch (opc1(inst))
	    {
	       case 0:
		  switch (crm(inst))
		  {
		     case 0:
		     case 1:
		     case 4:
		     case 8:
			if (opc2(inst) > 7)
			{
			   printf("opc2 > 7"); for(;;);
			}
			else
			{
			   printf(" -> FSER Reserved for lockdown\n");
			}
			break;
		     case 2:
			if (opc2(inst) == 0)
			{
			   printf(" -> FSER PRRR or MAIR0\n");
			}
			else if (opc2(inst) == 1) 
			{
			   printf(" -> FSER NMRR or MAIR1\n");
			}
			else
			{
			   printf("UNKNOWN value for opc2 %d\n", opc2(inst));
			   for(;;);
			}
			break;
		     case 3:
			if (opc2(inst) == 0)
			{
			   printf(" -> FSER AMAIR0\n");
			}
			else if (opc2(inst) == 1) 
			{
			   printf(" -> FSER AMAIR1\n");
			}
			else
			{
			   printf("UNKNOWN value for opc2 %d\n", opc2(inst));
			   for(;;);
			}
			break;
		     default:
			printf("Unknown value %d\n", crm(inst));
		  }
		  break;
	       case 1:
	       case 2:
	       case 3:
		  switch (crm(inst))
		  {
		     case 0:
		     case 1:
		     case 4:
		     case 8:
			printf(" -> FSER Reserved for TLB lockdown operation\n");
			break;
		     default:
			printf("UNKNOWN crm value (%d)\n", crm(inst));
			for(;;);
		  }
		  break;
	       case 4:
		  switch (crm(inst))
		  {
		     case 0:
		     case 1:
		     case 4:
		     case 8:
			printf(" -> FSER Reserved for TLB lockdown operation\n");
			break;
		     case 2:
			if (opc2(inst) == 0 || opc2(inst) == 1)
			   printf("HMAIR%d\n", opc2(inst));
			else
			{
			   printf("Wrong value\n");
			   for(;;);
			}
			break;
		     case 3:
			if (opc2(inst) == 0 || opc2(inst) == 1)
			   printf("HAMAIR%d\n", opc2(inst));
			else
			{
			   printf("Wrong value\n");
			   for(;;);
			}
			break;
		     default:
			printf("UNKNOWN crm value (%d)\n", crm(inst));
			for(;;);
		  }
		  break;
	       case 5:
	       case 6:
	       case 7:
		  switch (crm(inst))
		  {
		     case 0:
		     case 1:
		     case 4:
		     case 8:
			if (opc2(inst) > 7)
			{
			   printf("Wrong value for opc2\n"); for (;;);
			}
			else
			{
			   printf(" -> FSER Reserved for TLB lockdown operation\n");
			}
		  }
		  break;
	       default:
		  printf("UNKNOWN value %d\n", opc1(inst));
		  for(;;);
	    }
	    break;
	 default:
	    printf("Unknown %d !!\n", crn(inst));
	    for(;;);
      }
   }
   else
   {
      printf("Unhandled (print) copro %d\n", coproc(inst));
   }
#undef rt
#undef crn
#undef crm
#undef opc1
#undef opc2
#undef coproc

}
