/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include <stdint.h>
#include "debug.h"
#include "memset.h"
#include "memcpy.h"
#include "abort.h"
#include "arm_uart_io.h"
#include "hypervisor.h"
#include "arm_trampolines.h"
#include "arm_instr_info.h"
#include "arm_behavior_functions.h"
#include "reset.h"
#include "guest_image.h"
#include "arm_mmu.h"
#include "arm_execute.h"
#include "debug.h"
#include "arm_perf_counter.h"
#include "printf.h"
#include "hypervise.h"
#include "cache.h"
#include "bench_internals.h"
#include "output.h"

extern addr_t hypervisor_physical_base;
extern int begin_vector;

extern void print_size(void);
extern void init_table(void);
extern void print_table_match(void);


extern void arm_stop(void);

static void zero_bss(void)
{
   extern unsigned char _bss_start;
   extern unsigned char _bss_end;
   uint32_t bss_size = &_bss_end - &_bss_start;
   memset(&_bss_start, 0, bss_size);
}

static void copy_guest(void)
{
   extern unsigned char _guest_data_start;
   extern unsigned char guest_image_start;
   extern unsigned char guest_image_end;
   memcpy(&guest_image_start, &_guest_data_start, &guest_image_end - &guest_image_start);
}

extern void guest_static_analysis(int mode, int pass_count);


void bootstrap_main() {
   uint32_t begin, end;
    extern Hypervisor_Behavior_Function arm_behavior_runtime_functions[NB_BEHAVIOR];

    /* Start by first zeroing bss */
    zero_bss();

    /* Copy the guest code where it should be executed */
    copy_guest();

#ifdef RUN_ONLY_GUEST
    asm volatile("b guest_image");
#endif	
    
    
    Hypervisor_Platform platform;
    platform.cpu_mode_count = 32;
    platform.io_putc = arm_uart_putc;
    platform.instructionInfo = arm_platform_instruction_info;
    platform.modeIndex = arm_platform_modeIndex;
    platform.getPCValue = arm_platform_getPCValue;
    platform.getReturnValue = arm_platform_getReturnValue;
    platform.setReturnValue = arm_platform_setReturnValue;
    platform.setPCValue = arm_platform_setPCValue;
    platform.copyState = arm_platform_copyState;
    platform.setLastITAddr = arm_platform_setLastITAddr;
    platform.isInITBlock = arm_platform_isInITBlock;
    platform.execute = arm_platform_execute;
    platform.bytesNeededToPatchAddress = arm_platform_bytesNeededToPatchAddress;
    platform.patchAddress = arm_platform_patchAddress;
    platform.unpatchAddress = arm_platform_unpatchAddress;
    platform.stateForMode = arm_platform_saved_state_for_mode;
    platform.init = arm_platform_init;
    platform.execute_step = arm_execute_step;
    platform.isConditionMet = arm_platform_is_condition_met;
    platform.debugDumpState = arm_platform_debugDumpState;
    platform.behavior_runtime_functions = arm_behavior_runtime_functions;
    platform.setCPSR = arm_platform_setCPSR;
    platform.getCPSR = arm_platform_getCPSR;
    platform.init_perfcounters = arm_init_perfcounters;
    platform.get_cyclecount = arm_get_cyclecount;
    platform.set_cyclecount = arm_set_cyclecount;
    platform.init_overflow_timer = arm_init_overflow_timer;
    platform.irq_enable = arm_irq_enable;
    platform.irq_disable = arm_irq_disable;
    platform.destinationMode = arm_platform_get_destination_mode;
    platform.stop = arm_platform_stop;
    platform.get_timer = arm_get_timer;
    platform.get_next_instr = arm_platform_get_next_instr;

    hypervisor_init_platform(&platform);


    printf("HYPERV: starting static analysis pass\n");
    guest_static_analysis(ARMMode_Supervisor, 20);
    printf("HYPERV: done\n");
#ifdef BENCH_INTERNALS
    bench_print_variables();
#endif
    

    log(LOG_HYPERVISOR, "Platform has been set\r\n");
    printf("HYPERV: Starting hypervisor for guest code at %p\n", guest_image);
    begin = arm_get_timer();
    hypervise((uint32_t)guest_image, 0);
    end = arm_get_timer();
    printf("HYPERV: Guest image ended, shutting down (%d)\n", end-begin);

#ifdef BENCH_INTERNALS
    bench_print_variables();
#endif

    arm_platform_stop();
    
    for(;;);
}

