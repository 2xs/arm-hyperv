#!/usr/bin/perl -w

use strict;


sub build
{
    my ($build_mode, $guest, $guest_build_mode) = @_;
    # Clean hypervisor
    $ENV{BUILD}=$build_mode;
    system("make clean > /dev/null");
    # Build guest
    print("Building guest $guest ($guest_build_mode)\n");
    $ENV{BUILD}=$guest_build_mode;
    system("make -BC sample_guests/$guest > /dev/null") == 0 or return 0;
    # Build Hypervisor
    $ENV{BUILD}=$build_mode;
    print("Building in $build_mode\n");
    system("make > /dev/null") == 0 or return 0;
    return 1;
}

sub qemu_killer_process
{
    my ($qemu_pid, $max_time) = @_;
    my $pid = fork();
    return $pid if ($pid != 0);
    sleep $max_time;
    # Check if qemu process is still up
    my $alive = kill(0, $qemu_pid);
    exit 0 unless $alive;
    print("Killing qemu because of a ${max_time}s timeout");
    kill('TERM', $qemu_pid);
    exit 0;
}

sub kill_killer
{
    my ($killer_pid) = @_;
    kill('TERM', $killer_pid);
    wait;
}

sub run
{
    # expected_output is a regexp to match to check that run is successful
    my ($build_mode, $expected_output, $max_time, $verbose) = @_;
    $ENV{BUILD}=$build_mode;
    # Launch qemu and redirect its output for cheching the regexp
    printf("Running qemu for $build_mode mode\n");
    my $qemu_pid = open(QEMU_OUTPUT, "make emu|") or return 0;
    my $ret = 0;
    # Start a child process to wait and kill qemu after $max_time seconds
    my $killer_pid = qemu_killer_process($qemu_pid, $max_time);
    while (<QEMU_OUTPUT>)
    {
        print if $verbose;
        if (/$expected_output/)
        {
            $ret = 1;
            last;
        }
    }
    close(QEMU_OUTPUT);
    kill_killer($killer_pid);
    return $ret;
}

my $success = 0;
my $tests = 0;
my @modes = ("release", "nooptim");
my @guest_modes = ("release", "nooptim");
my @guests = ("test_supervisor_svc", "test_cpsr_inst", "test_user_mode_swi", "clock_ticks", "simple_scheduler");
my @failed = ();

$ENV{BENCH_INTERNALS} = 0;
$ENV{LOG_LEVEL} = 0;

foreach my $build_mode (@modes)
{
    foreach my $guest (@guests)
    {
        foreach my $guest_build_mode (@guest_modes)
        {
            $tests++;
            if (build($build_mode, $guest, $guest_build_mode))
            {
                if (run($build_mode, 'HYPERV.*shutting down.*', 30, 0))
                {
                    print("Success\n");
                    $success++;
                }
                else
                {
                    printf("Failed\n");
                    push(@failed, "($build_mode/$guest/$guest_build_mode)");
                }
            }
            else
            {
                printf("Build Failed\n");
                push(@failed, "build failed ($build_mode/$guest/$guest_build_mode)");
            }
        }
    }
}

print("$success/", $tests , " test passed\n");
if ($success != $tests)
{
    die "Failing tests: @failed\n";
}
