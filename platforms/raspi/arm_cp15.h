/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __ARM_CP15_H__
#define __ARM_CP15_H__

#include <stdint.h>

typedef union
{
   uint32_t value;
   struct { /* TODO: reverse me, or check me */
      unsigned zeRo:1;
      unsigned TE:1;
      unsigned AFE:1;
      unsigned TRE:1;
      unsigned NMFI:1;
      unsigned zEro:1;
      unsigned EE:1;
      unsigned VE:1;
      unsigned ONE:1;
      unsigned U:1;
      unsigned FI:1;
      unsigned UWXN:1;
      unsigned WXN:1;
      unsigned onE:1;
      unsigned HA:1;
      unsigned oNe:1;
      unsigned zero:1;
      unsigned RR:1;
      unsigned V:1;
      unsigned I:1;
      unsigned Z:1;
      unsigned SW:1;
      unsigned zeros:2;
      unsigned B:1;
      unsigned one:1;
      unsigned BEN:1;
      unsigned ones:2;
      unsigned C:1;
      unsigned A:1;
      unsigned M:1;
   } fields;
} CP15_SCTLR;

void arm_write_cr1(uint32_t val);
void arm_write_dacr(uint32_t val);
void arm_write_ttbr0(uint32_t val);
void arm_write_ttbr1(uint32_t val);
void arm_flush_tlb(void);
uint32_t arm_read_cr1(void);
uint32_t arm_read_sctlr(void);
void arm_write_sctlr(uint32_t val);
#endif /* __ARM_CP15_H__ */
