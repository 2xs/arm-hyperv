MAX_STATE_B_SIZE=8
MAX_STATE_SIZE=(1<<MAX_STATE_B_SIZE)
MAX_MODE_COUNT=32
MAX_HYPERVISOR_STACK_B_SIZE=17
MAX_HYPERVISOR_STACK_SIZE=(1<<MAX_HYPERVISOR_STACK_B_SIZE)

STATE_SP_OFFSET=13*4
STATE_LR_OFFSET=14*4
STATE_PC_OFFSET=15*4
STATE_CPSR_OFFSET=STATE_PC_OFFSET + 4
STATE_SPSR_OFFSET=STATE_CPSR_OFFSET + 4
STATE_PATCH_ADDR_OFFSET=STATE_SPSR_OFFSET + 4

SUPERVISOR_MODE= 0b10011
USER_MODE=       0b10000
FIQ_MODE=        0b10001
IRQ_MODE=        0b10010
ABT_MODE=        0b10111
UND_MODE=        0b11011
SYS_MODE=        0b11111
MON_MODE=         0b10110

        
/* Macro that are used multiple times */
/* Loads the CPU mode in r0 and cpsr in r2 */
.macro arm_get_cpu_mode
    mrs r2, cpsr
    and r0, r2, #0x1F                           /*  cpsr & 0x1F = CPU_mode */
.endm

/* loads the state pointer in r0 depending on the mode on r0 */
.macro arm_get_current_state
        ldr r1, =cpu_states
        add r0, r1, r0, lsl #MAX_STATE_B_SIZE       /*  r0 = &temp_saved + (CPU_mode * MAX_STATE_SIZE) */
        /* or simply r0 = state where state is a ARM_SavedState */
.endm
        
/* Sets SP to a tempory stack to run hypervisor */
.macro arm_set_hyp_stack
    ldr r1, =hyp_stack
    and r2, r2, #0x1F
    add r2, r2, #1
    add sp, r1, r2, lsl #MAX_HYPERVISOR_STACK_B_SIZE
.endm
/* Disable interuptions */        
.macro arm_disable_it
    cpsid i
.endm

/* Saves all registers to current state */
/* state address is in r0, save address of r0-r2 is in r1, cpsr is in r2 */
.macro arm_save_registers_to_state
    /*  Save r3-r14 */
    add r0, r0, #12  /* r0 = &(state->context.gpr.name.r3) */
    stm r0, {r3-r14} /* save r3-r14 to corresponding state fields */
    sub r0, r0, #12  /* r0 = state */

    /*  Save CPSR */
    str r2, [r0, #STATE_CPSR_OFFSET] /* state->context.cpsr.value = cpsr */
    mov r3, #0    
    /* Save SPSR */
    and r2,r2, #0x1F
    cmp r2, #SYS_MODE
    addeq r3, r3, #1
    cmp r2, #USER_MODE
    addeq r3, r3, #1
    mrseq r2, spsr /* load SPSR ONLY if not in sys mode and not in user mode */
    streq r2, [r0, #STATE_SPSR_OFFSET] /* state->context.spsr.value = spsr */
    /*  Save the original values of r0, r1 and r2 */
    ldm r1, {r3-r5}          /* r3 = saved_r0; r4 = saved_r1; r5 = saved_r2 */
    stm r0, {r3-r5}          /* state->context.gpr.name.r0 = r3; ... */
.endm
        
        .section .text

	/*  Return to the hypervised program */
	/*  R0: saved state */
	
	.globl arm_platform_execute
	.type arm_platform_execute, %function
	
	.arm

arm_platform_execute:
    /* Register usage:
        r0 = state address
        r3 = scratch_return_address
        r2 = pc
        r1 = cpsr
        r4 = temp register
    */
    ldr r3, =scratch_return_address
    ldr r2, [r0, #STATE_PC_OFFSET]      /*  Load PC value from the saved state */

    /*  Load CPSR in r1 to check mode */
    ldr r1, [r0, #STATE_CPSR_OFFSET]
    /* Check mode and load SPSR ONLY if not in system or user mode to avoid UNPREDICTABLE */    
    and r4, r1, #0x1F    
    cmp r4, #SYS_MODE
    beq skip_spsr
    cmp r4, #USER_MODE
    beq skip_spsr    
    /* Same with SPSR. The CPSR is done first to change the SPSR of the right mode */
    ldr r4, [r0, #STATE_SPSR_OFFSET]
    msr cpsr_fsxc, r1
    msr spsr_fsxc, r4
    b cont_restore_    
skip_spsr:
    /* case where only cpsr is restored */
    msr cpsr_fsxc, r1
cont_restore_:  
    /*  Store PC value */
    and r1, r1, #0x20                   /*  Read the Thumb bit */
    orr r2, r2, r1, lsr #5              /*  And set the LSB of the PC according to the saved Thumb state */
    str r2, [r3]                        /*  Store into scrath memory area */

    /*  Load all GPR register values */
    ldr sp, [r0, #STATE_SP_OFFSET]
    ldr lr, [r0, #STATE_LR_OFFSET]    
    ldm r0, {r0-r12}

    /*  Return to the hypervised program */
    ldr pc, [pc, #-4]
scratch_return_address:
    .word 0

    
    /*  The hypervised program returns to the hypervisor */
    .globl arm_platform_comeback_arm
    .type arm_platform_comeback_arm, %function
    .arm
arm_platform_comeback_arm:

    str r0, [pc, #16]
    mrs r0, cpsr                /* save cpsr */
    cpsid i                     /* disable interrupts asap */
    str r1, [pc, #8]
    str r2, [pc, #8]
    b _cont
_temp_save_r0r1r2:
    .word 0
    .word 0
    .word 0
_cont:
    /*  Read the current CPU mode, in order to choose the correct saved structure */
    mov r2, r0 /* save cpsr in r2 */
    and r0, r2, #0x1F                           /*  cpsr & 0x1F = CPU_mode */
    arm_get_current_state /* r0 = state */    
    ldr r1, =_temp_save_r0r1r2 /* r1 is the address of the saved r0-r2 values */

    arm_save_registers_to_state /* saves r0-r14 to current state */
    cps #SUPERVISOR_MODE    
    /*  The saved PC register is set to the breakpoint address */
    ldr r1, [r0, #STATE_PATCH_ADDR_OFFSET]
    str r1, [r0, #STATE_PC_OFFSET]

    arm_set_hyp_stack
	
    b arm_platform_handle_return

/* //////////////////////////////////////////////////// */
    /*  Returns the address of the saved state for the mode given in R0 */
    /*  R0: mode index */
    .globl arm_platform_saved_state_for_mode
    .type arm_platform_saved_state_for_mode, %function
    .arm
arm_platform_saved_state_for_mode:
    push {r1}
    arm_get_current_state /* r0 = state */    
    pop {r1}
    bx lr

   .globl arm_trampoline_hypervise_restart
arm_trampoline_hypervise_restart:
    /* The hypervisor will need to analyse code at LR */
    /* so, we will save guest state, and call hypervisor_execute with this state */
    
    /* General algorithm is
        - save current context in current state for current mode
        - set the hypervisor stack
        - branch in hypervisor_execute with the state address in r0 (state) and 0 in r1 (skip_first)
    */

    // save r0-r2
    str r0, [pc, #8]
    str r1, [pc, #8]
    str r2, [pc, #8]
    b _cont_restart
_temp_save_r0r1r2_restart:
    .word 0
    .word 0
    .word 0
_cont_restart:

    /*  Read the current CPU mode, in order to choose the correct saved structure */
    arm_get_cpu_mode /* r0 = CPU_Mode, r2 = cpsr */
    arm_get_current_state /* r0 = state */    

    ldr r1, =_temp_save_r0r1r2_restart /* r1 is the address of the saved r0-r2 values */

    arm_save_registers_to_state /* saves r0-r14 to current state */
    /* Disable interruptions */
    arm_disable_it    

    /*  The saved PC register is set to LR */
    str lr, [r0, #STATE_PC_OFFSET]

    /*  Setup a temporary stack for the hypervisor */
    arm_set_hyp_stack

    /* The state is already in r0, just need to set r1 to 0 (for skip_next) */
    mov r1, #0
    b hypervisor_execute

   .globl arm_trampoline_hypervise_restart_it
arm_trampoline_hypervise_restart_it:
    /* This code is called from an hypervisor IT.
    Its goal is to restart the analyze of a guest IT.
    The changes are minimal compared to the one above.
    The ONLY difference is that LR is NOT saved back in the state so that
    it can be saved to the LR of the IT before jumping here.
    */
    

    // save r0-r2
    str r0, [pc, #8]
    str r1, [pc, #8]
    str r2, [pc, #8]
    b _cont_restart_it
_temp_save_r0r1r2_restart_it:
    .word 0
    .word 0
    .word 0
_cont_restart_it:

    /*  Read the current CPU mode, in order to choose the correct saved structure */
    arm_get_cpu_mode /* r0 = CPU_Mode, r2 = cpsr */
    arm_get_current_state /* r0 = state */    

    ldr r1, =_temp_save_r0r1r2_restart_it /* r1 is the address of the saved r0-r2 values */

    /*  Save r3-r13 do NOT trash the state's LR */
    add r0, r0, #12  /* r0 = &(state->context.gpr.name.r3) */
    stm r0, {r3-r13} /* save r3-r13 to corresponding state fields */
    sub r0, r0, #12  /* r0 = state */

    /*  Save CPSR */
    str r2, [r0, #STATE_CPSR_OFFSET] /* state->context.cpsr.value = cpsr */
    /* Save SPSR */    
    mrs r2, spsr
    str r2, [r0, #STATE_SPSR_OFFSET] /* state->context.spsr.value = spsr */
        
    /*  Save the original values of r0, r1 and r2 */
    ldm r1, {r3-r5}          /* r3 = saved_r0; r4 = saved_r1; r5 = saved_r2 */
    stm r0, {r3-r5}          /* state->context.gpr.name.r0 = r3; ... */

    /*  The saved PC register is set to LR */
    str lr, [r0, #STATE_PC_OFFSET]

    /* Disable interruptions  */
    arm_disable_it
    /*  Setup a temporary stack for the hypervisor */
    arm_set_hyp_stack

    /* The state is already in r0, just need to set r1 to 0 (for skip_next) */
    mov r1, #0
    b hypervisor_execute
        
    .ltorg

/* //////////////////////////////////////////////////// */

    .section .bss
hyp_stack:
    .space MAX_HYPERVISOR_STACK_SIZE*MAX_MODE_COUNT

cpu_states:
    .space MAX_STATE_SIZE*MAX_MODE_COUNT
