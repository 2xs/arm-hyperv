#include "arm_uart_io.h"

typedef void (*hypervise_api_t)(void);

/* Get the pointers to hypervisor API calls */
hypervise_api_t hypervise_restart = (hypervise_api_t) 0x9000;
hypervise_api_t hypervise_stop = (hypervise_api_t) 0x9100;

void main(void)
{
   arm_uart_puts("Stoping hypervision\n");
   hypervise_stop();
   arm_uart_puts("Starting hypervision\n");
   hypervise_restart();
   arm_uart_puts("Hypervised print\n");
   hypervise_stop();
   arm_uart_puts("Non Hypervised print\n");
   hypervise_restart();
}
