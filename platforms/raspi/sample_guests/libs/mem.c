#include "mem.h"

void *memset(void *s, int c, size_t n)
{
   unsigned char *ptr = (unsigned char*)s;
   size_t i;
   for (i = 0 ; i < n ; ++i)
      ptr[i] = (unsigned char) c;
   return s;
}

int memcmp(const void *s1, const void *s2, size_t n)
{
   unsigned char *ptr1 = (unsigned char*)s1;
   unsigned char *ptr2 = (unsigned char*)s2;
   size_t i;
   for (i = 0 ; i < n ; ++i)
   {
      if (ptr1[i] != ptr2[i])
         return (ptr1[i] - ptr2[i]);
   }
   return 0;
}

void *memcpy(void *dest, const void *src, size_t n)
{
   unsigned char *ptr1 = (unsigned char*)dest;
   unsigned char *ptr2 = (unsigned char*)src;
   size_t i;
   for (i = 0 ; i < n ; ++i)
   {
      ptr1[i] = ptr2[i];
   }
   return dest;
}
