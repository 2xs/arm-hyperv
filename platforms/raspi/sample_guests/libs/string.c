int strcmp(const char *s1, const char *s2){
  int i;

  for(i=0; s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0';i++);

  if(s1[i] > s2[i])
    return 1;
  else if (s1[i] < s2[i])
    return -1;
  else
    return 0;
}

char *strcpy(char *dest, const char *src){
  char *d = (char *) dest;
  char *s = (char *) src;
  while (*s != 0) {
    *d++ = *s++;
  }

  return dest;
}



