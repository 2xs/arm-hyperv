#include "rpi-armtimer.h"
#include "rpi-interrupt.h"

static rpi_arm_timer_t* rpiArmTimer = (rpi_arm_timer_t*)RPI_ARMTIMER_BASE;

rpi_arm_timer_t* RPI_GetArmTimer(void)
{
   return rpiArmTimer;
}

#define RPI_APB_CLOCK 250000000 /* APB clock is 250 Mhz */
#define TIMER_CLOCK (RPI_APB_CLOCK >> 7) /* Timer clock is APB clock / 128 
                                             (we set the predivider register to 127) */
void RPI_RunTimer(uint32_t frequency)
{
   /* Setup the system timer interrupt */
   RPI_GetArmTimer()->Load = TIMER_CLOCK / frequency; /* Counter is decremented at each tick of TIMER_CLOCK */
   RPI_GetArmTimer()->PreDivider = 127; /* timer_clock = apb_clock/(predivider+1) */
   
   /* Setup the ARM Timer */
   RPI_GetArmTimer()->Control =
      RPI_ARMTIMER_CTRL_23BIT |
      RPI_ARMTIMER_CTRL_ENABLE |
      RPI_ARMTIMER_CTRL_INT_ENABLE; /* No prescale, thus the counter is decremented at each clock */
   
   /* Enable Timer IRQ */
   RPI_GetIrqController()->Enable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;
}

void RPI_ClearTimerIRQ(void)
{
   RPI_GetArmTimer()->IRQClear = 1;
}
