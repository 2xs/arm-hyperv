#include <stdint.h>
#include "arm_uart_io.h"
#include "printf.h"


#define assert(x) if (!(x)) { printf("Assertion %s failed (%x) at %s:%d\n", #x, cpsr_value, __FILE__, __LINE__); for (;;);}

void putchar(char c)
{
   arm_uart_putc(c);
}


#define READ_CPSR asm volatile("mrs %0, cpsr" :"=r"(cpsr_value))

void main(void)
{
   uint32_t cpsr_value;

   asm volatile("mov r0, #19");
   asm volatile("msr cpsr_fsxc, r0");
   asm volatile("mov r1, #12");
   asm volatile("mov r2, #76");
   asm volatile("cmp r1, r2");
   
   asm volatile("cpsid i");
   asm volatile("cpsie f");
   asm volatile("cpsid a");


   asm volatile("cps #19");
   READ_CPSR;
   assert(cpsr_value == 0x80000193);

   asm volatile("cps #31");
   READ_CPSR;
   asm volatile("cps #19");
   assert(cpsr_value == 0x6000019f);
   
   printf("Test passed\n");
   asm volatile("udf");
}
