#ifndef __SYS_H__
#define __SYS_H__

/* Syscall ABI:
   r0: syscall ID
   r1-r3: syscall parameters
   r0: return value if any
*/

/** Syscalls IDs */
enum
{
   SYS_EXIT,
   SYS_PUTS,
   SYS_PUTI,
   SYS_PUTC,
   SYS_NOP,
};

#endif
