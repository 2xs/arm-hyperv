#include "../syscalls.h"
#include "../printf.h"
#include "../defines.h"

#include "aes.h"
#include "perf_timer.h"

#define DECLARE_BENCH static uint32_t __start, __end, __runs,__count
#define START_BENCH(c)   __count = __runs = (c); __start = GET_PERF_TIMER_CLO; for ( ; __count ; --__count ) {
#define END_BENCH(x)  } do { __end = GET_PERF_TIMER_CLO; printf("BENCH %s_%s,%d,%d\n", (x), current_mode == SUPERVISOR_MONO_TASK ? "single_task" : "multi_task", (__end-__start), __runs); } while(0)


enum bench_mode {
   SUPERVISOR_MONO_TASK,
   USER_MULTI_TASK,
};
static int current_mode = 0;
static void (*exit_func)(void);

static volatile int bench_running;

void dummy_exit(void)
{
}


extern void dhrystone(void);
void dhrystone_task(void)
{
   DECLARE_BENCH;
   START_BENCH(200);
   dhrystone();
   END_BENCH("Dhrystone");
}

void AES_task(void)
{
   DECLARE_BENCH;
   START_BENCH(5);
   mbedtls_aes_self_test(0);
   END_BENCH("AES");
}

void syscall_bench_task(void)
{
   DECLARE_BENCH;
   START_BENCH(100);
   nop();
   END_BENCH("NOP SYSCALL");
   START_BENCH(100);
   puts("\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m\e[39m");
   END_BENCH("PUTS SYSCALL");
}

void generic_user_task(int task_id)
{
   printf("Task %d: Starting\n", task_id);
   while (bench_running)
   {
      DELAY(10000000);
   }
   printf("Task %d: ending\n", task_id);
   exit_func();
}


int fibo(int n){
  if(n==0 || n==1)
    return n;
  else
    return fibo(n-1) + fibo(n-2);
}

void fibo_task(void)
{
   DECLARE_BENCH;
   START_BENCH(1);
   for (int i = 0 ; i < 30 ; ++i)
   {
      fibo(i);
   }
   END_BENCH("Fibo");
}


void bench_task(int id)
{
   printf("Running bench in task %d\n", id);
   syscall_bench_task();
   fibo_task();
   dhrystone_task();
   AES_task();
   bench_running = 0;
   exit_func();
}


#include "../scheduler.h"
#include "arm_uart_io.h"

void setup_user_tasks(void)
{
   int error = 0;

   putchar_func = arm_uart_putc;
   exit_func = exit;
   current_mode = USER_MULTI_TASK;
   bench_running = 1;
   
   error += setup_task(generic_user_task, 0);
   error += setup_task(generic_user_task, 1);
   error += setup_task(generic_user_task, 2);
   error += setup_task(generic_user_task, 3);
   error += setup_task(generic_user_task, 4);

   error += setup_task(bench_task,5);

   if (error < 0)
   {
      arm_uart_puts("Error creating some tasks\n");
      for(;;);
   }
}

void run_micro_benchmarks(void)
{
   putchar_func = arm_uart_putc;
   exit_func = dummy_exit;
   current_mode = SUPERVISOR_MONO_TASK;
   /*task1();*/
   fibo_task();
   AES_task();
}
