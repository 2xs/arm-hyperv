#ifndef __TASKS_H__
#define __TASKS_H__

/** Setup tasks that will run in user mode, scheduled by the scheduler */
extern void setup_user_tasks(void);

/** Run the same tasks, but in supervisor mode, without scheduling */
extern void run_micro_benchmarks(void);

#endif
