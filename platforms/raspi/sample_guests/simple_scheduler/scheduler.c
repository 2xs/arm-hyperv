#include <stdint.h>
#include <stdlib.h> /* NULL */
#include "cps.h"

#include "defines.h"
#include "rpi-armtimer.h"
#include "arm_uart_io.h"


#define USER_STACK_SIZE 4*1024
#define MAX_TASKS 10


#define ABORT() for(;;)

enum
{
   EMPTY,
   RUNNING,
   READY,
   STOPPED,
}; 

enum
{
   R0,
   R1,
   R2,
   R3,
   R4,
   R5,
   R6,
   R7,
   R8,
   R9,
   R10,
   R11,
   R12,
   R13,
   SP=R13,
   R14,
   LR=R14,
   R15,
   PC=R15
};

typedef struct task_control
{
   int state;
   uint32_t regs[16];
   uint32_t cpsr;
   uint8_t stack[USER_STACK_SIZE];
   struct task_control *next;
} task_control_t;

static task_control_t *current_task = NULL;
static task_control_t user_tasks[MAX_TASKS];

int get_task_idx(task_control_t *t)
{
   if (!t)
      return 0;
   return t - user_tasks;
}

task_control_t *get_first_task_from_state(int state)
{
   for (int i = 0 ; i < MAX_TASKS ; ++i) /* search from search_start to end */
      if (user_tasks[i].state == state)
         return &user_tasks[i];
   return NULL;
}


int setup_task(void (*task)(int param), int param)
{
   task_control_t *t = get_first_task_from_state(EMPTY);
   if (!t)
      return -1;
   if (current_task == NULL)
      current_task = t;
   else
   {
      task_control_t *node = current_task;
      while (node->next != current_task)
         node = node->next;
      node->next = t;
   }
   t->next = current_task;
   
   /* Init registers */
   for (int i = 1 ; i < R14 ; ++i)
      t->regs[i] = (0xCAFE << 16) | get_task_idx(t);
   t->regs[0] = param; /* parameter to pass to task function */
   t->cpsr = 16; /* Set mode bits to user mode */
   t->cpsr &= ~(1 << 7); /* Enable IRQ */
   /* Set process stack */
   t->regs[SP] = (uint32_t) (t->stack + USER_STACK_SIZE);
   /* Set process PC value */
   t->regs[PC] = (uint32_t) task;
   /* Set process state to READY */
   t->state = READY;
   return 1;
}



void elect(void)
{
   if (!current_task)
      ABORT();
   /* For the first election, the current_task is ready, not running
      select this one
   */
   if (current_task->state == READY)
   {
      current_task->state = RUNNING;
      return;
   }
   /* Otherwise, choose next task */
   current_task->state = READY;
   current_task = current_task->next;
   /* Next can never be NULL on a READY or RUNNING task */
   if (!current_task)
      ABORT();
   current_task->state = RUNNING;
}

void remove_current_task(void);
/* Switch to next task */
/* This is executed in IRQ mode.
   In this mode, r0-r12 are same registers as in user mode
   r13 (sp) & r14 (lr) are banked registers

   This supposes that the current running task state has been correctly saved
   in the corresponding structure by the IRQ handler
*/
void NAKED schedule(void)
{
   if (current_task->state == STOPPED)
   {
      remove_current_task();
   }
   else
   {
      /* select new task */
      elect();
   }
   /* Restore new task context */
   /* Switch to system mode */

   /* Set SPSR to the backed up cpsr of the new task */
   asm volatile("msr spsr_fsxc, %0" :: "r"(current_task->cpsr));
   /* restore lr */
   asm volatile("mov lr, %0" :: "r"(current_task->regs[PC]));

   SYSTEM_MODE;
   /* restore user stack and lr from new task state */
   asm volatile("mov sp, %0" :: "r"(current_task->regs[SP]));
   asm volatile("mov lr, %0" :: "r"(current_task->regs[LR]));
   IRQ_MODE;
   /* Restore state from task control block */
   asm volatile("mov r0, %0" :: "r"(current_task->regs));
   asm volatile("ldm r0, {r0-r12}");
   /* Branch into current process */
   asm volatile("movs pc,lr");
}

void NAKED schedule_start(void)
{
   /* Set a first task */
   IRQ_MODE; /* switch to IRQ mode so that schedule always work the same way */
   schedule();
}

/* addr are backups of registers: r0-r12,lr */
void save_current_task_state(const uint32_t *addr)
{
   task_control_t *t = current_task;
   if (!t) /* if no running task, abort */
      ABORT();
   /* first, cpsr */
   asm volatile("mrs %0, spsr" : "=r"(t->cpsr));
   /* first, r0 r12 */
   for (int i = R0 ; i <= R12 ; ++i)
      t->regs[i] = addr[i];
   /* Then PC */
   t->regs[PC] = addr[R12+1];
}

void save_current_task_sp_lr(const uint32_t sp, const uint32_t lr)
{
   task_control_t *t = current_task;
   if (!t) /* if no running task, abort */
      ABORT();
   t->regs[SP] = sp;
   t->regs[LR] = lr;
}

void stop_current_task(void)
{
   task_control_t *t = current_task;
   if (!t) /* if no running task, abort */
      ABORT();
   t->state = STOPPED;
}

/* must be called from schedule */
void remove_current_task(void)
{
   task_control_t *t = current_task;
   elect(); /* select a new task */
   if (current_task == t) /* no more task in the system */
   {
      arm_uart_puts("END No more task to run, stop kernel\n");
      asm volatile("udf");
   }
   task_control_t *node = current_task;
   while (node->next != t && node->next != current_task)
   {
      node = node->next;
   }
   if (node->next != t)
   {
      arm_uart_puts("PANIC: Fail to remove task\n");
      asm volatile("udf");
   }
   node->next = t->next;
   t->state = EMPTY;
}

