#ifndef __DEFINES_H__
#define __DEFINES_H__

#define UNUSED   __attribute__((unused))
#define NORETURN __attribute__((noreturn))
#define NAKED    __attribute__((naked))

#define ASM      asm volatile

#define GET_REG(var,reg) asm volatile ("mov %0, "#reg"" :"=r"((var)))
#define DELAY(count) do { for (int i = 0 ; i < (count) ; ++i) ASM("nop"); } while(0)


#endif
