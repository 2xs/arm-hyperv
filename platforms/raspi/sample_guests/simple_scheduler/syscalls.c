#include <stdarg.h>

#include "arm_uart_io.h"
#include "sys.h"
#include "scheduler.h"
#include "sys.h"
#include "cps.h"

/* User land functions */
void exit(void)
{
    asm volatile("mov r0, %0"::"i"(SYS_EXIT));
    asm volatile("svc #0");
    for (;;);
}

void puts(const char *s)
{
    asm volatile("mov r1, %0"::"r"(s));
    asm volatile("mov r0, %0"::"i"(SYS_PUTS));
    asm volatile("svc #0");
}

void puti(int i)
{
   asm volatile("mov r1, %0"::"r"(i));
   asm volatile("mov r0, %0"::"i"(SYS_PUTI));
   asm volatile("svc #0");
}

void putchar(char c)
{
   asm volatile("mov r1, %0"::"r"(c));
   asm volatile("mov r0, %0"::"i"(SYS_PUTC));
   asm volatile("svc #0");
}

void nop(void)
{
   asm volatile("mov r0, %0"::"i"(SYS_NOP));
   asm volatile("svc #0");
}

/* Kernel functions */
void __attribute__((interrupt("SWI"))) guest_svc_handler(int syscall_id, ...)
{
   switch (syscall_id)
   {
      case SYS_NOP:
         break;
      case SYS_EXIT:
      stop_current_task();
      IRQ_MODE;
      schedule();
      arm_uart_puts("task stopped\n");
      break;
      case SYS_PUTS:
      {
         va_list ap;
         va_start(ap, syscall_id);
         arm_uart_puts(va_arg(ap, const char*));
         va_end(ap);
      }
      break;
      case SYS_PUTI:
      {
         va_list ap;
         va_start(ap, syscall_id);
         arm_uart_puti(va_arg(ap, int));
         va_end(ap);
      }
      break;
      case SYS_PUTC:
      {
         va_list ap;
         va_start(ap, syscall_id);
         arm_uart_putc(va_arg(ap, int));
         va_end(ap);
      }
      break;
   }
}

