#ifndef __SYSCALLS_H__
#define __SYSCALLS_H__

/** exit task */
void exit(void);

/** put string */
void puts(const char *s);

/** put integer */
void puti(int i);

/** put char */
void putchar(char c);

/** nop syscall */
void nop(void);



#endif
