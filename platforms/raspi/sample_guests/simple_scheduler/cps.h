#ifndef __CPS_H__
#define __CPS_H__

#define USER_MODE       asm volatile("cps 16")
#define SYSTEM_MODE     asm volatile("cps 31")
#define SUPERVISOR_MODE asm volatile("cps 19")
#define IRQ_MODE        asm volatile("cps 18")

#define INTERRUPT_ENABLE  asm volatile("cpsie i");
#define INTERRUPT_DISABLE asm volatile("cpsid i");

#endif
