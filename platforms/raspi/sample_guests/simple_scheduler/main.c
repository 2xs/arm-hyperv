#include <stdint.h>
#include "arm_uart_io.h"
#include "rpi-armtimer.h"
#include "rpi-interrupt.h"
#include "scheduler.h"
#include "defines.h"
#include "cps.h"
#include "user_tasks/tasks.h"


/* Timer code strongly inspired by
 * http://www.valvers.com/open-software/raspberry-pi/step04-bare-metal-programming-in-c-pt4/ */


void NAKED guest_irq_handler(void) /* Naked to handle context by hand */
{
   register uint32_t *sp asm("sp");
   register uint32_t lr asm("lr");
   /* Save context to IRQ stack */
   ASM("sub lr, lr, #4"); /* set LR to the instruction to execute in the task */
   ASM("push {r0-r12, lr}");
   save_current_task_state(sp);
   ASM("pop {r0-r12, lr}"); /* restore IRQ stack */
   SYSTEM_MODE;
   save_current_task_sp_lr((uint32_t)sp, lr); /* save user stack pointer and LR */
   IRQ_MODE; /* switch back to IRQ mode */
   /* Clear timer IRQ */
   RPI_GetArmTimer()->IRQClear = 1;

   /* Context of current task is saved, schedule next task */
   schedule(); /* this does never returns */
}

void copy_exception_vector(void)
{
   extern uint8_t begin_interrupt_vector, end_interrupt_vector;
   /* Set interrupt vector */
   unsigned char *dst = (unsigned char*)0;
   uint32_t size = &end_interrupt_vector - &begin_interrupt_vector;
   uint32_t i;
   for (i = 0 ; i < size ; ++i)
      dst[i] = (&begin_interrupt_vector)[i];
}



void __attribute__((interrupt("UDF"))) guest_undef_handler(void){
   arm_uart_puts("UDF caught while executing code");
   for(;;);
}


void zero_bss(void)
{
   extern unsigned char _bss_start;
   extern unsigned char _bss_end;
   register unsigned char *ptr = &_bss_start;
   while (ptr < &_bss_end)
      *ptr++ = 0;
}


void main(void)
{
   zero_bss();
   arm_uart_puts("Starting simple scheduling sample\n");
   copy_exception_vector();

   /* run_micro_benchmarks(); */

   setup_user_tasks();
   RPI_RunTimer(10); /* 10 Hz timer (100 ms)*/
   schedule_start();
   for(;;)
   {
   }
}
