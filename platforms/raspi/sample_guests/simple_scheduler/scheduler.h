#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include <stdint.h>
#include "defines.h"

/** Schedules the next ready task using round robin */
void NAKED schedule(void);
/** Starts the scheduler */
void NAKED schedule_start(void);
/** Setups a new task */
int setup_task(void (*task)(int), int param);

/** Stops the current task */
void stop_current_task(void);

/** Saves the current task state which is stored at addr 
    registers at addr are, in order:
    r0-r12,lr
*/
void save_current_task_state(const uint32_t *addr);
void save_current_task_sp_lr(const uint32_t sp, const uint32_t lr);

#endif
