.section .start

.arm
.global main
.type main, %function

.global _start
_start:
        /* Set system stack */
        ldr sp, =stack_top
        /* Set user stack */
        /* switch to system mode (sp is same as user mode) */
        cps #0b11111
        ldr sp, =user_stack
        /* swith back to supervisor mode */
        cps #0b10011
        bl main
g_udf:  udf                     ;@ Undefined behavior instructions that the hypervisor recognize as guest ended instruction
lague:  b  lague


/* Used to copy the execption vector at 0x0 */
.global begin_interrupt_vector
.global end_interrupt_vector
.extern guest_swi_handler
.type guest_swi_handler, %function
        
begin_interrupt_vector: 
    b .
    b .
    ldr pc, swi_handler_addr
    b .
    b .
    b . 
    b .
    b .

swi_handler_addr:
    .word guest_swi_handler

end_interrupt_vector:

        
STACK_SIZE=4096
        .section .stack
        .space STACK_SIZE
stack_top:      
        .space STACK_SIZE
user_stack:
