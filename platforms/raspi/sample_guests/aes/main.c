#include <stdint.h>
#include "arm_uart_io.h"
#include "perf_timer.h"
#include "aes.h"
#include "printf.h"

#define DECLARE_BENCH static uint32_t __start, __end, __runs,__count
#define START_BENCH(c)   __count = __runs = (c); __start = GET_PERF_TIMER_CLO; for ( ; __count ; --__count ) {
#define END_BENCH(x)  } do { __end = GET_PERF_TIMER_CLO; printf("BENCH %s_%s,%d,%d\n", (x), current_mode == SUPERVISOR_MONO_TASK ? "single_task" : "multi_task", (__end-__start), __runs); } while(0)

enum bench_mode {
   SUPERVISOR_MONO_TASK,
   USER_MULTI_TASK,
};
static int current_mode = 0;


void putchar(char c)
{
   arm_uart_putc(c);
}

void main(void)
{
   putchar_func = arm_uart_putc;
   arm_uart_puts("Starting AES Benchmark\r\n");
   DECLARE_BENCH;
   START_BENCH(4);
   mbedtls_aes_self_test(0);
   arm_uart_puts("This is AES Benchmark (iteration)\r\n");
   END_BENCH("AES");

}
