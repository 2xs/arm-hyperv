#include "arm_uart_io.h"
#include "printf.h"

void putchar(char c)
{
   arm_uart_putc(c);
}

static volatile int svc_count = 0;

void __attribute__((interrupt("SWI"))) guest_svc_handler(int syscall_id, ...)
{
   printf("SVC %d\n", syscall_id);
   svc_count++;
}


void main(void)
{
   int i;
   /* GCC does not generate lr backups as it does not know that it will be changed by svc */
   asm volatile("push {lr}");
   for (i = 0 ; i < 10 ; ++i)
   {
      asm volatile("mov r0, %0" :: "r"(i));
      asm volatile("svc #0");
   }

   if (svc_count != i)
   {
      printf("Test failed\n");
      for (;;);
   }
   printf("Test passed\n");
   /* Restore lr */
   asm volatile("pop {lr}");
}
