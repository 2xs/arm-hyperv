.section .start

.arm
.global main
.type main, %function

.global _start
_start:
        /* Set system stack */
        ldr sp, =stack_top
        /* Set IRQ stack */
        cps #18
        ldr sp, =irq_stack_top
        /* Back to supervisor */
        cps #19

        /* Set exception vector */
        ldr r0, =begin_interrupt_vector
        mov r1, #0
        /* Exception vector and one branch address */
        ldmia r0!, {r2-r10}
        stmia r1!, {r2-r10}
        
        bl main
g_udf:  udf                     ;@ Undefined behavior instructions that the hypervisor recognize as guest ended instruction
lague:  b  lague


/* Used to copy the execption vector at 0x0 */
.global begin_interrupt_vector
.global end_interrupt_vector

.extern guest_irq_handler
.type guest_irq_handler, %function
        
begin_interrupt_vector: 
    b .
    b .
    b .
    b .
    b .
    b . 
    ldr pc, irq_handler_addr
    b .

irq_handler_addr:
    .word guest_irq_handler
    
end_interrupt_vector:

        
STACK_SIZE=4096
        .section .stack
        .space STACK_SIZE
stack_top:
        .space STACK_SIZE
irq_stack_top:
