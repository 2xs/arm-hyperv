#include "arm_uart_io.h"
#include "printf.h"
#include "rpi-armtimer.h"

void putchar(char c)
{
   arm_uart_putc(c);
}

static volatile int irq_count = 0;

void __attribute__((interrupt("IRQ"))) guest_irq_handler(void)
{
   printf("Guest IRQ %d\n", irq_count++);
   RPI_ClearTimerIRQ();
}


void main(void)
{

   RPI_RunTimer(10); /* Run a 10Hz timer */

   asm volatile("cpsie i"); /* enable interrupts */
   
   while (irq_count <= 10)
   {
   }
   printf("Test passed\n");
   asm volatile("udf");
}
