#include "arm_uart_io.h"
#include "aes.h"
#include <stdint.h>

typedef volatile struct
{
   uint32_t cs;
   uint32_t clo;
   uint32_t chi;
   uint32_t c0;
   uint32_t c1;
   uint32_t c2;
   uint32_t c3;
} timer_t;

typedef void (*hypervise_api_t)(void);

/* Get the pointers to hypervisor API calls */
hypervise_api_t hypervise_restart = (hypervise_api_t) 0x17000;
hypervise_api_t hypervise_stop = (hypervise_api_t) 0x17100;

/* bench function */
typedef void (*bench_t)(void);

extern void dhrystone ( void );



int fibo(int n){
  if(n==0 || n==1)
    return n;
  else
    return fibo(n-1) + fibo(n-2);
}

void bench_fibo(void){
  fibo(20);  
}

void bench_aes(void)
{
   mbedtls_aes_self_test(1);
}


void bench_uart_print(void)
{
   arm_uart_puts("Hello World counting!\n");
   arm_uart_puts("Hello World counting!\n");
   arm_uart_puts("Hello World counting!\n");
}


uint32_t bench(bench_t to_bench, int hypervision_on, int count)
{
   volatile timer_t * rasp_timer = (volatile timer_t*)0x20003000;
   /* TODO: if these variables become non static -> hypervisor lose
    * control (hypervise_restart does not restart */
   static uint32_t start, stop;
   int i = 0;
   if (!hypervision_on)
      hypervise_stop();
   start = rasp_timer->clo;
   for (i = 0 ; i < count ; ++i)
      to_bench();
   stop = rasp_timer->clo;
   if (!hypervision_on)
      hypervise_restart();
   return stop-start;
}

void bench_no_control(const char *bench_name, bench_t to_bench, int count)
{
   uint32_t hypervised_time = bench(to_bench, 1, count);
   arm_uart_puts("BENCH ");
   arm_uart_puts(bench_name);
   arm_uart_putc(',');
   arm_uart_puti(hypervised_time);
   arm_uart_putc(',');
   arm_uart_puti(count);
   arm_uart_putc('\n');
   
}
   
#define DO_BENCH(f,c) bench_no_control(#f, f, (c))

void putchar(char c)
{
   arm_uart_putc(c);
}


void main(void)
{
   DO_BENCH(bench_uart_print,10);
   DO_BENCH(dhrystone,10);
   DO_BENCH(bench_fibo,10);
   DO_BENCH(bench_aes, 5); 
   arm_uart_puts("END\n");
}
