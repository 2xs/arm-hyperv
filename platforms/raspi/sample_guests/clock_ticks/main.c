#include <stdint.h>
#include "arm_uart_io.h"
#include "rpi-armtimer.h"
#include "rpi-interrupt.h"

#define GET_REG(var,reg) asm volatile ("mov %0, "#reg"" :"=r"((var)))
#define ASM asm volatile

/* Code strongly inspired by http://www.valvers.com/open-software/raspberry-pi/step04-bare-metal-programming-in-c-pt4/ */


void print_cpsr(void)
{
   /* reads CPSR */
   uint32_t cpsr;
   ASM("mrs r0, cpsr");
   GET_REG(cpsr, r0);
   arm_uart_puts("CPSR: ");
   arm_uart_puti(cpsr);
   arm_uart_putc('\n');
}

void print_sp(void)
{
   uint32_t sp;
   GET_REG(sp, sp);
   arm_uart_puts("SP: ");
   arm_uart_puti(sp);
   arm_uart_putc('\n');
}

#define PRINT_LR do {                           \
      uint32_t lr;                              \
      GET_REG(lr, lr);                          \
      arm_uart_puts("LR: ");                    \
      arm_uart_puti(lr);                        \
      arm_uart_putc('\n');                      \
   } while(0)                                   \


typedef enum
{
   PUTS,
   PUTI,
   PUTC,
   STOP,
} syscall_type_t;

static volatile union {
   const char *str;
   char c;
   int val;
} syscall_param;

void enable_interrupts(void)
{
   ASM("cpsie i");
}


static volatile int clock_count = 0;
void __attribute__((interrupt("IRQ"))) guest_irq_handler(void)
{
   arm_uart_puts("youpi clock: ");
   arm_uart_puti(clock_count);
   arm_uart_puts("\n");
   
   RPI_GetArmTimer()->IRQClear = 1;
   if (++clock_count > 10)
   {
      arm_uart_puts("10 clock ticks reached, stopping guest\n");
      ASM("udf");
   }
}

void __attribute__((interrupt("SWI"))) guest_swi_handler(void)
{
   uint32_t lr;
   GET_REG(lr, lr);
   lr -= 4; /* lr now holds the address of the swi instruction */
   uint32_t opcode = *((uint32_t*)lr);
   switch (opcode & 0x00ffffff) 
   {
      case PUTS:
         arm_uart_puts(syscall_param.str);
         break;
      case PUTI:
         arm_uart_puti(syscall_param.val);
         break;
      case PUTC:
         arm_uart_putc(syscall_param.c);
         break;
      case STOP:
         ASM("udf");
         break;
   }
}

extern uint8_t begin_interrupt_vector, end_interrupt_vector;


void puts(const char *str)
{
   syscall_param.str = str;
   ASM("swi 0");
}

void puti(int val)
{
   syscall_param.val = val;
   ASM("swi 1");
}
void putc(char c)
{
   syscall_param.c = c;
   ASM("swi 2");
}

void stop(void)
{
   ASM("swi 3");
}



void user_task(void)
{
   puts("Using puts syscall to print!\n");
   puts("Let's pretend we are computing something very important\n");
   for(;;){
      arm_uart_puti(RPI_GetArmTimer()->Value);
      arm_uart_putc('\n');
   }
}

void start_task(void (*task_ptr)(void))
{
   uint32_t cpsr;
   arm_uart_puts("Starting userland task\n");
   cpsr = 16; /* User mode with exception enabled */
   ASM("msr cpsr_fsxc, %0"::"r"(cpsr));
   task_ptr();
}
void main(void)
{
   arm_uart_puts("Begin test\n");
   /* Set interrupt vector */
   unsigned char *dst = (unsigned char*)0;
   uint32_t size = &end_interrupt_vector - &begin_interrupt_vector;
   uint32_t i;
   for (i = 0 ; i < size ; ++i)
      dst[i] = (&begin_interrupt_vector)[i];

   
   // Enable Timer IRQ
   RPI_GetIrqController()->Enable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;

   
   /* Setup the system timer interrupt */
   /* Timer frequency = Clk/256 * 0x400 */
   rpi_arm_timer_t *timer = RPI_GetArmTimer();
   timer->Load = 0x400;

   /* Setup the ARM Timer */
   timer->Control =
      RPI_ARMTIMER_CTRL_23BIT |
      RPI_ARMTIMER_CTRL_ENABLE |
      RPI_ARMTIMER_CTRL_INT_ENABLE |
      RPI_ARMTIMER_CTRL_PRESCALE_256;

   /* Enable interrupts! */
   /* switch to user mode */
   start_task(user_task);
   for(;;);
}
