#ifndef __UART_IO_H__
#define __UART_IO_H__

void arm_uart_putc(char c);
void arm_uart_puts(const char *s);
void arm_uart_puti(const unsigned int i);

void init_ok_led();
void set_ok_led_off();
void set_ok_led_on();

#endif
