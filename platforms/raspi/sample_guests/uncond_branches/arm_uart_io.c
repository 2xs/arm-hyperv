#include <stdint.h>

//#define UART0_BASE_ADDR 0x20201000

enum
{
    // The GPIO registers base address.
    GPIO_BASE = 0x20200000,
 
    // The offsets for reach register.
 
    // Controls actuation of pull up/down to ALL GPIO pins.
    GPPUD = (GPIO_BASE + 0x94),
 
    // Controls actuation of pull up/down for specific GPIO pin.
    GPPUDCLK0 = (GPIO_BASE + 0x98),
 
    // The base address for UART.
    UART0_BASE = 0x20201000,
 
    // The offsets for reach register for the UART.
    UART0_DR     = (UART0_BASE + 0x00),
    UART0_RSRECR = (UART0_BASE + 0x04),
    UART0_FR     = (UART0_BASE + 0x18),
    UART0_ILPR   = (UART0_BASE + 0x20),
    UART0_IBRD   = (UART0_BASE + 0x24),
    UART0_FBRD   = (UART0_BASE + 0x28),
    UART0_LCRH   = (UART0_BASE + 0x2C),
    UART0_CR     = (UART0_BASE + 0x30),
    UART0_IFLS   = (UART0_BASE + 0x34),
    UART0_IMSC   = (UART0_BASE + 0x38),
    UART0_RIS    = (UART0_BASE + 0x3C),
    UART0_MIS    = (UART0_BASE + 0x40),
    UART0_ICR    = (UART0_BASE + 0x44),
    UART0_DMACR  = (UART0_BASE + 0x48),
    UART0_ITCR   = (UART0_BASE + 0x80),
    UART0_ITIP   = (UART0_BASE + 0x84),
    UART0_ITOP   = (UART0_BASE + 0x88),
    UART0_TDR    = (UART0_BASE + 0x8C),
};

/*void arm_uart_putc(char c) {
   *(char *) UART0_BASE_ADDR = c;
}*/
 
static void mmio_write(uint32_t reg, uint32_t data)
{
  *(volatile uint32_t *)reg = data;
}
 
static uint32_t mmio_read(uint32_t reg)
{
  return *(volatile uint32_t *)reg;
}

volatile unsigned int* gpio = (unsigned int*)0x20200000UL;

void init_ok_led(){	
  /* Write 1 to the GPIO16 init nibble in the Function Select 1 GPIO
     peripheral register to enable GPIO16 as an output */
  //	*(unsigned int*)GPFSEL |= (1 << 18);
  gpio[1] |= (1 << 18);
}

void set_ok_led_off(){
  /* Set the LED GPIO pin high ( Turn OK LED off for original Pi )*/
  //*(unsigned int*)GPSET = (1 << 16);
  gpio[7] = (1 << 16);
}

void set_ok_led_on(){
  /* Set the LED GPIO pin low ( Turn OK LED on for original Pi )*/
  //      *(unsigned int*)GPCLR = (1 << 16);
  gpio[10] = (1 << 16);
}

void arm_uart_putc(char byte)
{
  while ( mmio_read(UART0_FR) & (1 << 5) ) { }
  mmio_write(UART0_DR, byte);
  while ( mmio_read(0x20201018) & (1 << 5) ) { }
}

void arm_uart_puts(const char *s)
{
   while (*s)
      arm_uart_putc(*s++);
}

void arm_uart_puti(const unsigned int i){

  if(i/10 > 0)
     arm_uart_puti(i/10);
  arm_uart_putc('0' + (i%10));         
}
