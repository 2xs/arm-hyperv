#include "arm_uart_io.h"

void main(void)
{
   arm_uart_puts("Before UDF\n");
   asm volatile("udf");
   arm_uart_puts("UDF done\n");
   for(;;);
}
