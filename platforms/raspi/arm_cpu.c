/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "arm_cpu.h"
#include "memcpy.h"
#include "debug.h"
#include "arm_trampolines.h"
#include "arm_instr_info.h"
#include "output.h"
#include "memset.h"
#include "arm_mmu.h"
#include "arm_cp15.h"

#include "printf.h"
#include "cache.h"

#define ARM_INSTR_SIZE 4

HypervisorReturnFunction return_address_by_mode[MAX_MODE_COUNT];

extern void arm_stop(void);
void arm_platform_stop(void)
{
   cachePrintStats();
   arm_stop();
   for (;;);
}

uint32_t arm_platform_read_cpsr() {
  uint32_t cpsr;

  asm volatile ("mrs %0, cpsr\n" : "=r" (cpsr));
  return cpsr;
}

ARM_Mode arm_platform_cpu_mode() {
    return (ARM_Mode) (arm_platform_read_cpsr() & 0x1F);
}

void arm_platform_handle_return(ARM_SavedState * state) {
   log(LOG_ANALYSER, "RETURN into HYP in MODE: %d with state %p\n", state->context.cpsr.fields.mode, state);
   return_address_by_mode[state->context.cpsr.fields.mode](state);
}

int arm_platform_get_destination_mode(void *state) {
   int mode = ((ARM_SavedState *)state)->destination_mode_switch;
   ((ARM_SavedState *)state)->destination_mode_switch = 0;
   return mode;
}

int arm_platform_patch_size_arm(addr_t at, addr_t dest) {
    int32_t delta = (int32_t) (dest - at);
    if (delta >= -33554432 && delta <= 33554428)
       return 4;
    return 8;
}

int arm_platform_bytesNeededToPatchAddress(void * saved_state, addr_t patch_address, addr_t destination) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;
    assert(!arm_state->context.cpsr.fields.thumb);
    return arm_platform_patch_size_arm(patch_address, destination);
}

void arm_platform_patch_arm_4(addr_t at, addr_t dest) {
    uint32_t *ptr = (uint32_t *) at;
    int32_t delta = (int32_t) (dest - at);
    delta -= 8; /* The delta is computed using pc + 8 because of prefetching */
    int32_t imm = (delta >> 2) & 0xFFFFFF; /* Compute the immediate on 24 bits */
    *ptr = 0xEA000000 | imm; // B dest
}

void arm_platform_patch_arm_8(addr_t at, addr_t dest) {
    uint32_t *ptr = (uint32_t *) at;
    *ptr++ = 0xE51FF004; // LDR PC, [PC, #-4]
    *ptr = dest;
}


extern int is_in_guest(addr_t addr);

void arm_platform_patchAddress(void * saved_state, addr_t patch_address, addr_t destination) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;

    /* Patch must only be done on guest code */
    assert(is_in_guest(patch_address));
    assert(!arm_state->context.cpsr.fields.thumb);

    // Save the original instruction(s)
    arm_state->patch_addr = patch_address;
    arm_state->patch_size = arm_platform_bytesNeededToPatchAddress(saved_state, patch_address, (addr_t) arm_platform_comeback_arm);
    memcpy(arm_state->saved_instrs, (void *) patch_address, arm_state->patch_size);

    // Save the return address in the supervisor
    int mode = arm_platform_modeIndex(saved_state);
    log(LOG_PATCH, "patch:%p:mode:%d:state:%p:destination:%p\n", patch_address, mode, saved_state, destination);
    return_address_by_mode[mode] = (HypervisorReturnFunction) destination;

    // Patch to jump into the ARM trampoline
    if (arm_state->patch_size == 4)
       arm_platform_patch_arm_4(patch_address, (addr_t) arm_platform_comeback_arm);
    else
       arm_platform_patch_arm_8(patch_address, (addr_t) arm_platform_comeback_arm);
}

void arm_platform_unpatchAddress(void * saved_state) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;
    addr_t patch_address = arm_state->patch_addr;
    memcpy((void *) patch_address, arm_state->saved_instrs, arm_state->patch_size);
    arm_state->patch_size = 0;
}

int arm_platform_modeIndex(void * saved_state) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;

    return arm_state->context.cpsr.fields.mode;
}

addr_t arm_platform_getPCValue(void * saved_state) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;
    return (addr_t) arm_state->context.gpr.name.pc;
}

void arm_platform_setReturnValue(void * saved_state, uint32_t value) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;
    arm_state->context.gpr.name.r0 = value;
}


void arm_platform_setCPSR(void * state, uint32_t value)
{
  ARM_SavedState *arm_state = (ARM_SavedState *) state;
  arm_state->context.cpsr.value = value;
}

uint32_t arm_platform_getCPSR(void * state)
{
  ARM_SavedState *arm_state = (ARM_SavedState *) state;
  return arm_state->context.cpsr.value;
}

uint32_t arm_platform_getReturnValue(void * saved_state) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;
    return arm_state->context.gpr.name.r0;
}

void arm_platform_setPCValue(void * saved_state, addr_t value, int link) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;

    assert(!arm_state->context.cpsr.fields.thumb);
    assert(!(value & 3)); /* PC values must be multiples of 32 bits */
    if (link) {
        addr_t old_pc = arm_state->context.gpr.name.pc;
        int len = arm_platform_instruction_length(saved_state, *(uint32_t *) old_pc);
        arm_state->context.gpr.name.lr = (old_pc + len);
    }
    arm_state->context.gpr.name.pc = (uint32_t) value;
}

static ARM_SavedState arm_state2;

/*warn: the copy is made in a global variable so only one copy can be used at the same time*/
void arm_platform_copyState(void * state, void ** new_state){
   memcpy(&arm_state2, state, sizeof(arm_state2));
   *new_state = &arm_state2;
}

int arm_platform_is_condition_met(void* state) {
  Hypervisor_InstrInfo info;
  return arm_platform_condition_is_met(
				       (ARM_SavedState*)state,
				       arm_platform_condition(&info)
				       );
}

void arm_platform_setLastITAddr(void * saved_state, addr_t value) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;
    arm_state->last_it_instr_addr = value;
}

int arm_platform_isInITBlock(void * saved_state, addr_t instr_addr) {
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;
    addr_t last_it = arm_state->last_it_instr_addr;
    if (last_it < instr_addr && instr_addr <= last_it + 4 * 4) {
        // Read IT instruction
        uint16_t w = * (uint16_t *) last_it;
        uint8_t mask = w & 0xF;

        // Count the number of instruction in the IT block
        static int cnts[] = {
            1 /* 0000 */, 4 /* 0001 */, 3 /* 0010 */, 4 /* 0011 */,
            2 /* 0100 */, 4 /* 0101 */, 3 /* 0110 */, 4 /* 0111 */,
            1 /* 1000 */, 4 /* 1001 */, 3 /* 1010 */, 4 /* 1011 */,
            2 /* 1100 */, 4 /* 1101 */, 3 /* 1110 */, 4 /* 1111 */
        };
        int cnt = cnts[mask];
        // if (mask & 1) cnt = 4;
        // else if (mask & 2) cnt = 3;
        // else if (mask & 4) cnt = 2;
        // else cnt = 1;

        // Disassemble each instruction
        int i;
        addr_t i_addr = last_it + 2;
        for (i=0; i<cnt; i++) {
            if (i_addr == instr_addr) return 1;
            uint16_t *p = (uint16_t *) i_addr;
            uint8_t high = *p >> 11;
            i_addr += (high >= 0x1D) ? 4 : 2;
        }
    }
    return 0;
}
void arm_platform_init(void)
{
   ARM_SavedState *arm_state = arm_platform_saved_state_for_mode(ARMMode_Supervisor);
   log(LOG_HYPERVISOR, "Building initial state\n");

   /* Build the initial state by ensuring that all registers are cleared */
   bzero(arm_state, sizeof(ARM_SavedState));

   /* Install the exception vector */
   /*
      TODO: Shouldn't we ensure that interrupts are disabled at this time ?
   */
   extern unsigned char begin_interrupt_vector;
   extern unsigned char end_interrupt_vector;
   unsigned int vector_size = &end_interrupt_vector - &begin_interrupt_vector;
   log(LOG_HYPERVISOR, "Copying %d bytes exception vector from %p to %p\n", vector_size, &begin_interrupt_vector, 0);
   memcpy((void*)0x0, &begin_interrupt_vector, vector_size);

   /* Setup MMU */
   log(LOG_HYPERVISOR, "Setting up MMU...");
   extern uint32_t tt0[];
   unsigned int i;
   /* Maps all remaining memory 1:1 */
   for (i=0; i<TRANSLATION_TABLE_SIZE; ++i)
   {
      arm_mmu_map_section(tt0, i * MB, i * MB, MMU_FLAG_READWRITE|MMU_FLAG_EXEC);
   }
   /* set up the translation table base */
   arm_write_ttbr0((uint32_t)tt0);

   /* set up the domain access register */
   arm_write_dacr(0x00000003);
   /* turn on the mmu */
   arm_write_cr1(arm_read_cr1() | 0x1);
   log(LOG_HYPERVISOR, "done!\n");
}

#include "printf.h"
void arm_platform_debugDumpState(void * saved_state) {
    static const char *modes[] = {
        "<unknown mode 0>", "<unknown mode 1>", "<unknown mode 2>", "<unknown mode 3>",
        "<unknown mode 4>", "<unknown mode 5>", "<unknown mode 6>", "<unknown mode 7>",
        "<unknown mode 8>", "<unknown mode 9>", "<unknown mode 10>", "<unknown mode 11>",
        "<unknown mode 12>", "<unknown mode 13>", "<unknown mode 14>", "<unknown mode 15>",
        "User", "FIQ", "IRQ", "Supervisor",
        "<unknown mode 20>", "<unknown mode 21>", "Monitor", "Abort",
        "<unknown mode 24>", "<unknown mode 25>", "Hyp", "Undefined",
        "<unknown mode 28>", "<unknown mode 29>", "<unknown mode 30>", "System"
    };
    ARM_SavedState *arm_state = (ARM_SavedState *) saved_state;
    printf("State at %p\n", arm_state);
    printf("  R0  = %08X    R8  = %08X\n", (unsigned int) arm_state->context.gpr.name.r0, (unsigned int) arm_state->context.gpr.name.r8);
    printf("  R1  = %08X    R9  = %08X\n", (unsigned int) arm_state->context.gpr.name.r1, (unsigned int) arm_state->context.gpr.name.r9);
    printf("  R2  = %08X    R10 = %08X\n", (unsigned int) arm_state->context.gpr.name.r2, (unsigned int) arm_state->context.gpr.name.r10);
    printf("  R3  = %08X    R11 = %08X\n", (unsigned int) arm_state->context.gpr.name.r3, (unsigned int) arm_state->context.gpr.name.r11);
    printf("  R4  = %08X    R12 = %08X\n", (unsigned int) arm_state->context.gpr.name.r4, (unsigned int) arm_state->context.gpr.name.r12);
    printf("  R5  = %08X    SP  = %08X\n", (unsigned int) arm_state->context.gpr.name.r5, (unsigned int) arm_state->context.gpr.name.sp);
    printf("  R6  = %08X    LR  = %08X\n", (unsigned int) arm_state->context.gpr.name.r6, (unsigned int) arm_state->context.gpr.name.lr);
    printf("  R7  = %08X    PC  = %08X\n", (unsigned int) arm_state->context.gpr.name.r7, (unsigned int) arm_state->context.gpr.name.pc);
    printf("  CPSR = %08X\n", (unsigned int) arm_state->context.cpsr.value);
    printf("     > Mode %s (%s)\n", modes[arm_state->context.cpsr.fields.mode], arm_state->context.cpsr.fields.thumb ? "Thumb" : "ARM");
    printf("     > FIQ %s, IRQ %s, ASYNC %s\n",
                arm_state->context.cpsr.fields.fiq_mask ? "disabled" : "enabled",
                arm_state->context.cpsr.fields.irq_mask ? "disabled" : "enabled",
                arm_state->context.cpsr.fields.async_mask ? "disabled" : "enabled");
    printf("     > Cond:  %c%c%c%c%c\n",
                arm_state->context.cpsr.fields.n ? 'N' : '-',
                arm_state->context.cpsr.fields.z ? 'Z' : '-',
                arm_state->context.cpsr.fields.c ? 'C' : '-',
                arm_state->context.cpsr.fields.v ? 'V' : '-',
                arm_state->context.cpsr.fields.q ? 'Q' : '-');
    printf("     > Patch: %p\n", (void *) arm_state->patch_addr);
}


void arm_irq_enable() {
	asm volatile("cpsie i");
}

void arm_irq_disable() {
	asm volatile("cpsid i");

}

static ARM_SavedState hypervisor_state;
ARM_SavedState *arm_platform_get_hypervisor_state(void)
{
   return &hypervisor_state;
}

addr_t arm_platform_get_next_instr(const addr_t start)
{
    return start + ARM_INSTR_SIZE;
}
