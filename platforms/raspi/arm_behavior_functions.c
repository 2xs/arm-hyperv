/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include <stdarg.h>

#include "arm_behavior_functions.h"
#include "arm_instr_info.h"
/* #include "debug.h" */
#include "arm_copro_debug.h"
#include "arm_execute.h"
#include "../../utils/debug.h"
#include "arm_trampolines.h"

#include "runtime.h"
#include "analyse.h"
#include "hypervisor_debug.h"
#include "arm_cpu.h"
#include "hypervise.h"
#include "hypervisor.h"
#include "bits.h"
#include "output.h"

Hypervisor_Behavior_Function arm_behavior_runtime_functions[NB_BEHAVIOR];

#define SWITCH_HALFWORD(n) ((((n)&0xffff)<<16) | (((n)&0xffff0000)>>16))


/* Performs the necessary patch for executing guest until a given instruction and set
   the trap to come back to hypervisor_is_cond_met which will compute the destination of an
   odd branch and restart the analysis from this destination 
*/
static void NORETURN odd_branch_patch_and_execute(void *state, Hypervisor_InstrInfo *info)
{
   arm_platform_patchAddress(state,(uint32_t)(info->address),(addr_t)hypervisor_is_cond_met);
   arm_platform_execute(state);
}

static int branch_reset_mask_it_0xffff0fff( void* state, Hypervisor_InstrInfo* info  UNUSED , int extended){
  ARM_SavedState* arm_state = (ARM_SavedState*) state;

  if(extended)
     *(uint32_t*)(arm_state->context.gpr.name.pc-4) |= (0xf000 | (1<<20));
  else
     *(uint32_t*)arm_state->context.gpr.name.pc |= (0xf000 | (1<<20));
  return 1;
}

/* info should be null
 * extended = 0 means reset the instruction at PC 
 * extended = 1 means reset the instruction before the instruction at PC*/
static int branch_reset_mask_0xffff0fff( void* state, Hypervisor_InstrInfo* info  UNUSED , int extended){
  ARM_SavedState* arm_state = (ARM_SavedState*) state;

  if(extended)
     *(uint32_t*)(arm_state->context.gpr.name.pc-4) |= 0xf000;
  else
     *(uint32_t*)arm_state->context.gpr.name.pc |= 0xf000;
  return 1;
}


int branch_set_mask_switch_0xffff0fff( void* state UNUSED, Hypervisor_InstrInfo* info, int extended UNUSED){
  *(uint32_t*)info->address = SWITCH_HALFWORD(info->instr & 0xffff0fff);
  arm_behavior_runtime_functions[Behavior_Number_OddBranch] = branch_reset_mask_0xffff0fff;
  odd_branch_patch_and_execute(state, info);
  return 0;
}

int branch_set_mask_it_0xffff0fff( void* state UNUSED , Hypervisor_InstrInfo* info, int extended  UNUSED) {
  *(uint32_t*)info->address = info->instr & 0xffef0fff;

  arm_behavior_runtime_functions[Behavior_Number_OddBranch] = branch_reset_mask_it_0xffff0fff;
  odd_branch_patch_and_execute(state, info);
  return 0;
}
int branch_set_mask_0xffff0fff( void* state UNUSED , Hypervisor_InstrInfo* info, int extended  UNUSED) {
  *(uint32_t*)info->address = info->instr & 0xffff0fff;

  arm_behavior_runtime_functions[Behavior_Number_OddBranch] = branch_reset_mask_0xffff0fff;
  odd_branch_patch_and_execute(state, info);
  return 0;
}




/* info should be null
 * extended = 0 means reset the instruction at PC 
 * extended = 1 means reset the instruction before the instruction at PC*/
static int branch_reset_mask_0xff78( void* state, Hypervisor_InstrInfo* info  UNUSED , int extended){
  ARM_SavedState* arm_state = (ARM_SavedState*) state;
  printf("%p %s\n", arm_platform_getPCValue(state), __FUNCTION__);;
    
  if(extended)
    *(uint16_t*)(arm_state->context.gpr.name.pc-2) |= 0x87;
  else
    *(uint16_t*)arm_state->context.gpr.name.pc |= 0x87;
  
  return 1;
}

int branch_set_mask_0xff78( void* state  UNUSED , Hypervisor_InstrInfo* info, int extended  UNUSED ){
  
  *(uint16_t*)info->address = info->instr & 0xff78;

  arm_behavior_runtime_functions[Behavior_Number_OddBranch] = branch_reset_mask_0xff78;
  odd_branch_patch_and_execute(state, info);
	
  return 0;
}


int branch_at_pc_destination( void* state, Hypervisor_InstrInfo* info, int extended  UNUSED ){

   arm_platform_setPCValue(state,
                           info->pc_destination,
                           info->behavior & Behavior_Link);

  return 0;
}

int odd_branch_emulation( void* state, Hypervisor_InstrInfo* info, int extended UNUSED){
    ARM_SavedState * arm_state = (ARM_SavedState *) state;
  
    /* LDM/LDMIA/LDMFD (ARM) - LDMIB/LDMED - POP (ARM)*/
    if((info->instr & 0b1111110100001000000000000000) == 0b1000100100001000000000000000){
       uint32_t tmp=0;
       uint8_t rn=0;
       addr_t addr=0;
       uint8_t i=0;
       uint8_t count_register=0;
       uint8_t isBefore=0;

       /* Do we have to increment the addr before ?*/
       isBefore = (info->instr & 0x1000000) >> 24;
      
       rn = (info->instr & 0xf0000) >> 16; /* get the base register */
       addr = arm_state->context.gpr.reg[rn] + 4*isBefore;

       for(i=0;i<16;i++) {
          if( info->instr & (1<<i) ) { /* if the register is in the destination */
             /* update the counter */
             count_register++;

             tmp = *(uint32_t*)addr;
	  
             arm_state->context.gpr.reg[i] = tmp; /* Update the VM */
             addr += 4;
          }
       }

       /* if writing back is activated */
       if( info->instr & 0x200000 ) {
          /* Update the base register */
          arm_state->context.gpr.reg[rn] += count_register*4;
       }
    }   
    /* LDMDA/LDMFA - LDMDB/LDMEA */
    else if((info->instr & 0b1111110100001000000000000000) == 0b1001000100001000000000000000){
       uint32_t tmp=0;
       uint8_t rn=0;
       addr_t addr=0;
       uint8_t i=0;
       uint8_t count_register=0;
       uint8_t isBefore=0;

       /* Do we have to increment the addr before ?*/
       isBefore = (info->instr & 0x1000000) >> 24;

       /* count the number of register in the destination */
       for(i=0;i<15;i++) 
          if( info->instr & (1<<i) )  /* if the register is in the destination */
             count_register++;

       rn = (info->instr & 0x40000) >> 16; /* get the base register */
       addr = arm_state->context.gpr.reg[rn] - count_register*4  + 4*isBefore;
      
       for(i=0;i<16;i++) {
          if( info->instr & (1<<i) ) { /* if the register is in the destination */

             /* load the value for this register  */
             asm volatile("ldr %0, [%1]" 
                          : "=r" (tmp)
                          : "r" (addr)
                );
	  
             arm_state->context.gpr.reg[i] = tmp; /* Update the VM */
             addr += 4;
          }
       }

       /* if writing back is activated */
       if( info->instr & 0x200000 )
          /* Update the base register */
          arm_state->context.gpr.reg[rn] -= count_register*4;
    }

    return 0;
}


static INLINE int
read_copro_matches(uint32_t instruction, uint8_t copro, uint8_t crn, uint8_t crm, uint8_t opc1, uint8_t opc2)
{
   Thumb_MRC_T1Encoding thumb_mrc_info;
   thumb_mrc_info.instr = instruction;

#define rt(name) name.s.second.s.rt
#define crn(name) name.s.first.s.crn
#define crm(name) name.s.second.s.crm
#define opc1(name) name.s.first.s.opc1
#define opc2(name) name.s.second.s.opc2
#define coproc(name) name.s.second.s.coproc

   return coproc(thumb_mrc_info) == copro && crn(thumb_mrc_info) == crn && crm(thumb_mrc_info) == crm && opc1(thumb_mrc_info) == opc1 && opc2(thumb_mrc_info) == opc2;

#undef rt
#undef crn
#undef crm
#undef opc1
#undef opc2
#undef coproc

}

/* MIDR register - Returns the device ID code that contains information about the processor */
/* http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0460c/Bgbceeed.html */
#define MRC_IS_MIDR(inst) read_copro_matches(inst, 15, 0, 0, 0, 0)


/* ID_MMFR0 register - Provides information about the implemented memory model
 * and memory management support */
/* http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0438e/BABEHBEB.html */
#define MRC_IS_IDMMFR0(inst) read_copro_matches(inst, 15, 0, 1, 0, 4)


/* SCTLR - Provides the top level control of the system, including its memory system */
/* http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0388e/CIHEHDHJ.html */
#define MRC_IS_SCTLR(inst) read_copro_matches(inst, 15, 1, 0, 0, 0)

/* TTBCR - Determines which of the Translation Table Base Registers, TTBR0 or
 * TTBR1, defines the base address for the translation table walk that is
 * required when a VA is not found in the TLB. */
/* http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0433a/CIHHACFF.html */
#define MRC_IS_TTBCR(inst) read_copro_matches(inst, 15, 2, 0, 0, 2)


/* ID_MMFR1 - Provides information about the memory model and memory management support in AArch32. */
/* http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0500f/BABFGFJH.html */
#define MRC_IS_IDMMFR1(inst) read_copro_matches(inst, 15, 0, 1, 0, 5)

int arbitration_copro( void* saved_state, Hypervisor_InstrInfo* info, int extended UNUSED)
{
   ARM_SavedState *state = (ARM_SavedState *) saved_state;
   
   /* Reading copro */
   if (info->behavior & Behavior_ReadsCopro)
   {
      print_copro_instr(info->instr);
     
      /* CRN is legit */
      if (MRC_IS_MIDR(info->instr) || MRC_IS_IDMMFR0(info->instr) || MRC_IS_SCTLR(info->instr) || MRC_IS_TTBCR(info->instr) || MRC_IS_IDMMFR1(info->instr))
      {
         return 1;
      }
      for(;;);
   }
   else /* Write copro */
   {
      arm_execute_step(state, info->address, state->context.cpsr.fields.thumb);
   }
  
  return 1;
}



/** When detecting a CPS, the hypervisor patch the next instruction to return here.
    Then we can rerun the analysis from here, without skipping first
*/
void NORETURN return_from_mode_switch(void * state) {
   Hypervisor_Platform *p = hypervisor_current_platform();
   p->unpatchAddress(state);
   hypervisor_execute(state, 0);
}

ARM_SavedState *update_new_state(int old_mode, int new_mode, uint32_t new_cpsr)
{
   Hypervisor_Platform *p = hypervisor_current_platform();
   ARM_SavedState *new_state = p->stateForMode(new_mode);
   ARM_SavedState *old_state = p->stateForMode(old_mode);

   
   /* Not all registers should be copied from one state to the
    * other, see p93 of the ARM1176 technical reference to see
    * banked register for different modes */
   
   /* R0 to R7 are the same whatever the mode is so they must be copied */
   new_state->context.gpr.name.r0 = old_state->context.gpr.name.r0;
   new_state->context.gpr.name.r1 = old_state->context.gpr.name.r1;
   new_state->context.gpr.name.r2 = old_state->context.gpr.name.r2;
   new_state->context.gpr.name.r3 = old_state->context.gpr.name.r3;
   new_state->context.gpr.name.r4 = old_state->context.gpr.name.r4;
   new_state->context.gpr.name.r5 = old_state->context.gpr.name.r5;
   new_state->context.gpr.name.r6 = old_state->context.gpr.name.r6;
   new_state->context.gpr.name.r7 = old_state->context.gpr.name.r7;
   /* R8 -> R12 are only copied when new_mode AND old mode is not FIQ */
   if (new_mode != ARMMode_FIQ && old_mode != ARMMode_FIQ)
   {
      new_state->context.gpr.name.r8 =  old_state->context.gpr.name.r8;
      new_state->context.gpr.name.r9 =  old_state->context.gpr.name.r9;
      new_state->context.gpr.name.r10 = old_state->context.gpr.name.r10;
      new_state->context.gpr.name.r11 = old_state->context.gpr.name.r11;
      new_state->context.gpr.name.r12 = old_state->context.gpr.name.r12;
   }
   
   /* PC is same for all states, copy it */
   new_state->context.gpr.name.pc = old_state->context.gpr.name.pc;
   /* SP & LR are always different so do not copy them except for system/user */
   if ((old_mode == ARMMode_System && new_mode == ARMMode_User) ||
       (new_mode == ARMMode_System && old_mode == ARMMode_User))
   {
      new_state->context.gpr.name.sp = old_state->context.gpr.name.sp;
      new_state->context.gpr.name.lr = old_state->context.gpr.name.lr;
   }
   new_state->context.cpsr.value = new_cpsr;
   return new_state;
}

static int is_valid_psr(uint32_t psr)
{
   int mode = psr & 0b11111;
   switch (mode)
   {
      case ARMMode_User       :
      case ARMMode_FIQ        :
      case ARMMode_IRQ        :
      case ARMMode_Supervisor :
      case ARMMode_Monitor    :
      case ARMMode_Abort      :
      case ARMMode_Hyp        :
      case ARMMode_Undefined  :
      case ARMMode_System     :
         break; /* Valid mode */
      default:
         return 0; /* Invalid mode */
   }
   return 1;
}


#define IS_LDM_EXCEPTION_RET(instr) (((instr) & 0b1110010100001000000000000000) == 0b1000010100001000000000000000)
#define IS_MOVS(instr) (((instr) & 0b1111111111111111111111110000) == 0b0001101100001111000000000000)
#define IS_SUBS(instr) assert(NOT_IMPLEMENTED)

uint32_t get_pc_from_stack(uint32_t *base_ptr, int pu_bits, uint16_t reglist)
{
   int reg_count = BITS_ONE(reglist); /* count the number of bits set in reglist */
   int u_bit = pu_bits & 1;
   int p_bit = (pu_bits >> 1) & 1;
   if (p_bit)
      reg_count++; /* empty stack (i.e. not full) so base_ptr points to next free space */
   if (!u_bit)
      reg_count = -reg_count; /* ascending stack so need to seek backward */
   return base_ptr[reg_count-1];
}

int arm_writes_psr_arbitration( void* saved_state, Hypervisor_InstrInfo* info, int extended UNUSED){
   ARM_SavedState* state = (ARM_SavedState*) saved_state;
   uint8_t old_mode = state->context.cpsr.fields.mode;
   int new_value=0;
   int spsr = 0; /* true if the instruction writes the spsr */

   assert(!state->context.cpsr.fields.thumb);
  
   /* MSR (immediate) */
   if( ( info->instr & 0b1111111100111111000000000000 ) == 0b11001000001111000000000000 ){
      assert(NOT_IMPLEMENTED);
   }

   /* MSR (register) */
   else if( ( info->instr & 0b1111111100111111111111110000 ) == 0b1001000001111000000000000 ){
      assert(NOT_IMPLEMENTED);
   }

   /* MSR (Banked register) */
   else if( ( info->instr & 0b1111101100001111111011110000 ) == 0b0001001000001111001000000000){
      /* Setting a banked register let it do it*/
      return 0;
   }

   /* MSR (immediate) - sys instruction */
   if( ( info->instr & 0b1111101100001111000000000000 ) == 0b11001000001111000000000000 ){
      assert(NOT_IMPLEMENTED);
   }

   /* MSR (register) - sys instruction (see p 1996-1997) */
   else if( ( info->instr & 0b1111101100001111111111110000 ) == 0b1001000001111000000000000 ) {
      int mask = BITS_VAL(info->instr, 16, 4); /* msr fields (fsxc) */
      int reg_num = info->instr & 0b1111;
      uint32_t reg_val = state->context.gpr.reg[reg_num];
      /* Check if destination is SPSR or CPSR */
      spsr = BIT_VAL(info->instr,22); /* bit 22 is 1 if writing spsr */
      if (spsr)
         new_value = state->context.spsr.value;
      else
         new_value = state->context.cpsr.value;
       
      if (BIT_VAL(mask,0)) /* bits <7:0> (c) */
         BITS_SET_VAL(new_value, 0, 8, BITS_VAL(reg_val, 0, 8));
      if (BIT_VAL(mask, 1)) /* bits <15:8> (x) */
         BITS_SET_VAL(new_value, 8, 8, BITS_VAL(reg_val, 8, 8));
      if (BIT_VAL(mask, 2)) /* bits <23:16> (s) */
         BITS_SET_VAL(new_value, 16, 8, BITS_VAL(reg_val, 16, 8));
      if (BIT_VAL(mask, 3)) /* bits <31:24> (f) */
         BITS_SET_VAL(new_value, 24, 8, BITS_VAL(reg_val, 24, 8));
      log(LOG_BEHAVIOR, "AT %p, MSR_0x%x %s, r%d : 0x%08x 0x%08x\n", info->address, mask, spsr ? "spsr" : "cpsr", reg_num, new_value, reg_val);
   }
   /* MSR (Banked register) - sys instruction */
   else if( ( info->instr & 0b1111101100001111111011110000 ) == 0b1001000001111001000000000 ){
   }
   /* CPS (ARM) (p 1978 of ARM Architecture reference) */
   else if( (info->instr & 0b11111111111100011111111000100000) ==  0b11110001000000000000000000000000) {
      int mode_switch = BIT_VAL(info->instr, 17); /* Mode switch is bit 17 */
      int affectA     = BIT_VAL(info->instr, 8); /* bit 8 is A */
      int affectI     = BIT_VAL(info->instr, 7); /* bit 7 is I */
      int affectF     = BIT_VAL(info->instr, 6); /* bit 6 is F */
      int enable      = BITS_VAL(info->instr, 18, 2) == 2; /* bit 18 and 19 tells if enable of disable (cpsXe or cpsXd ) */
      if (mode_switch) {
         new_value = (state->context.cpsr.value & ~0b11111);
         new_value |= info->instr & 0b11111;
         /* Check if new_value is valid psr */
         assert(is_valid_psr(new_value));
         if ((new_value & 0b11111) != ARMMode_User)
         {
            /* CPS, patch the next instruction and let it run to perform the actual mode switch 
               But ONLY if CPS is not to user mode.
            */
            ARM_SavedState* new_state = update_new_state(old_mode, new_value & 0b11111, new_value);
            Hypervisor_Platform *p = hypervisor_current_platform();
            addr_t to_patch = info->address + info->length;
            /* The patch has to be done in the new state because
               it sets informations in the structure to be able to unpatch */
            p->patchAddress(new_state, (uint32_t)(to_patch), (addr_t) return_from_mode_switch);
            /* But the execution is done in the old state as the ACTUAL mode change will be done by the execution */
            p->execute(state);
         }
      }
      else
      {
         new_value = state->context.cpsr.value;
         if (enable)
         {
            /* enable A, I or F bit */
            if (affectA)
               BIT_SET(new_value, 8);
            if (affectI)
               BIT_SET(new_value, 7);
            if (affectF)
               BIT_SET(new_value, 6);
         }
         else
         {
            /* disable A, I or F bit */
            if (affectA)
               BIT_CLR(new_value, 8);
            if (affectI)
               BIT_CLR(new_value, 7);
            if (affectF)
               BIT_CLR(new_value, 6);
         }
      }
   }
   /* Emulate the LDM and MOVS to see where we would return after its execution */
   /* 
      - If the return is in the hypervisor, let it run.
      - If the return does not change mode, update PC to LR and continue analysis
      - If the return does change mode, do nothing here, wait for
      the generic mode switch handling few lines later 
   */
   /* LDM (exception return) */
   else if (IS_LDM_EXCEPTION_RET(info->instr) || IS_MOVS(info->instr))
   {
      uint32_t return_addr;
      if (IS_MOVS(info->instr))
         return_addr = state->context.gpr.name.lr;
      else if (IS_LDM_EXCEPTION_RET(info->instr))
      {
         /* this instruction is ldm Rn[!], {..., pc}^ */
         int reg_num = BITS_VAL(info->instr, 16, 4); /* Rn is bits <19-16> */
         uint16_t reglist = BITS_VAL(info->instr, 0, 16); /* Register list is bits <15-0> */
         uint32_t *addr = (uint32_t*)state->context.gpr.reg[reg_num]; /* Base address used to restore registers */
         int pu_bits = BITS_VAL(info->instr, 23, 2); /* PU bits are <24-23> */
         return_addr = get_pc_from_stack(addr, pu_bits, reglist);
      }
      else
         assert(NOT_IMPLEMENTED);
      log(LOG_BEHAVIOR, "Preparing to return to 0x%08x\n", return_addr);
      /* It is safe to let the instruction executes if it returns to the hypervisor.
         This case can occur when the hypervisor is interrupted by an exception
      */
      if (is_in_hypervisor(return_addr))
      {
         /* If in hypervisor mode, set the hypervisor state PC to the
          * needed instruction and execute using the hypervisor state
          * that have been saved in exception handling (see
          * arm_exception.c)
          */
         ARM_SavedState* hyp_state = arm_platform_get_hypervisor_state();
         hyp_state->context.gpr.name.pc = info->address;
         arm_platform_execute(hyp_state);
      }

      /* This instruction will restore SPSR in CPSR, so the new value
       * is the current SPSR */
      log(LOG_BEHAVIOR, "Mode change using LDM or MOVS\n");
      new_value = state->context.spsr.value;
      /* The instruction is used to jump and restore a different mode.  
          
         If the mode remains the same, then the intruction is just emulated,
         thus we have to make the jump by copying lr to pc.
         Otherwise, the mode switch will be handled by letting the
         guest execute itself in a safe guarded way.

         The important thing here is then: if no mode switch, emulate the jump */
      if ((new_value & 0b11111) == state->context.cpsr.fields.mode)
         state->context.gpr.name.pc = return_addr;
      log(LOG_BEHAVIOR, "Switching to mode %d\n", new_value & 0b11111);
   }    


/* new_value must be a valid psr */
   assert(is_valid_psr(new_value));
   if (spsr)
   {
      /* If writing spsr, just update it */
      state->context.spsr.value = new_value;
      return 1;

   }
/* The write is in CPSR, it may change CPU mode */
   log(LOG_BEHAVIOR, "Instruction changes CPU mode ?\n");
   if (new_value != 0 && state->context.cpsr.fields.mode != (new_value & 0b11111)) /* CPU mode change */
   {
      uint8_t new_mode = new_value & 0b11111;
      log(LOG_BEHAVIOR, "CPU Mode change from %d to %d (new cpsr: %x)\n", state->context.cpsr.fields.mode, new_mode, new_value);
      /* CPU Mode change. Depending on the modes (from and to mode) we have to update the states */
      ARM_SavedState *new_state = update_new_state(old_mode, new_mode, new_value);

      if (new_state->patch_size) /* if mode switch to a mode that was patched, it is safe to let it run */
      {
         log(LOG_BEHAVIOR, "Switching to a patched mode %d\n", new_mode);
         arm_platform_execute(state);
      }

      if( new_mode == ARMMode_User ){
         /* If the guest wants to go in user mode, stop the hypervisor.
            We will get control back when an interrupt occur
         */
         log(LOG_BEHAVIOR, "switching to user mode -> releasing control at PC: %p\n", hypervisor_current_platform()->getPCValue(state));
         log(LOG_BEHAVIOR, "LR for current mode is %p\n", state->context.gpr.name.lr);
         arm_platform_execute(state);
      }
      /* If not user mode, we need to save the new mode */
      log(LOG_BEHAVIOR, "saving new mode in destination_mode_switch field of the old mode (state: %p)\n", state);
      /* Set the mode of current state to the new mode.  This is needed
         for the analyzer to be aware of the mode change and to switch
         state accordingly
      */
      state->destination_mode_switch = new_mode;
      /* And set the CPSR of the new state to the new value */
      new_state->context.cpsr.value = new_value;
   }
   return 1;
}

#include "arm_exception.h"
int arm_swi_arbitration( void* saved_state, Hypervisor_InstrInfo* info, int extended UNUSED){
  
  /* Perform a mode switch and analyse the exception vector */
  ARM_SavedState* state = (ARM_SavedState*) saved_state;
  log(LOG_BEHAVIOR, "Performing SWI arbitration for instruction %x\n", state->context.gpr.name.pc);

  /* Update the new state */
  ARM_SavedState* new_state = update_new_state(state->context.cpsr.fields.mode, ARMMode_Supervisor, state->context.cpsr.value);
  /* SVC backups the CPSR in SPSR */
  new_state->context.spsr.value = state->context.cpsr.value;
  /* Update the new_state cpsr mode field to Supervisor */
  new_state->context.cpsr.fields.mode = ARMMode_Supervisor;
  /* Set the LR of the new state to the instruction following SVC */
  new_state->context.gpr.name.lr = state->context.gpr.name.pc + info->length;
  /* Set the PC of the new state to the SVC interrupt handler */
  new_state->context.gpr.name.pc = HANDLER_ADDR_SVC;
  /* Tell the Hypervisor that the new analysis will be performed in Supervisor mode */
  state->destination_mode_switch = ARMMode_Supervisor;
  
  
  /* Do not skip the next instruction because PC is not at the begining of the SWI handler*/
  return 0;
}

void patch_udf_and_run( void* state, Hypervisor_InstrInfo* info, int extended UNUSED){
   arm_platform_patchAddress(state,(uint32_t)(info->address),(addr_t)hypervisor_exit_from_guest);

  arm_platform_execute(state);
}
