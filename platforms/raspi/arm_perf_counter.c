/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "arm_perf_counter.h"

uint32_t ccnt_overflow_counter;

/*  Sets up performance counter coprocessor.
 *
 */
#define __dsb() __asm__ __volatile__ ("mcr   p15,0,%[t],c7,c10,4\n" :: [t] "r" (0) : "memory");

#define __isb(x) __asm__ __volatile__ ("mcr p15, 0, %0, c7, c5, 4" : : "r" (0) : "memory")

void arm_init_perfcounters (uint8_t reset_cyclecount)
{
  if(reset_cyclecount)
    asm volatile ("mcr p15,  0, %0, c15,  c12, 0\n" : : "r" (0b1000101));
  else
    asm volatile ("mcr p15,  0, %0, c15,  c12, 0\n" : : "r" (0b1000001));
  __isb(); 
}

void arm_stop_perfcounters (void)
{
   unsigned int value;

  asm volatile ("mrc p15, 0, %0, c15, c12, 0\t\n": "=r"(value));

  value &= ~1;
  
  asm volatile ("mcr p15,  0, %0, c15,  c12, 0\n" : : "r"(value));
  
  __isb(); 
}

/* Gets actual cycle-counter value */
unsigned int arm_get_cyclecount (void)
{
   unsigned int value;
   // Read CCNT Register
   asm volatile ("MRC p15, 0, %0, c15, c12, 1\t\n": "=r"(value));

   return value;
}

void arm_set_cyclecount (unsigned int value)
{
  // Write CCNT Register
  asm volatile ("MCR p15, 0, %0, c15, c12, 1\t\n":: "r"(value));
  __isb();
}

// page 196
#define ARM_TIMER_LOD 0x2000B400
#define ARM_TIMER_VAL 0x2000B404
#define ARM_TIMER_CTL 0x2000B408
#define ARM_TIMER_CLI 0x2000B40C
#define ARM_TIMER_RIS 0x2000B410
#define ARM_TIMER_MIS 0x2000B414
#define ARM_TIMER_RLD 0x2000B418
#define ARM_TIMER_DIV 0x2000B41C
#define ARM_TIMER_CNT 0x2000B420

// page 112
#define IRQ_BASIC 0x2000B200
#define IRQ_PEND1 0x2000B204
#define IRQ_PEND2 0x2000B208
#define IRQ_FIQ_CONTROL 0x2000B210
#define IRQ_ENABLE_BASIC 0x2000B218
#define IRQ_DISABLE_BASIC 0x2000B224

#define PUT32(x,y) *(uint32_t*)(x) = (y)

void arm_init_overflow_timer(void){
  ccnt_overflow_counter = 0;
  
  PUT32(IRQ_DISABLE_BASIC,1);
  PUT32(ARM_TIMER_CTL,0x003E0000);
  PUT32(ARM_TIMER_LOD,2000000-1);
  PUT32(ARM_TIMER_RLD,2000000-1);
  PUT32(ARM_TIMER_DIV,0x000000F9);
  PUT32(ARM_TIMER_CLI,0);
  PUT32(ARM_TIMER_CTL,0x003E00A2);
  PUT32(IRQ_ENABLE_BASIC,1);
  
  asm volatile("mrs r0, cpsr");
  asm volatile("bic r0,r0,#0x80");
  asm volatile("msr cpsr_c,r0");
}

static timer_t *rasp_timer = (timer_t *)0x20003000;

uint32_t arm_get_timer(void)
{
   return rasp_timer->clo;
   /* uint64_t res = rasp_timer->clo; */
   /* res |= rasp_timer->chi; */
   /* return res; */
}
