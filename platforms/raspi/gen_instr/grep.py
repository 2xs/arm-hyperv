#!/usr/bin/env python3

import sys

from arm_instr_info_list import *

if __name__ == "__main__":

    if len(sys.argv) == 2:
        arm_ins = None
        thm_ins = None
        try :
            id = int(sys.argv[1])
            arm_ins = filter(lambda l : l['id'] == id, instructions_arm)
            thm_ins = filter(lambda l : l['id'] == id, instructions_thumb)
                
        except ValueError:
            label = sys.argv[1]
            arm_ins = filter(lambda l : label in l['label'], instructions_arm)
            thm_ins = filter(lambda l : label in l['label'], instructions_thumb)

        finally:

            print ("ARM: ")
            for l in list(arm_ins):
                print(l)
                print( "\tbehaviors : " + build_behavior(l) )
                
            print ("Thumb: ")
            for l in list(thm_ins):
                print(l)
                print( "\tbehaviors : " + build_behavior(l) )
