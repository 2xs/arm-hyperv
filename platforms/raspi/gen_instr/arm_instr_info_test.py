#!/usr/bin/env python3

from arm_instr_info_list import *

if __name__ == "__main__":

    print(".section .linux, \"awx\" @progbits\n.globl linux_zimage\n.type linux_zimage, %function\nlinux_zimage:")

    instructions_thumb = make_table(instructions_thumb)
    
    for instr  in instructions_thumb:
        v = build_value( instr )
        print(".word " + v)

        #print(instr['label'])
        
    #    if len(v) < 32:
     #       print(".byte 0b" + v[8:16])
      #      print(".byte 0b" + v[0:8])
       # else:

