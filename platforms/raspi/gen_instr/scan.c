/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include <stdio.h>
#include <stdint.h>

typedef enum Hypervisor_InstrBehavior {
    Behavior_Nothing = 1,

    Behavior_ReadsMemory = 2,
    Behavior_WritesMemory =4,

    Behavior_Branches = 8,
    Behavior_Link = 16,
    Behavior_CanChangeState = 32,

    Behavior_RaisesException = 64,

    Behavior_ITBlockStart = 128,

    Behavior_ReadsCopro = 256,
    Behavior_WritesCopro = 512,

    Behavior_Unpredictable = 1024,
    Behavior_Unsuported = 2048,
    
    Behavior_ReadsPsr = 1<<12,
    Behavior_WritesPsr = 1<<13,

    Behavior_HardwareAccess = 1<<14,

    Behavior_OddBranch = 1<<15,
} Hypervisor_InstrBehavior;
#define NB_BEHAVIOR 16
struct instr_table {
   uint32_t value, mask, user, behavior;
   char* name;
};

const char* tab[NB_BEHAVIOR] = {
   "Behavior_Nothing",
   "Behavior_ReadsMemory ",
   "Behavior_WritesMemory ",
   "Behavior_Branches ",
   "Behavior_Link ",
   "Behavior_CanChangeState ",
   "Behavior_RaisesException ",
   "Behavior_ITBlockStart ",
   "Behavior_ReadsCopro ",
   "Behavior_WritesCopro ",
   "Behavior_Unpredictable ",
   "Behavior_Unsuported ",
   "Behavior_ReadsPsr ",
   "Behavior_WritesPsr ",
   "Behavior_HardwareAccess",
   "Behavior_OddBranch"};

#define LIGHT_DEBUG
#include "arm_instr_info_gen.c"

int main(int argc, char* argv[]) {
   int i,nb,j;
   
   if (argc == 1) {
      fprintf(stderr, "Usage: %s 0x... \n", argv[0][0] == '.' ? argv[0]+2 : argv[0]);
      return -1;
   }
   
   for (nb=1; nb < argc; ++nb) {
      uint32_t instr = strtol(argv[nb], 0, 16);

      for(i=0 ; i < sizeof(instr_table_thumb) / sizeof(struct instr_table) ; i++){
         if((instr & instr_table_thumb[i].mask) == instr_table_thumb[i].value){	  
            printf("Thumb, %s\n",instr_table_thumb[i].name);
            for(j=0;j<NB_BEHAVIOR;j++)
               if(instr_table_thumb[i].behavior & (1<< j))
                  printf("\t%s\n", tab[j]);
            break;
         }
      }
  
      for(i=0 ; i < sizeof(instr_table_arm) / sizeof(struct instr_table) ; i++){
         if((instr & instr_table_arm[i].mask) == instr_table_arm[i].value){	  

            printf("ARM, %s\n",instr_table_arm[i].name);
            for(j=0;j<NB_BEHAVIOR;j++)
               if(instr_table_arm[i].behavior & (1<< j))
                  printf("\t%s\n", tab[j]);

            break;
         }
      }
   }
   return 0;
}
