/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __ARM_CPU_H__
#define __ARM_CPU_H__

#include "types.h"
#include "platform.h"
#include "arm_cp15.h"

#define MAX_STATE_SIZE 256
#define MAX_MODE_COUNT 32
#define MAX_HYPERVISOR_STACK_SIZE 1024

/* TODO: is this pragma really needed ? If so, the comment should
 * explicit why */
#pragma pack(1)

typedef void (*HypervisorReturnFunction)(void * state);

typedef enum {
    ARMMode_User       = 16,
    ARMMode_FIQ        = 17,
    ARMMode_IRQ        = 18,
    ARMMode_Supervisor = 19,
    ARMMode_Monitor    = 22,
    ARMMode_Abort      = 23,
    ARMMode_Hyp        = 26,
    ARMMode_Undefined  = 27,
    ARMMode_System     = 31,
} ARM_Mode;

typedef struct {
    unsigned mode:5;
    unsigned thumb:1;
    unsigned fiq_mask:1;
    unsigned irq_mask:1;
    unsigned async_mask:1;
    unsigned big_endian:1;
    unsigned it_7_2:6;
    unsigned ge:4;
    unsigned reserved:4;
    unsigned jazelle:1;
    unsigned it_1_0:2;
    unsigned q:1;
    unsigned v:1;
    unsigned c:1;
    unsigned z:1;
    unsigned n:1;
} ARM_cpsr_fields;

typedef union
{
   reg_t reg[16];
   struct {
      reg_t r0,  r1, r2,  r3;
      reg_t r4,  r5, r6,  r7;
      reg_t r8,  r9, r10, r11;
      reg_t r12, sp, lr,  pc;
   } name;
} ARM_Registers;

typedef union
{
   uint32_t value;
   ARM_cpsr_fields fields;
} ARM_cpsr;

typedef struct
{
   ARM_Registers gpr;
   ARM_cpsr cpsr, spsr;
} ARM_Context;

typedef struct {
   ARM_Context context;
   // +72
   /* These fields are used to remember where and how the guest code was patched by a call to hypervisor_handle_return */
   uint32_t patch_addr;      /**< the address of the patched instructions */
   uint8_t  saved_instrs[8]; /**< the instructions that were patched */
   uint8_t  patch_size;      /**< The size of the patch. If 0 then nothing was patched in this state */
   int      patched_instruction_index; /**< index of the patched instruction in the instruction table (used to speed up hypervisor_handle_return */

   addr_t last_it_instr_addr;
   CP15_SCTLR saved_sctlr;
   uint32_t ttbr0;
   uint32_t ttbr1;
   uint8_t  destination_mode_switch; /**< When the analyzer detects a
                                      * mode switch, it stores the new
                                      * mode here. This will then be
                                      * used to select the new mode
                                      * for restarting analysis */
} ARM_SavedState;




typedef union {
  uint32_t entry;
  struct {
    unsigned pxn:1;
    unsigned one:1;
    unsigned b:1;
    unsigned c:1;
    unsigned xn:1;
    unsigned domain:4;
    unsigned impl:1;
    unsigned ap:2;
    unsigned tex:3;
    unsigned ap2:1;
    unsigned s:1;
    unsigned ng:1;
    unsigned zero:1;
    unsigned ns:1;
    unsigned base_addr:12;
  } value;
} ARM_Section;

typedef union {
  uint32_t entry;
  struct {
    unsigned xn:1;
    unsigned one:1;
    unsigned b:1;
    unsigned c:1;
    unsigned ap:2;
    unsigned tex:3;
    unsigned ap2:1;
    unsigned s:1;
    unsigned ng:1;
    unsigned page_base_addr:20;
  } value;
} ARM_L2_SmallPage;


int arm_platform_modeIndex(void * saved_state);
addr_t arm_platform_getPCValue(void * saved_state);
void arm_platform_setReturnValue(void * saved_state, uint32_t value);
uint32_t arm_platform_getReturnValue(void * saved_state);
void arm_platform_setLastITAddr(void * saved_state, addr_t value);
int arm_platform_isInITBlock(void * state, addr_t instr_addr);
void arm_platform_setPCValue(void * saved_state, addr_t value, int link);
void arm_platform_copyState(void * state, void ** new_state); /*warn: the copy is made in a global variable so only one copy can be used at the same time*/
int arm_platform_bytesNeededToPatchAddress(void * saved_state, addr_t patch_address, addr_t destination);
/* The instruction table index is the index returned by instruction_info on patch_address */
void arm_platform_patchAddress(void * saved_state, addr_t patch_address, addr_t destination);
void arm_platform_unpatchAddress(void * saved_state);
void arm_platform_init(void);
void arm_platform_debugDumpState(void * saved_state);
int arm_platform_is_condition_met(void* state);
void arm_platform_setCPSR(void * state, uint32_t value);
uint32_t arm_platform_getCPSR(void * state);
void arm_irq_enable();
void arm_irq_disable();
int arm_platform_get_destination_mode(void *state);
/** returns the current CPU mode by reading cpsr */
ARM_Mode arm_platform_cpu_mode(void);
/** Stop the execution of the platform */
void arm_platform_stop(void);
/** Returns the state of the hypervisor */
ARM_SavedState *arm_platform_get_hypervisor_state(void);
addr_t arm_platform_get_next_instr(const addr_t start);

#endif /* end of include guard: __ARM_CPU_H__ */
