/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "arm_mmu.h"
#include "arm_cp15.h"
#include "debug.h"

uint32_t tt0[TRANSLATION_TABLE_SIZE] __attribute__((aligned(16384)));
uint32_t tt1[TRANSLATION_TABLE_SIZE] __attribute__((aligned(16384)));

extern addr_t hypervisor_physical_base;
extern int hypervisor_pages_number;



void arm_mmu_map_section(uint32_t tt[], addr_t paddr, addr_t vaddr, unsigned int flags)
{
   int index;
   unsigned int AP;
   unsigned int CB;
   unsigned int XN;
   unsigned int TEX = 0;

   AP = (flags  & MMU_FLAG_READWRITE) ? 0x3 : 0x2;
   CB = ((flags & MMU_FLAG_CACHED) ? 0x2 : 0) | ((flags & MMU_FLAG_BUFFERED) ? 0x1 : 0);
   XN = !!(flags  & MMU_FLAG_EXEC);
   index = vaddr / MB;
   // section mapping
   tt[index] = (paddr & ~(MB-1)) | (TEX << TEX_SHIFT) | (AP << AP_SHIFT) | (0 << DOM_SHIFT) | (XN << XN_SHIFT) | (CB << CB_SHIFT) | (2<<0);
   
   arm_flush_tlb();
}


