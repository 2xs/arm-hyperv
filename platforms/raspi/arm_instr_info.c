/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "cache-template.h"
#include "arm_instr_info.h"
#include "output.h"
#include "arm_behavior_functions.h"

#include "arm_mmu.h"

#include "debug.h"

#include "bench_internals.h"


struct instr_table {
    uint32_t value, mask, user, behavior;
#ifdef LOG_LEVEL
  char* name;
#endif
};

#include "gen_instr/arm_instr_info_gen.c" /*import instr_table*/

#define SWITCH_HALFWORD(n) ((((n)&0xffff)<<16) | (((n)&0xffff0000)>>16))


int32_t arm_platform_sign_extend(uint32_t x, int len) {
    return (x & (1 << (len - 1)) ?
                (-1u << len)  | x
              : x & ((1 << len) - 1));
}

int arm_platform_instruction_length_arm() {
    return 4;
}

int arm_platform_instruction_length(ARM_SavedState * state, uint32_t word UNUSED) {
   assert (!state->context.cpsr.fields.thumb);
   return arm_platform_instruction_length_arm();
}

///////////

Hypervisor_InstrCondition arm_platform_condition_arm(/* out */ Hypervisor_InstrInfo * info) {
   Hypervisor_InstrCondition cond = (Hypervisor_InstrCondition) (info->instr >> 28);
   if (cond == Cond_UNDEF)
      cond = Cond_AL; /* Cond_UNDEF is for instructions that can not be conditionnal, thus always executed (CPS for example) */
   return cond; 
}

Hypervisor_InstrCondition arm_platform_condition(/* out */ Hypervisor_InstrInfo * info) {
   return arm_platform_condition_arm(info);
}

///////////
struct opcode_table {
    uint32_t value, mask, user, behavior;
};

int arm_platform_condition_is_met(ARM_SavedState * state, Hypervisor_InstrCondition condition) {
    // ARMv7-A and ARMv7-R Architecture Reference Manual
    // A8.3 - Conditional execution

    switch(condition) {
        case Cond_EQ: return state->context.cpsr.fields.z == 1;
        case Cond_NE: return state->context.cpsr.fields.z == 0;
        case Cond_CS: return state->context.cpsr.fields.c == 1;
        case Cond_CC: return state->context.cpsr.fields.c == 0;
        case Cond_MI: return state->context.cpsr.fields.n == 1;
        case Cond_PL: return state->context.cpsr.fields.n == 0;
        case Cond_VS: return state->context.cpsr.fields.v == 1;
        case Cond_VC: return state->context.cpsr.fields.v == 0;
        case Cond_HI: return state->context.cpsr.fields.c == 1 && state->context.cpsr.fields.z == 0;
        case Cond_LS: return state->context.cpsr.fields.c == 0 || state->context.cpsr.fields.z == 1;
        case Cond_GE: return state->context.cpsr.fields.n == state->context.cpsr.fields.v;
        case Cond_LT: return state->context.cpsr.fields.n != state->context.cpsr.fields.v;
        case Cond_GT: return state->context.cpsr.fields.z == 0 && state->context.cpsr.fields.n == state->context.cpsr.fields.v;
        case Cond_LE: return state->context.cpsr.fields.z == 1 || state->context.cpsr.fields.n != state->context.cpsr.fields.v;
        default: return 1;
    }
}


int instr_table_callback(ARM_SavedState * state, /* out */ Hypervisor_InstrInfo * info, const struct instr_table *instr, int extended UNUSED) {
  extern Hypervisor_Behavior_Function arm_behavior_runtime_functions[NB_BEHAVIOR];
  
  arm_behavior_runtime_functions[Behavior_Number_ReadsCopro] = arbitration_copro;
  arm_behavior_runtime_functions[Behavior_Number_WritesCopro] = arbitration_copro;
  arm_behavior_runtime_functions[Behavior_Number_RaisesException] = 0x0;

  info->behavior = instr->behavior;
  log(LOG_INSTRUCTION_INFO, "@%p: %x User flags: %x\n", info->address, info->instr, instr->user);
  switch (instr->user)
  {
     case Undefined_Flag:
        arm_behavior_runtime_functions[Behavior_Number_Unpredictable] = (Hypervisor_Behavior_Function)patch_udf_and_run;
       break;
    
     case flag_r15_12_15:
       arm_behavior_runtime_functions[Behavior_Number_OddBranch] = branch_set_mask_0xffff0fff;
        break;


     case flag_r15_15:
        arm_behavior_runtime_functions[Behavior_Number_OddBranch] = odd_branch_emulation;
        break;

     case flag_r15_0_2_7: /* assume Thumb 16 */
       arm_behavior_runtime_functions[Behavior_Number_OddBranch] = branch_set_mask_0xff78;	  
        break;

     case flag_r15_12_15_low:
       arm_behavior_runtime_functions[Behavior_Number_OddBranch] = branch_set_mask_switch_0xffff0fff;   
        break;

     case ARMUser_Reg03:
     {
        int reg_idx = info->instr & 15;
        info->pc_destination = state->context.gpr.reg[reg_idx];

        arm_behavior_runtime_functions[Behavior_Number_Branches] = branch_at_pc_destination;
        break;
     }
     case ARMUser_SMC:
     {
        // TODO
        break;
     }
     case ARMUser_BKPT:
     {
        // TODO
        info->condition_met = 1; // BKPT is always executed
        break;
     }
     case ARMUser_BLX:
     {
        
        int h = (info->instr >> 24) & 1;
        int32_t delta = arm_platform_sign_extend((info->instr << 2) | (h << 1), 26);
        /* Branch is relative to PC and PC is the address of the instruction */
        info->pc_destination = info->address + 8 + delta;
	
        arm_behavior_runtime_functions[Behavior_Number_Branches] = branch_at_pc_destination;
        break;
     }
     case ARMUser_Branch:
     {
        int32_t delta = arm_platform_sign_extend(info->instr << 2, 26);
        /* Branch is relative to PC and PC is the address of the instruction */
        info->pc_destination = info->address + 8 + delta;
	
        arm_behavior_runtime_functions[Behavior_Number_Branches] = branch_at_pc_destination;
        break;
     }
     
     case ARMUser_SVC:
     {
         info->exception = info->instr & 0xFFFFFF;
	 arm_behavior_runtime_functions[Behavior_Number_RaisesException] = arm_swi_arbitration;
         break;
     }     

     case Writes_PSR_Flag:
	 arm_behavior_runtime_functions[Behavior_Number_WritesPsr] = arm_writes_psr_arbitration;
         break;
     /* To be removed : Handled by the tables */
     /* case ThumbUser_MovPC: */
     /* case Thumb32User_MovPC: */

  }

  return 1;
}


#ifndef NO_INSTRUCTION_CACHE
static cache_entry_t instruction_cache[CACHE_SIZE];
#endif

unsigned int arm_platform_instruction_info(void * saved_state, addr_t at, /* out */ Hypervisor_InstrInfo * info, unsigned int index, int extended) {
   ARM_SavedState * state = (ARM_SavedState *) saved_state;

   static const uint32_t word_masks[] = {0, 0xFF, 0xFFFF, 0xFFFFFF, 0xFFFFFFFF};
   unsigned int i;


   BENCH_INC(INSTRUCTION_INFO_COUNT);

   at &= ~1;
    
   uint32_t word = * (uint32_t *) at;

#ifndef NO_INSTRUCTION_CACHE
   /** If the caller does not know the index of the instruction, query the cache */
   if (index == 0)
   {
      const cache_value_t *cached_index = cache_lookup(instruction_cache, word);
      if (cached_index)
         index = cached_index->integer;
   }
#endif
   
   info->address = at;
   info->length = arm_platform_instruction_length(state, word);
   word &= word_masks[info->length];
   info->instr = word;
   info->behavior = Behavior_Nothing;
   info->condition = arm_platform_condition(info);
      
   if (extended) {
      info->condition_met = arm_platform_condition_is_met(state, info->condition);
   }
    
   if (index != 0)
   { /* true if the caller already knows the instruction */
      i = index-1;

      assert(!state->context.cpsr.fields.thumb);
      log(LOG_INSTRUCTION_INFO, "[ARM] (cache) %x : %s @%p %x\n",i,instr_table_arm[i].name, at,info->instr);
      instr_table_callback( state, info, &instr_table_arm[i], extended);
#ifndef NO_INSTRUCTION_CACHE
      /* update the cache */
      cache_value_t cached_index;
      cached_index.integer = index;
      cache_update(instruction_cache, info->instr, &cached_index);
#endif
      return (info->idx = index);
   }
   /* The instruction is unknown to the caller, find it */
   BENCH_INC(INSTRUCTION_INFO_LOOP_COUNT);
   /* Try to match the instruction with ARM masks*/
   for(i=0 ; i < sizeof(instr_table_arm) / sizeof(struct instr_table) ; i++)
   {
      
      if((info->instr & instr_table_arm[i].mask) == instr_table_arm[i].value)
      {
         log(LOG_INSTRUCTION_INFO, "[ARM] (no cache) %x : %s @%p %x\n",i,instr_table_arm[i].name, at,info->instr);
         instr_table_callback( state, info, &instr_table_arm[i], extended);
         BENCH_ADD(INSTRUCTION_INFO_LOOP_SUM, i);
#ifndef NO_INSTRUCTION_CACHE
         /* update the cache */
         cache_value_t cached_index;
         cached_index.integer = i+1;
         cache_update(instruction_cache, info->instr, &cached_index);
#endif
         return (info->idx = (i + 1));
      }
   }
   return 0;
}


