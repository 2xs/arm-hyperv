/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "arm_cpu.h"
#include "arm_execute.h"
#include "arm_instr_info.h"

extern void return_from_exec(ARM_SavedState* state);

/*
  thumb_mode = 1 if goto thumb code   
  thumb_mode = 0 if goto arm code
*/
void arm_execute_step(void* guest_state, addr_t addr, bool thumb_mode)
{
  ARM_SavedState hv;
  addr_t addr_to_patch = addr + arm_platform_instruction_length((ARM_SavedState *)guest_state, *(uint32_t *)addr);
  
  arm_platform_patchAddress(guest_state, addr_to_patch, (addr_t)return_from_exec);
  arm_execute_single_instr(&hv, guest_state, addr, thumb_mode);
     
  arm_platform_unpatchAddress(guest_state);
}
