/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#include "debug.h"

#include "printf.h"
#include "arm_uart_io.h"

#include <stdint.h>
#define ARM_TIMER_CLI 0x2000B40C

#include "arm_cpu.h"
#include "arm_trampolines.h"
#include "hypervisor.h"
#include "arm_exception.h"


void arm_trampoline_hypervise_restart_it();

extern uint32_t _hypervisor_begin_addr;
extern uint32_t _hypervisor_end_addr;

/* r0 -> guest exception handler address
   r1 -> exception return address
*/
/* USED attribute is needed because of link time optimisation that strips the function otherwise */
void USED NAKED  generic_restart_analyse_it(void)
{
   register uint32_t *exception_return_address;
   /* To use hypervise_restart we need to fake that we are in guest
    * ready to execute the guest's swi handler */
   /* To do this, the only thing to do is to set the stack to the
      guest state that corresponds to supervisor mode and set LR to the address of the
      swi handler which is at 0x8 */

   /* Some remarks:

      - Here SP is the guest stack for the mode corresponding to the
        current handler. So we do not need to change it there
        (arm_trampoline_hypervise_restart_it will handle this)
   */
   

   /* Backup guest user state before messing with registers */
   asm volatile("push {r0-r12}");
   /* Get the return address */
   asm volatile("mov %0, r1" :"=r"(exception_return_address));
   
   /* Get the guest state for the mode corresponding to handler */
   ARM_SavedState *state = arm_platform_saved_state_for_mode(arm_platform_cpu_mode());

   if (is_in_hypervisor((addr_t)exception_return_address))
   {
      /* If the exception occurs in the hypervisor, we have to save its state */
      /* In the current stack, there are:
         {r0-r12, r0-r2}. 

         The first occurence of r0-r2 are not those we want. The value
         of the registers when the exception occurs are the second ones.
      */
      ARM_SavedState *hyp_state = arm_platform_get_hypervisor_state();
      uint32_t *irq_sp;
      /* Get the stack pointer */
      asm volatile("mov %0, sp" :"=r"(irq_sp));
      /* and backup hypervisor state from there */
      /* First, r3-r12 */
      memcpy(hyp_state->context.gpr.reg + 3, irq_sp + 3, 10*sizeof(uint32_t));
      /* then r0-r2 */
      memcpy(hyp_state->context.gpr.reg, irq_sp + 13, 3*sizeof(uint32_t));
      /* cpsr */
      asm volatile("mrs %0, spsr" : "=r"(hyp_state->context.cpsr.value));
      /* switch to supervisor mode and get LR and stack */
      /* Hypervisor always runs in supervisor mode, so we have to assert here */
      assert(hyp_state->context.cpsr.fields.mode == ARMMode_Supervisor);
      asm volatile("cps #19");
      /* backup lr, sp and spsr */
      asm volatile("mov %0, lr" :"=r"(hyp_state->context.gpr.name.lr));
      asm volatile("mov %0, sp" :"=r"(hyp_state->context.gpr.name.sp));
      asm volatile("mrs %0, spsr" :"=r"(hyp_state->context.spsr.value));
      /* switch back to irq mode */
      asm volatile("cps #18");
      /* then continue as usual, launch the analysis of the guest's exception handler */
   }
   
   /* if the exception occurs when the hypervisor code was running, we must:
      - run the guest exception vector under hypervision (thus analyse and execute it)
      - at the end of the exception vector, resume hypervisor execution
      
      The process is the same when interrupting guest code
   */
   /* Set the state's LR to the current return instruction (the
    * instruction of that needs to be executed after the
    * exception handler)
    */
   state->context.gpr.name.lr = (uint32_t) exception_return_address;
   /* restore registers to their value before guest was interrupted */
   asm volatile("pop {r0-r12}");
   /* Set LR to the guest exception handler. We use LR because the
      arm_trampoline_hypervise_restart_it uses it to update PC in the
      guest state
    */
   asm volatile("mov lr, r0");
   asm volatile("msr cpsr_fsxc, r2"); /* restore cpsr */
   asm volatile("pop {r0-r2}"); /* restore original r0, r1 of the guest */
   /* launch analyse on the guest interrupt vector */
   /* Here we must ldr pc and NOT let gcc generate a function call
      so that the generated assembly code does NOT change the LR value 
      that we explicitely put at the exception vector address
      The ..._restart_it function uses lr to know where the hypervision should start
   */
   asm volatile("ldr pc, =arm_trampoline_hypervise_restart_it");
}


/* For all handlers, the guest handler address is in r0, but the guest
 * r0 must be saved on stack.  The generatic_restart_analyse_it will
 * made the pop
 */

void NAKED swi_handler(void) {
   asm volatile("push {r0-r2}"); /* backup r0,r1 and r2, these will be restored by generic_restart_analyse_it */
   asm volatile("mrs r2, cpsr"); /* load cpsr in r2 to restore it later */
   asm volatile("mov r0, #0x8"); /* Address of the guest swi handler */
   asm volatile("mov r1, lr");   /* return address of the handler */
   asm volatile("b generic_restart_analyse_it");
}

void NAKED irq_handler(void) {

   /*
     Need to test if the IRQ was trigged in guest code or hypervisor code:
     - If in guest code -> easy, same as swi/svc case

     - If in hypervisor (because IRQ are disable in hypervisor, this
       can only occur when returning from guest to hypervisor in
       arm_trampolin...):
       - Remember that an IRQ was triggered
       - let the handler finish normally -> will return in hypervisor
       - when the hypervisor wants to restart analysis, simulate a virtual IRQ by:
          - set the LR of the guest state to the value of PC
          - set PC to the address of the guest's IRQ handler
          - switch the hypervisor to irq mode
          - launch analysis with the new state
    */
   
   asm volatile("push {r0-r2}"); /* backup r0 and r1, these will be restored by generic_restart_analyse_it */
   asm volatile("mrs r2, cpsr"); /* load cpsr in r2 to restor it later */
   asm volatile("mov r0, #0x18"); /* Address of the guest irq handler */
   asm volatile("mov r1, lr");
   asm volatile("b generic_restart_analyse_it");
}

void __attribute__((interrupt("UDF"))) undef_handler(void){
   register uint32_t *lr asm("r9");
   register uint32_t r0 asm("r0");
   asm volatile("mov r9, lr");
   lr--;
   printf("UDF caught while executing code at 0x%p\n", lr);
   asm volatile("mrs r0, spsr");
   printf("CPSR was 0x%x\n", r0);
   printf("Instruction is 0x%x\n", *lr);
   assert(0);
}

void generic_handler(void)
{  
   assert(0);
}

/* This functions increments the overflow count, and resets the overflow flag in the hardware */
void cycle_count_overflow_reset(void)
{  
  unsigned int pmcr;
  extern uint32_t ccnt_overflow_counter;
  
  ccnt_overflow_counter++;
  
  asm volatile ("MRC p15, 0, %0, c15, c12, 0\t\n": "=r"(pmcr)); /* Reads Performance Monitor Control Register */
  pmcr &= ~(1<<10); /* Resets 10th bit => resets overflow flag */
  asm volatile ("MCR p15, 0, %0, c15, c12, 0\t\n": "=r"(pmcr)); /* Writes Performance Monitor Control Register */
   
  *(uint32_t*)ARM_TIMER_CLI = 0;
}
