/* Copyright Université Lille 1 (2014)
 * 
 * Corresponding author: Michael Hauspie <michael.hauspie@univ-lille1.fr>
 * For full author list, see AUTHORS.md file
 * 
 * This software is a computer program whose purpose is to
 * hypervise guest systems.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */
#ifndef __ARM_PERF_COUNTER_H__
#define __ARM_PERF_COUNTER_H__

#include <stdint.h>

typedef volatile struct
{
   uint32_t cs;
   uint32_t clo;
   uint32_t chi;
   uint32_t c0;
   uint32_t c1;
   uint32_t c2;
   uint32_t c3;
} timer_t;

void arm_init_perfcounters (uint8_t reset_cyclecount);
void arm_stop_perfcounters (void);
unsigned int arm_get_cyclecount (void);
void arm_set_cyclecount (unsigned int value);
void arm_init_overflow_timer(void);

/* 1Mhz counter */
uint32_t arm_get_timer(void); /* FIXME: 64bit */

#endif /* __ARM_PERF_COUNTER_H__ */
