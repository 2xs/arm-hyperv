#!/bin/bash

set -e
trap clean EXIT

QEMU_REPOSITORY=https://github.com/Torlus/qemu.git
QEMU_COMMIT=bf4eb7c8e705e997233415926fae83d31240e3b1
BUILD_FOLDER=/tmp/qemu-build

function clean()
{
    rm -rf $BUILD_FOLDER
}


git clone $QEMU_REPOSITORY $BUILD_FOLDER
cd $BUILD_FOLDER
git checkout $QEMU_COMMIT


# git submodule update --init dtc
# Does not build with recent glib because of a warning, so disable it
./configure --target-list=arm-softmmu --python=/usr/bin/python2 --extra-cflags='-Wno-error=deprecated-declarations'

make

make install

cd /
rm -rf $BUILD_FOLDER
