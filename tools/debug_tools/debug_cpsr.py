#!/usr/bin/env python2

# Copyright Université Lille 1 (2014)

# Michael Hauspie <michael.hauspie@univ-lille1.fr>
# François Serman

# This software is a computer program whose purpose is to
# hypervise a guest system.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import sys

def destroy_cpsr(psr):
    cpsr_fields = {
        'mode': None,
        'THUMB': None,
        'FIQ': None,
        'IRQ': None,
        'ABORT': None,
        'ENDIANESS': None,
        'JAZEL': None,
        'CONDITIONS': {
            'Q': None,
            'V': None,
            'C': None,
            'Z': None,
            'N': None,
        }
    }
    
    modes = { 0b10000: 'User mode',
              0b10001: 'FIQ mode',
              0b10010: 'IRQ mode',
              0b10011: 'Supervisor mode',
              0b10110: 'Monitor mode',
              0b10111: 'Abort mode',
              0b11011: 'Undefined mode',
              0b11111: 'System mode',
              0b11010: 'Hyp mode'
    }
    
    cpu_mode = psr & 0b11111
    if not cpu_mode in modes:
        raise Exception ("cpumode not handled")
    
    psr >>= 5
    thumb = psr & 1

    psr >>= 1
    fiq = psr & 1

    psr >>= 1
    irq = psr & 1

    psr >>= 1
    abort = psr & 1

    psr >>= 1
    endianess = psr & 1
 
    # skip GE and blanks fields 
    
    psr >>= 15
    jazel = psr & 1

    psr >>= 2
    conditions = psr

    cpsr_fields['mode'] = modes[cpu_mode]
    cpsr_fields['THUMB']= bool(thumb)
    cpsr_fields['FIQ']= bool(fiq)
    cpsr_fields['IRQ']= bool(irq)
    cpsr_fields['ABORT']= bool(abort)
    cpsr_fields['ENDIANESS']= endianess
    cpsr_fields['JAZEL']= bool(jazel)
    cpsr_fields['CONDITIONS']= {
            'Q': bool(conditions & 1),
            'V': bool(conditions & 0b10),
            'C': bool(conditions & 0b100),
            'Z': bool(conditions & 0b1000),
            'N': bool(conditions & 0b10000)
    }

    return cpsr_fields
            
def dump_cpsr(cpsr):
    base = 0
    if cpsr[0:2] == '0b' :
        base = 2
    elif cpsr[0:2] == '0x':
        base = 16
    else:
        base = 10
    try:
        psr = int(cpsr, base)
    except ValueError:
        return 

    psr_fields = destroy_cpsr(psr)

    if psr_fields['THUMB']:
        if psr_fields['JAZEL']:
            mode_instr = 'THUMB EE'
        else:
            mode_instr = 'THUMB'
    else:
        if psr_fields['JAZEL']:
            mode_instr = 'JAZEL'
        else:
            mode_instr = 'ARM'
        
    
    print "CPU mode: %s (%s)\nFIQ %s\nIRQ %s\nimprecise abort %s\n%s-ENDIAN" % (psr_fields['mode'],
                                                                                                  mode_instr,
                                                                                                  ('disabled' if psr_fields['FIQ'] else 'enabled'),
                                                                                                  ('disabled' if psr_fields['IRQ'] else 'enabled'),
                                                                                                  ('disabled' if psr_fields['ABORT'] else 'enabled'),
                                                                                                  ('BIG' if psr_fields['ENDIANESS'] else 'LITTLE'))

    print "FLAGS :"
    print "\tNegative condition flag : %s\n\tZero condition flag : %s\n\tCarry condition flag : %s\n\tOverflow condition flag : %s\n\tCumulative saturation bit : %s" %(psr_fields['CONDITIONS']['N'], psr_fields['CONDITIONS']['Z'], psr_fields['CONDITIONS']['C'], psr_fields['CONDITIONS']['V'], psr_fields['CONDITIONS']['Q'])
    
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "usage :", sys.argv[0], "<cpsr>"

    for cpsr in sys.argv[1:]:
        dump_cpsr(cpsr)
